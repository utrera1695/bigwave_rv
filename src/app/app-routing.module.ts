import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './modules/web/home/home.component';
import {ForgotPasswordComponent} from './modules/web/forgot-password/forgot-password.component';
import {SearchsProfileComponent} from './modules/web/search/searchs-profile/searchs-profile.component';
import {SearchPageComponent} from './modules/web/search/search-page.component';
import {DetailComponent} from './modules/web/detail/detail.component';
import {DetailProductComponent} from './modules/web/detail-product/detail-product.component';
import {ProductsByCategoryComponent} from './modules/web/products-by-category/products-by-category.component';
import {HelpCenterComponent} from './modules/web/help-center/help-center.component';
import {NgModule} from '@angular/core';
import {CartListComponent} from './modules/web/user/shoping-cart/cart-list/cart-list.component';
import {MessagesProfileComponent} from './modules/shared-components/messages-profile/messages-profile.component';
import {InformationComponent} from './modules/web/user/information/information.component';
import {OwnProfilesComponent} from './modules/web/user/own-profiles/own-profiles.component';
import {ProfileFollowComponent} from './modules/web/user/profile-follow/profile-follow.component';
import {IsLoggedGuardService} from './core/services/guards/is-logged-guard.service';
import {GettingStartedComponent} from './modules/web/user/getting-started/getting-started.component';
import {AdminPostResolveService} from './core/services/admin-post-resolve.service';
import {BlackboardComponent} from './modules/profile/blackboard/blackboard.component';
import {PromotionsComponent} from './modules/profile/promotions/promotions.component';
import {PublicationsComponent} from './modules/profile/publications/publications.component';
import {StadisticsComponent} from './modules/profile/stadistics/stadistics.component';
import {ScheduleComponent} from './modules/profile/schedule/schedule.component';
import {OrdersComponent} from './modules/profile/orders/orders.component';
import {CustomersComponent} from './modules/profile/customers/customers.component';
import {OwnProfileInformationComponent} from './modules/profile/own-profile-information/own-profile-information.component';
import {ListProductComponent} from './modules/profile/products/list-product/list-product.component';
import {EditProductComponent} from './modules/profile/products/edit-product/edit-product.component';
import {ErrorPageComponent} from './modules/shared-components/error-page/error-page.component';
import {ProfileWishesComponent} from './modules/profile/profile-wishes/profile-wishes.component';
import {WishesListComponent} from './modules/profile/profile-wishes/wishes-items/wishes-list.component';
import {UserWishesComponent} from './modules/web/user/user-wishes/user-wishes.component';


export const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
	{
		path: 'errors/:id',
		component: ErrorPageComponent
	},
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'searchs',
    component: SearchPageComponent,
    data: {
       tabs: 'products'
    }
  },
  {
    path: 'profiles',
    component: SearchsProfileComponent
  },
  {
    path: 'profiles/:type_profile/:profile_id',
    component: DetailComponent
  },
  {
    path: 'product/:type_profile/:profile_id/:product_id',
    component: DetailProductComponent
  },
  {
    path: 'cart',
    component: CartListComponent
  },
  {
    path: 'products/:type/:categoryID/:categoryName',
    component: ProductsByCategoryComponent
  },
  {
    path: 'helpCenter',
    component: HelpCenterComponent
  },
  {
    path: 'profile',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'comenzar-ahora'
      },
      {
        path: 'comenzar-ahora',
        component: GettingStartedComponent,
				canActivate: [IsLoggedGuardService]
      },
      {
        path: 'perfil',
        component: InformationComponent,
        canActivate: [IsLoggedGuardService],
      },
      {
        path: 'pymes',
        component: OwnProfilesComponent,
        canActivate: [IsLoggedGuardService]
      },
			{
				path: 'independents',
				component: OwnProfilesComponent,
				canActivate: [IsLoggedGuardService]
			},
			{
				path: 'sellers',
				component: OwnProfilesComponent,
				canActivate: [IsLoggedGuardService]
			},
      {
        path: 'deseos',
        component: UserWishesComponent,
        canActivate: [IsLoggedGuardService]
      },
      {
        path: 'mensajes',
        component: MessagesProfileComponent,
        canActivate: [IsLoggedGuardService]
      },
      {
        path: 'carrito',
        component: CartListComponent
      },
      {
        path: 'favoritos',
        component: ProfileFollowComponent,
        canActivate: [IsLoggedGuardService]
      }
    ]
  },
  {
    path: 'admin/:type/:id',
    canActivate: [IsLoggedGuardService],
    resolve: {
      post: AdminPostResolveService
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'pizarra'
      },
      {
        path: 'pizarra',
        component: BlackboardComponent
      },
			{
    		path: 'productos',
				component: ListProductComponent
			},
			{
    		path: 'productos/:id',
				component: EditProductComponent
			},
      {
        path: 'informacion',
        component: OwnProfileInformationComponent
      },
      {
        path: 'publicaciones',
        component: PublicationsComponent
      },
      {
        path: 'mensajes',
        component: MessagesProfileComponent
      },
			{
				path: 'deseos',
				component: ProfileWishesComponent
			},
			{
				path: 'deseos/:id/listado',
				component: WishesListComponent
			},
      {
        path: 'promociones',
        component: PromotionsComponent
      },
      {
        path: 'estadisticas',
        component: StadisticsComponent
      },
      {
        path: 'pedidos',
        component: OrdersComponent
      },
      {
        path: 'clientes',
        component: CustomersComponent
      },
      {
        path: 'ubicacion-horarios',
        component: ScheduleComponent
      }
    ]
  },
	{
		path: '**',
		redirectTo: '/errors/404'
	}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
