import { Pipe, PipeTransform } from '@angular/core';
import { FilterSearchs } from './filterSearchs.pipe';
import { element } from 'protractor';


@Pipe({
  name: 'filterCategoryPyme'
})
export class FilterCategoryPyme implements PipeTransform {

  transform(pymes: any, category?: any): any {
    if (category != null) {
      var array_product: Array<any> = [];
      for (let j = 0; j < pymes.length; j++) {
        let prod = null;
        for (let i = 0; i < pymes[j].subcategories.length; i++) {
          for (let index = 0; index < category.subcategories.length; index++) {
            if (pymes[j].subcategories[i].id == category.subcategories[index].id) {
              prod =  pymes[j]
            }
          }
        }
        if (prod != null) {
          array_product.push(prod);
        }
      }

      return array_product;

    } else {
      return pymes
    }
  }
}