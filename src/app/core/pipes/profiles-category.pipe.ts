import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'profilesCategory'
})
export class ProfilesCategoryPipe implements PipeTransform {

  transform(array: any[], page_size: number, page_number: number, OrderName: string, filtro: string): any {

    if (!array.length) return []

    let new_array = [];

    new_array = array.filter(prod => prod.title.toUpperCase().indexOf(filtro.toUpperCase()) >-1);


    if (OrderName != undefined) {
      if (OrderName === 'A-Z') {
        new_array.sort(function (a, b) {  
          if (a.title > b.title) {
            return 1;
          }
          if (a.title < b.title) {
            return -1;
          }
          // a must be equal to b
          return 0;
        });
      } else {
        new_array.sort(function (a, b) {
          if (a.title < b.title) {
            return 1;
          }
          if (a.title > b.title) {
            return -1;
          }
          // a must be equal to b
          return 0;
        });
      }
    }

    page_size = page_size || 5
    page_number = page_number || 1
    --page_number
    //@ts-ignore
    return new_array.slice(page_number * page_size, (page_number + 1) * page_size);

  }

}
