import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'truncar'
})
export class Truncar implements PipeTransform {

  transform(cadena: any, nro: any): any {

    if (cadena != null && cadena != undefined) {

      if (cadena.length > nro) {
        return cadena.slice(0, nro).toLowerCase() + "...";
      }
      return cadena.toLowerCase();
    }

    return cadena

  }

}
