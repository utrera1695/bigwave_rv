import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'paginatorSlicer'
})
export class PaginatorSlicerPipe implements PipeTransform {

  transform<T>(arr: T[], pageSize: number, pageNumber: number, sortPath?: string, sort?: string): T[] {
  	if(!arr || !arr.length){
  		return arr;
		}

  	if(sortPath && sort){
			arr.sort((a, b) => {
				const _a = _.get(a, sortPath, 0) as number;
				const _b = _.get(b, sortPath, 0) as number;
				if(sort === 'ASC'){
					return _b - _a;
				}
				return _a - _b;
			});
		}

  	const start = pageNumber === 0 ? 0: pageNumber * pageSize;
		const end = start + pageSize;

		return arr.slice(start, end);
  }

}
