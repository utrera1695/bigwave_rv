import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterstock'
})
export class FilterStockPipe implements PipeTransform {

  transform(array: any[], page_size: number, page_number: number, order_by: number): any[] {
    if (!array.length) return []
    let oriArray = [];
    oriArray = array;
    if (order_by != undefined) {
      if (order_by == 1) {
        oriArray.sort(function (a, b) {
          return a.price - b.price;
        });
      } else {
        oriArray.sort(function (a, b) {
          return b.price - a.price;
        });
      }
    }
    page_size = page_size || 5
    page_number = page_number || 1
    --page_number
    //@ts-ignore
    return oriArray.slice(page_number * page_size, (page_number + 1) * page_size);

  }


}


