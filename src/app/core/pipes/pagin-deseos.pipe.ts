import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginDeseos'
})
export class PaginDeseosPipe implements PipeTransform {

  transform(array: any[], page_size: number, page_number: number, filtro:string, Orden:string,user_id): any {
    
    if (!array.length) return []

    var new_array: any [] = [];

    if (user_id) {
      for (let index = 0; index < array.length; index++) {   
        if (array[index].attributes.user.id == user_id) {
          new_array.push(array[index])
        } 
      }
    } 
    
    new_array = array.filter(prod => prod.attributes.wisheable.name.toUpperCase().indexOf(filtro.toUpperCase()) >-1)

    if (Orden != undefined) {
      if (Orden == 'Menor a mayor') {
        new_array.sort(function (a, b) {
            return a.attributes.wisheable.price - b.attributes.wisheable.price;
        });
      } else {
        new_array.sort(function (a, b) {
            return b.attributes.wisheable.price - a.attributes.wisheable.price; 
        });
      }
    }

    page_size = page_size || 5
    page_number = page_number || 1
    --page_number
    //@ts-ignore
    return new_array.slice(page_number * page_size, (page_number + 1) * page_size);
  }


}
