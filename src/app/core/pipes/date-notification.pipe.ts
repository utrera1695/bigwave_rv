import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment'

@Pipe({name: 'dateNotification'})
export class DateNotificationPipe implements PipeTransform {
    transform(value?: Date | string, type?: string): string {
        const date = moment(value);

        if(!date.isValid()){
            return 'fecha inválida';
        }
        const fromNow = date.locale('es-us').startOf('minute').fromNow();
        const now = moment();
        const yesterday = moment().subtract(1, 'days');

        if(date.isBetween(yesterday, now)){
            return fromNow;
        }

        switch(type){
          case 'short' : return date.format('lll');
          default : return date.format('LLL');
        }

    }
}
