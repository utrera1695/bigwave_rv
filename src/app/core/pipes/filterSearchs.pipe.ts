import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearchs'
})
export class FilterSearchs implements PipeTransform {

  transform(profiles: any, criterio?: any): any {

    if (criterio.length == 0 || criterio.length == 3)
      return profiles

    let maxu = []
    if (criterio.length == 2) {
      profiles.forEach(element => {
        let aux = { letter: "", content: [] }
        aux.letter = element.letter;
        aux.content = []
        element.content.forEach(elem => {
          if (elem.type_profile == criterio[0] || elem.type_profile == criterio[1]) {
            aux.content.push(elem)
          }
        });
        maxu.push(aux)

      });
      return maxu
    } else {
      if (criterio.length == 1) {
        profiles.forEach(element => {
          let aux = { letter: "", content: [] }
          aux.letter = element.letter;
          aux.content = []
          element.content.forEach(elem => {
            if (elem.type_profile == criterio[0]) {
              aux.content.push(elem)
            }
          });
          maxu.push(aux)
        });
      }
      return maxu
    }
  }

}
