import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'filterComments'
})
export class FilterComments implements PipeTransform {

  transform(arr: any, isPublication?:boolean, ownerProfile?:any): any {
    if(isPublication){
      
      return arr.filter((elem)=>elem.attributes.user.id == ownerProfile)
    }else{
      return arr.filter((elem)=>elem.attributes.user.id != ownerProfile)
    }

  }

}
