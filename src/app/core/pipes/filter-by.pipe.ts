import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

  transform<T>(arr: T[], path: string, filter: string): T[] {
  	if(!arr || !arr.length || !filter) return arr;
    return arr.filter(obj => {
    	const propValue = (_.get(obj, path, '') as string).toLowerCase();
    	return propValue.includes(filter.toLowerCase());
    });
  }

}
