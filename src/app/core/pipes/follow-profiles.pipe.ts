import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'followProfiles'
})
export class FollowProfilesPipe implements PipeTransform {

  transform(array: any[], page_size: number, page_number: number, filtro: string, OrderName: string): any {

    let new_array = [];

    new_array = array.filter(prod => prod.attributes.title.toUpperCase().indexOf(filtro.toUpperCase()) >-1);

     if (OrderName != undefined) {
      if (OrderName === 'A-Z') {
        new_array.sort(function (a, b) {  
          if (a.attributes.title > b.attributes.title) {
            return 1;
          }
          if (a.attributes.title < b.attributes.title) {
            return -1;
          }
          return 0;
        });
      } else {
        new_array.sort(function (a, b) {
          if (a.attributes.title < b.attributes.title) {
            return 1;
          }
          if (a.attributes.title > b.attributes.title) {
            return -1;
          }
          return 0;
        });
      }
    }

    page_size = page_size || 5
    page_number = page_number || 1
    --page_number
    //@ts-ignore
    return new_array.slice(page_number * page_size, (page_number + 1) * page_size);
  }

}
