import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginCategory'
})
export class PaginCategoryPipe implements PipeTransform {

  transform(array: any[], page_size: number, page_number: number, type:string, filtro: string, order_by:number): any {

    if (!array.length) return []

    page_size = page_size || 5
    page_number = page_number || 1
    --page_number

    let new_array: any [];

    
    if (type == 'categoria') {
      new_array = array.filter(prod => prod.name.toUpperCase().indexOf(filtro.toUpperCase()) >-1)
    } else {
      new_array = array.filter(prod => prod.attributes.name.toUpperCase().indexOf(filtro.toUpperCase()) >-1)
    }

    if (order_by != undefined) {
      if (order_by == 1) {
        new_array.sort(function (a, b) {
          if (type == 'categoria') {
            return a.price - b.price;
          } else {
            return a.attributes.price - b.attributes.price;
          } 
        });
      } else {
        new_array.sort(function (a, b) {
          if (type == 'categoria') {
            return b.price - a.price;
          } else {
            return b.attributes.price - a.attributes.price;
          } 
        });
      }
    }

    



    //@ts-ignore
    return new_array.slice(page_number * page_size, (page_number + 1) * page_size);

  }


}
