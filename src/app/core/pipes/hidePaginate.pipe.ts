import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'hideItems'
})
export class HideItems implements PipeTransform {

  transform(arr: any, band?:boolean , elemts?: any, pos?): any {

   if(band && pos == undefined){ // trunca un array sin childrens
    if(elemts == undefined)
    return arr
    if(elemts <= arr.length){
      return arr.slice(0 , 3)
    }
   }

   return arr
  }

}
