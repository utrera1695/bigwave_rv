import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(arr: any[], path: string, filter: 'ASC' | 'DESC'): any[] {
		if(!arr || !arr.length || !filter) return arr;
    return arr.sort((a, b) => {
    	const _a = _.get(a, path, 0) as number;
    	const _b = _.get(b, path, 0) as number;
			if(filter === 'ASC'){
				return _b - _a;
			}
    	return _a - _b;
		});
  }

}
