import { NgModule } from '@angular/core';

import {

	MatButtonModule,
	MatCheckboxModule,
	MatToolbarModule,
	MatCardModule,
	MatSidenavModule,
	MatMenuModule,
	MatIconModule,
	MatGridListModule,
	MatSelectModule,
	MatDividerModule,
	MatListModule,
	MatTooltipModule,
	MatDialogModule,
	MatChipsModule,
	MatRadioModule,
	MatAutocompleteModule,
	MatExpansionModule,
	MatSlideToggleModule,
	MatSliderModule,
	MatPaginatorModule,
	MatTabsModule,
	MatProgressSpinnerModule,
	MatProgressBarModule,
	MatInputModule,
	MatSnackBarModule,
	MatBadgeModule,
	MatBottomSheetModule, MatDatepickerModule, MatTreeModule, MatNativeDateModule, MatTableModule, MatSortModule, MatButtonToggleModule
} from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';

const modules = [
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatCardModule,
  MatSidenavModule,
  MatMenuModule,
  MatIconModule,
  MatGridListModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatTooltipModule,
  MatDialogModule,
  MatChipsModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatPaginatorModule,
  MatTabsModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatInputModule,
  MatSnackBarModule,
  MatBadgeModule,
  MatBottomSheetModule,
	MatDatepickerModule,
	MatTreeModule,
	MatNativeDateModule,
	MatTableModule,
	MatSortModule,
	MatButtonToggleModule,
  MatStepperModule
];

@NgModule({
  imports: modules,
  exports: modules
})
export class MaterialModule {}
