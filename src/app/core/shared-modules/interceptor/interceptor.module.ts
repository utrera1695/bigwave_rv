import {Router} from '@angular/router';
import {Injectable, Injector, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError, ignoreElements, takeWhile} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {UIComponentsService} from '../../services/material-ui.service';
import {AngularTokenService} from 'angular-token';


@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
	private ui: UIComponentsService;
	private ats: AngularTokenService;
	constructor(
		public router: Router,
		private injector: Injector
	) {
		this.ui = injector.get(UIComponentsService);
		this.ats = injector.get(AngularTokenService);
	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		return next.handle(request).pipe(
			catchError((error: HttpErrorResponse) => {
				if(error.status !== 401 && error.status !== 404 && error.status !== 409){
					this.router.navigate(['/errors', error.status]);
					return throwError(error);
				}

				return throwError(error);
			}
		));
	}


}

@NgModule({
	providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
	]
})
export class InterceptorModule { }
