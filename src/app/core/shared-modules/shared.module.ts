import {NgModule} from '@angular/core';
import {InterceptorModule} from './interceptor/interceptor.module';

@NgModule({
  declarations: [],
  imports: [
		InterceptorModule
  ]
})
export class SharedModule { }
