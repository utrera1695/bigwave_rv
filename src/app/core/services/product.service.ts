import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BigwaveResponse } from '../abstract/interfaces/response.interface';
import { Product } from '../abstract/interfaces/product.interface';
import {ProfileTypes} from '../abstract/enums/profile-types.enum';

@Injectable()
export class ProductService {

	constructor(private http: HttpClient) {}

    // Ver un item
	getByEndpoint(endpoint: string): Observable<HttpResponse<BigwaveResponse<Product>>>{
		return this.http.get<any>(`${environment.api_url}${endpoint}`, {observe: 'response'});
	}

	getProduct(profileType: ProfileTypes, profileId: string, productId: string): Observable<BigwaveResponse<any>>{
		const url = `${profileType.toString()}/${profileId}/products/${productId}`;
		return this.http.get<any>(`${environment.api_url}${url}`);
	}

	getWished(profileType: ProfileTypes, id: string): Observable<BigwaveResponse<any>>{
		const url = `${profileType.toString()}/${id}/wished_products`;
		return this.http.get<any>(`${environment.api_url}${url}`);
	}

	getAll(profileType: ProfileTypes, id: string): Observable<BigwaveResponse<any>>{
		return this.http.get<any>(`${environment.api_url}${profileType.toString()}/${id}/products`);
	}

	update(profileType: ProfileTypes, profileId: string, productId: string, payload): Observable<BigwaveResponse<any>>{
		const url = `${profileType.toString()}/${profileId}/products/${productId}/update`;
		return this.http.put<any>(`${environment.api_url}${url}`, payload);
	}

	delete(profileType: ProfileTypes, id: string, idProduct: string ,pass: string): Observable<BigwaveResponse<any>>{
		const url = `${profileType}/${id}/products/${idProduct}/destroy`;
		return this.http.put<any>(`${environment.api_url}${url}`, {current_password: pass});
	}

	changeStatus(idProduct: string): Observable<BigwaveResponse<any>>{
		return this.http.put<any>(`${environment.api_url}products/${idProduct}/status`, null);
	}

}
