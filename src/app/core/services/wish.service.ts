import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Wish, WishDetail} from '../abstract/interfaces/wish.interface';
import {ProfileTypes} from '../abstract/enums/profile-types.enum';
import {BigwaveResponse, GenericClass} from '../abstract/interfaces/response.interface';
import {ProfileWish, QueryProfileWish} from '../abstract/interfaces/profile-wishes.interface';
import {BigwaveUtils} from '../abstract/class/BigwaveUtils.class';
import {UserWish} from '../abstract/interfaces/user-wish.interface';

@Injectable({
  providedIn: 'root'
})
export class WishService {

  constructor(
  	private http: HttpClient
	) { }

	create(wish: Wish){
  	return this.http.post(`${environment.api_url}wishes/create`, {wish});
	}

	wished(id){
  	return this.http.get(`${environment.api_url}products/${id}/wished`);
	}

	destroyWish(id){
  	return this.http.put(`${environment.api_url}wishes/${id}/destroy`, null);
	}

  unwished(product_id){
    return this.http.put(`${environment.api_url}products/${product_id}/unwish`, null);
  }

	userWishes(query?: string){
  	const params = new HttpParams()
			.set('categories', query);
  	const opts = query ? { params }: {};
  	return this.http.get<BigwaveResponse<GenericClass<UserWish>[]>>(`${environment.api_url}wishes/my_wishes`, opts);
	}

	profileWishedProducts(profile_type: ProfileTypes, profile_id, query: QueryProfileWish){
  	const params = BigwaveUtils.parseObjectToParams(query);
  	return this.http.get<BigwaveResponse<ProfileWish[]>>(`${environment.api_url}${profile_type}/${profile_id}/wished_products`, {params});
	}

	wishesByProductProfile(product_id, query: any){
		const params = BigwaveUtils.parseObjectToParams(query);
		return this.http.get<BigwaveResponse<WishDetail[]>>(`${environment.api_url}wishes/products/${product_id}`, {params});
	}

	answerWish(data, id_wish){
  	return this.http.post(`${environment.api_url}sended_wish/${id_wish}/answer`, { answer_wish: data });
	}
}
