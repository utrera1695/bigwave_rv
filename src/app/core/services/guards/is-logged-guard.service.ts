import {Injectable, Injector} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AngularTokenService} from 'angular-token';

@Injectable()
export class IsLoggedGuardService implements CanActivate {

  constructor(
    private router: Router,
    private angularTokenService: AngularTokenService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.angularTokenService.userSignedIn()) {
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }
}
