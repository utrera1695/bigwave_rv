import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Country} from '../abstract/interfaces/country-state.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class LocationService {

	currentCountryStateSource: BehaviorSubject<Country> = new BehaviorSubject(null);
	currentCountryState = this.currentCountryStateSource.asObservable();

  constructor(
  	private http: HttpClient
	) {
	}

  setCountryState(name: string, code: string){
  	this.currentCountryStateSource.next({name, code: code.toLowerCase()});
	}

	clearCountryState(){
  	this.currentCountryStateSource.next(null);
	}

	hasCountryState(){
  	return this.getCountryState() != null;
	}

	getCountryState(){
  	return this.currentCountryStateSource.getValue();
	}

	getCurrentCode(){
  	return this.currentCountryStateSource.getValue() ? this.currentCountryStateSource.getValue().code: null;
	}

	getCurrentCountryName(){
  	return this.getCountryState() ? this.getCountryState().name : null;
	}

	getAllCountries(){
  	return this.http.get<{countries: Country[]}>(`${environment.api_url}/countries`);
	}
}
