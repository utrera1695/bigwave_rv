import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NotificationBigwave } from '../abstract/interfaces/notification.interface';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class NotificationService {

    constructor(private http: HttpClient) {}

    getAll(): Observable<HttpResponse<NotificationBigwave[]>> {
        return this.http.get<NotificationBigwave[]>(`${environment.api_url}notifications/all`,{observe: 'response'});
    }

    getAllConfigurations(): Observable<HttpResponse<any>>{
        return this.http.get<any>(`${environment.api_url}notifications/settings`,{observe: 'response'});
    }

    markAs(id: string, mark: 'read' | 'unread'): Observable<HttpResponse<NotificationBigwave>>{
        return this.http.get<NotificationBigwave>(`${environment.api_url}notifications/${id}/${mark}`,{observe: 'response'});
    }

    markAllAs(ids: string, mark: 'read' | 'unread'): Observable<HttpResponse<NotificationBigwave[]>>{
        const params: HttpParams = new HttpParams()
            .set('notifications_ids', ids);

        return this.http.post<NotificationBigwave[]>(`${environment.api_url}notifications/${mark}`, null, {observe: 'response', params: params});
    }
}
