import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BigwaveResponse, GenericClass} from '../abstract/interfaces/response.interface';
import {Observable} from 'rxjs';
import {CotizationAtributes} from '../abstract/interfaces/cotization/get-cotization.interface';

@Injectable()
export class CotizationService {

  constructor(
  	private http: HttpClient
	) { }

	getOne(id): Observable<HttpResponse<BigwaveResponse<GenericClass<CotizationAtributes>>>>{
  	return this.http.get<any>(`${environment.api_url}cotizations/${id}`, {observe: 'response'});
	}

	update(id, payload): Observable<HttpResponse<any>>{
  	return this.http.put<any>(`${environment.api_url}cotizations/${id}/update`, {cotization: payload}, {observe: 'response'});
	}
}
