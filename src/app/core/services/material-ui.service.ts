import {Injectable, Injector} from '@angular/core';
import {
	MatDialog,
	MatSnackBar,
	MatSnackBarDismiss,
	MatSnackBarHorizontalPosition,
	MatSnackBarVerticalPosition
} from '@angular/material';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {GenericMessages} from '../abstract/enums/generic-messages.enum';
import {AreYouSureDialogComponent} from '../../modules/shared-components/are-you-sure-dialog/are-you-sure-dialog.component';
import {debounceTime} from 'rxjs/operators';

@Injectable()
export class UIComponentsService {

  private onShowGenericGlobalMessageBehavior: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  onShowGenericGlobalMessage = this.onShowGenericGlobalMessageBehavior.asObservable();

	constructor(
		private snack: MatSnackBar,
		private dialog: MatDialog
	) {
	}

	/**
	 * @desc Muestra un snack
	 *
	 * @returns Observable<MatSnackBarDismiss>
	 * nos podemos suscribir a este observable si necesitamos
	 * obtener el evento cuando se cerró el snack
	 *
	 * @param message tipea un mensaje de de vizualización
	 * @param undoMsg mensaje que saldrá en la visual del botón para cerrar si no se define no saldrá el botón
	 * @param delayTime tiempo de expiración del modal por defecto tiene 2seg
	 * @param xPosition posición horizontal del snackbar
	 * @param yPosition posición vertical de snackbar
	 *
	 * @author Armando Perdomo
	 */
	showSnackNotification(
		message: string,
		undoMsg?: string,
		delayTime?: number,
		xPosition?: MatSnackBarHorizontalPosition,
		yPosition?: MatSnackBarVerticalPosition
	): Observable<void> {

    if (!message) {
      throw new Error(`message: ${message} <= debe definir message \n
            showSnackNotification('soy un bonito snack');`);
    }

    const duration = delayTime ? delayTime : 3000;
    const action = undoMsg ? undoMsg : null;
    const horizontalPosition: MatSnackBarHorizontalPosition = xPosition ? xPosition : 'end';
    const verticalPosition: MatSnackBarVerticalPosition = yPosition ? yPosition : 'top';

    return this.snack.open(message, action, {
      duration: duration,
      horizontalPosition: horizontalPosition,
      verticalPosition: verticalPosition
    }).onAction();
	}

	/**
	 * Para generar un snack con el mensaje genérico
	 * @param error Cuerpo u objeto que será dispuesto en consola como error
	 * @param describedBehavior Descripción breve para dar a conocer en consola donde se produce el error
	 */
	weirdGenericError(error?: any, describedBehavior?: string): void {
		if (error) {
			console.error(!describedBehavior ? 'Error trace::' : describedBehavior, error);
		}

		this.showSnackNotification(GenericMessages.WEIRD_ERR, GenericMessages.OK);
	}

	/**
	 * Para generar un snack con el mensaje genérico
	 * @param error Cuerpo u objeto que será dispuesto en consola como error
	 * @param describedBehavior Descripción breve para dar a conocer en consola donde se produce el error
	 */
	internalGenericError(error?: any, describedBehavior?: string): void {
		if (error) {
			console.error(!describedBehavior ? 'Error trace::' : describedBehavior, error);
		}

		this.showSnackNotification(GenericMessages.INTERNAL_ERR, GenericMessages.OK);
	}

	/**
	 * Imprime un dialog modal para advertirle al usuario de una operación
	 *
	 * @param tittle Títlo del modal
	 * @param message Mensaje que se mostrará en el cuerpo, puede contener inyecciones de html que sirvan dentro de <p>
	 * @param confirm_message Mensaje de confirmación del botón
	 * @param color_btn_confirm sufijo para el color del botón, por defecto traerá 'primary'
	 */
	showDialogNotification(tittle: string, message: string, confirm_message?: string, width?: string) {
		return this.dialog.open(AreYouSureDialogComponent, {
			data: {tittle: tittle, message: message, confirm_message: confirm_message},
			width: width || '400px'
		});
	}

  /**
   * Muestra el mensaje global dependiendo del número de tmpl
   * primero se debe proveer un número de id y un tmpl para mostrar
   * de lo contrario mostrará un mensaje en blanco
   * @important 0 será tomado como falsey
   * @param tmplNumber
   */
	showGenericGlobalMessage(tmplNumber: number){
	  this.onShowGenericGlobalMessageBehavior.next(tmplNumber);
  }

  /**
   * Forza el cerrado del mensaje global genérico
   * tomando en cuenta el 0 como falsey
   */
  forceCloseGenericGlobalMessage(){
	  this.onShowGenericGlobalMessageBehavior.next(0);
  }
}
