import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ImagesService {
  url = 'https://jsonplaceholder.typicode.com/photos';
  constructor(private http: HttpClient) {}

  getTestImages() {
    return this.http.get<any[]>(this.url);
  }
}
