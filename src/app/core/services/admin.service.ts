import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {MatSidenav} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {NavigationStart, Router} from '@angular/router';
import {Admin} from '../abstract/interfaces/admin';
import {ProfileTypes} from '../abstract/enums/profile-types.enum';
import {environment} from '../../../environments/environment';

@Injectable()
export class AdminService {
  /**
   * Fuente para obtener un valor booleano si esta en modo admin o no
   */
  protected isInAdminModeSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isInAdminMode = this.isInAdminModeSource.asObservable();

  /**
   * Si esta en modo admin se debe manejar los datos del administrador por esta fuente
   * De lo contrario podría generar errores
   */
  protected currentAdminDataSource: BehaviorSubject<Admin> = new BehaviorSubject(null);
  currentAdminData = this.currentAdminDataSource.asObservable();

  /**
   * Fuente del sidenavAdmin main donde estarán las opciones del menú admin
   */
	sidenavSource: MatSidenav;

	public currentProfileCreateName: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    // Es importante inicializar el validator del routing
    this.initRouterListenerValidator();
  }

	/**
	 * Retorna un valor booleano indicando si está en admin mode
	 */
	isInAdmin(){
		return this.isInAdminModeSource.getValue();
	}

	getCurrentAdminId(){
		return this.currentAdminDataSource.getValue().id;
	}

	getCurrentAdminType(){
		return this.currentAdminDataSource.getValue().type
	}

	getRoute(extra?: string){
		return `/admin/${this.getCurrentAdminType()}/${this.getCurrentAdminId()}` + extra;
	}

	/**
   * Escuhará los eventos del enrutador
   * Si hace un match con el path de no ser 'admin'
   * Esto indica de que ya no se encuentra en ese modo
   * por lo tanto setea los valores de las fuentes como nulos
   */
  initRouterListenerValidator() {
    this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationStart) {
          if (!event.url.includes('admin')) {
            this.clearAdminMode();
          }
        }
      }
    );
  }

  /**
   * Para inicializar el modo admin
   * (Todavía no se puede utilizar de manera forzosa)
   * @param data
   */
  setAdminMode(data: Admin) {
    this.isInAdminModeSource.next(true);
    this.currentAdminDataSource.next(data);
  }

  /**
   * Para cerrar el modo admin
   * (Todavía no se puede utilizar de manera forzosa)
   */
  clearAdminMode() {
    this.isInAdminModeSource.next(false);
    this.currentAdminDataSource.next(null);
    this.sidenavSource = null;
  }

  setSidenavSource(sidenav: MatSidenav) {
    this.sidenavSource = sidenav;
  }

  toggleSidenav() {
    return this.sidenavSource.toggle();
  }

  forceCloseSidenav() {
    return this.sidenavSource.close();
  }

	/**
	 * Para actualizar el profile (seller, independet, pyme)
	 * @param profileType tipo {ProfileTypes} para conocer el perfil a modificar
	 * @param id
	 * @param payload
	 */
  updateAdmin(profileType: ProfileTypes, id: string, payload: any) {
    const endpoint = profileType === ProfileTypes.SELLERS ? `${environment.api_url}${profileType}/current_user/update`: `${environment.api_url}${profileType}/${id}/update`;
    const payloadKeyType = profileType === ProfileTypes.SELLERS ? 'seller' : profileType === ProfileTypes.PYMES ? 'pyme' : 'independent';
    return this.http.put<any>(endpoint, { [payloadKeyType]: payload}, {observe: 'events'});
  }

	/**
	 * Si no se desea utilizar el suscriptor {currentAdminData}
	 * se puede llamar a este método
	 */
	getAdminData(): Admin{
  	return this.currentAdminDataSource.getValue();
	}


	isOwnProfile(profile_id){
		return this.http.get<boolean>(`${environment.api_url}profiles/${profile_id}/own_profile`, {observe: 'response'});
	}
}
