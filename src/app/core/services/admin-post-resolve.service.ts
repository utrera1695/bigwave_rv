import {Injectable, Injector} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {AdminService} from './admin.service';
import {ProfileTypes} from '../abstract/enums/profile-types.enum';
import {BigwaveResponse} from '../abstract/interfaces/response.interface';
import {Admin} from '../abstract/interfaces/admin';
import {environment} from '../../../environments/environment';
import {UIComponentsService} from './material-ui.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminPostResolveService implements Resolve<any> {

  constructor(
    private router: Router,
    private injector: Injector,
    private http: HttpClient,
    private adminService: AdminService
  ) { }

  /**
   * Obtiene la información del usuario admin actual
   * Los parámetros deben ser suministrados como obligatorios
   * el parámetro type hace referencia al enum {ProfileTypes}
   * el parámetro id es solo un valor numérico
   */
  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any>  {
    const params = route.params;
    const type = params['type'];
    const id = params['id'];

    // Validación de la url, si no cumple extrictamente con esto, enruta a '/'
    if ((type !== ProfileTypes.SELLERS && type !== ProfileTypes.PYMES && type !== ProfileTypes.INDEPENDENTS) || isNaN(id)) {
      this.router.navigate(['/']);
      return;
    }

    let data: BigwaveResponse<Admin>;
    try {
      data = (await this.http.get<any>(`${environment.api_url}${type}/${id}`, {observe: 'response'}).toPromise()).body;
    } catch (e) {
      const ui = this.injector.get(UIComponentsService);
      ui.internalGenericError(e);
      return this.router.navigate(['/']);
    }

    this.adminService.setAdminMode(data.data);

    return Promise.resolve();
  }
}
