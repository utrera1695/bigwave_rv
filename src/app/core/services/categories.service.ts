import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BigwaveResponse, GenericClass} from '../abstract/interfaces/response.interface';
import {Categories} from '../abstract/interfaces/categories.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(
  	private http: HttpClient
	) { }

	allCategories(){
  	return this.http.get<BigwaveResponse<GenericClass<Categories>[]>>(`${environment.api_url}profiles/categories`);
	}
}
