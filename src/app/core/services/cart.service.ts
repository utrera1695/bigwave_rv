import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { CartItem, Item, ItemOptsValues } from '../abstract/interfaces/cart-item.interface';
import { CotizationRequestToCreate } from '../abstract/interfaces/cotization/create-cotization.interface';

@Injectable()
export class CartService {

	protected cartDataSource: BehaviorSubject<CartItem[]> = new BehaviorSubject([]);
	cartData: Observable<CartItem[]> = this.cartDataSource.asObservable();

	protected onQuantityUpdateSource: Subject<boolean> = new Subject();
	onQuantityUpdate: Observable<boolean> = this.onQuantityUpdateSource.asObservable();

	protected onUpdateSource: Subject<boolean> = new Subject();
	onUpdate: Observable<boolean> = this.onUpdateSource.asObservable();

	hasToRetrieveRemoteDataSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
	hasToRetrieveRemoteData: Observable<boolean> = this.hasToRetrieveRemoteDataSource.asObservable();

	constructor(
		private http: HttpClient
	) {
		this.syncDatasourceAndLocalAndRemote();
	}

	/**
	 * Sincroniza los datos del almacenamiento local con el datasource
	 * Si no existe almacenamiento en local storage y si existe en el datasource por lo tanto se actualiza
	 *  datasource -> localstorage
	 *
	 * Si existe localstorage con datos se sincroniza con el datasource
	 * 	localstorage -> datasource
	 */
	syncDatasourceAndLocalAndRemote(dataRetrievedFromServer?: CartItem[]) {
		const localItems: CartItem[] = JSON.parse(localStorage.getItem('cart_bigwave')) || [];
		const productsDatasource = this.cartDataSource.getValue();
		const remoteItems = dataRetrievedFromServer;

		// Si viene definido la data desde el servidor puede ser [] o undefined
		if (remoteItems) {
			//Si tiene datos
			if (remoteItems.length) {
				this.updateLocalFromRemoteData(localItems, remoteItems);
				return;
			}

			//Si no tiene datos pero local si tiene
			if (!remoteItems.length && localItems.length) {
				this.onUpdateSource.next(true);
				return;
			}
		}

		// En tal caso que sea elíminado el almacenamiento local
		if (!localItems.length && productsDatasource.length) {
			const toSave: string = JSON.stringify(productsDatasource);
			localStorage.setItem('cart_bigwave', toSave);
			return;
		}

		if (localItems.length) {
			this.cartDataSource.next(localItems);
		}
	}

	/**
	 * Si viene data desde el servidor actualizamos desde la remota a la local
	 * @param localItems
	 * @param remoteItems
	 */
	updateLocalFromRemoteData(localItems: CartItem[], remoteItems: CartItem[]) {
		//Si localStorage no tiene longitud entonces no hay nada que comparar
		// y escribimos Sobre ella la remota
		if (!localItems.length) {
			const toSave = JSON.stringify(remoteItems);
			localStorage.setItem('cart_bigwave', toSave);
			this.cartDataSource.next(remoteItems);
			return;
		}

		//Ahora por cada item remoto sobrescribimos
		// Iteramos por cada item del carrito
		remoteItems.forEach(
			(cartItem) => {
				//Creamos el objeto profile_data
				const profile_data = {
					profile_id: cartItem.profile_id,
					profile_name: cartItem.profile_name,
					profile_url: cartItem.profile_url
				};
				//Iteramos entre los items y agregamos para ese profile el item
				cartItem.items.forEach(
					(item) => {
						this.addItemLocal(profile_data, item);
					}
				);
			}
		);
	}

	/**
	 * Para mandar a cotizar los productos del carrito perteneciente a un profile,
	 * pasando por parametros un array de :item_ids
	 *
	 * { "profile_id": 7, "items": [ 1 ] }
	 *
	 * @param profileId
	 * @param itemsIds
	 */
	quoteItems(data: CotizationRequestToCreate[]): Observable<HttpResponse<any>> {
		return this.http.post<any>(`${environment.api_url}/shopping_carts/multiple_quotes`, {data: data}, {observe: 'response'});
	}

	/**
	 * Obtiene el carrito de compras
	 */
	getAllDataLocal() {
		return this.cartDataSource.getValue();
	}

	/**
	 * Activa el suscriptor para las actualizaciones del dataSource controlado por el usuario
	 *
	 * Al ejecutar esta función debería actualizar la data remota en el llamado del suscriptor
	 */
	triggerUpdateSource() {
		this.onUpdateSource.next(true);
	}

	getDataByIdProfile(idProfile: number): CartItem {
		const localItems: CartItem[] = JSON.parse(localStorage.getItem('cart_bigwave')) || []; //Obtenemos la información de ls o [] en caso de ser null
		let profileData: CartItem;

		localItems.forEach(
			(cartItem) => {
				if (cartItem.profile_id === idProfile) {
					profileData = cartItem;
					return;
				}
			}
		);

		return profileData;
	}


	/**
	 * Agrega el producto al carrito de compras al almacenamiento local y luego sincroniza el datasource
	 *
	 * @param profileData la data del perfil
	 * @param item la información del producto
	 */
	addItemLocal(profileData: {profile_name: string, profile_url: string, profile_id: number}, item: Item) {
		const localItems: CartItem[] = JSON.parse(localStorage.getItem('cart_bigwave')) || []; //Obtenemos la información de ls o [] en caso de ser null

		if (localItems.filter((v) => v.profile_id === profileData.profile_id)[0]) {//Consultamos si existe el profile para acumular
			localItems.forEach(//Si existe mandamos a iterar para agregar al carrito
				(data) => {
					if (data.profile_id === profileData.profile_id) {//Condicionando que si hacen match

						//Consultamos para ver si ya existe el producto dentro de la lista
						const innerItem = data.items.filter((i) => i.product_id === item.product_id)[0];
						if (innerItem) {
							let hasUpdatedValue = false;

							data.items.forEach(
								(i) => {
									//Obtenemos los valores de:
									const customFieldIds: number[] = i.custom_field_ids;
									const itemOptsValues: ItemOptsValues[] = i.option_values;
									//Luego validamos que tengan match los attr del item que entra con los del consultado
									const hasSameCustomFieldsIds: boolean = customFieldIds.every((cf) => item.custom_field_ids.includes(cf));
									const hasSameOptsValues: boolean = itemOptsValues.every((iov) => Boolean(item.option_values.filter((i) => i.value === iov.value)[0])); // No había otra manera mas optima bro

									//Si existe esa concordancia
									if (i.product_id === item.product_id && hasSameCustomFieldsIds && hasSameOptsValues) {
										i.quantity = item.quantity + i.quantity > i.stock ? i.stock : i.quantity + item.quantity; //Solamente procedemos a actualizar la cantidad
										hasUpdatedValue = true;
										return; //Finalizamos el bucle
									}
								}
							);

							if (!hasUpdatedValue) {
								data.items.push(item);
								return; //Finalizamos el bucle
							}
						} else {
							data.items.push(item);
							return; //Finalizamos el bucle
						}
					}
				}
			);

			localStorage.setItem('cart_bigwave', JSON.stringify(localItems));
			this.syncDatasourceAndLocalAndRemote();
			return;
		}

		//Si no existe el profile en la lista del carrito de compras por lo tanto crea un nuevo registro
		const toSave: CartItem = {
			profile_name: profileData.profile_name,
			profile_id: profileData.profile_id,
			profile_url: profileData.profile_url,
			items: [item]
		};

		localItems.push(toSave);
		localStorage.setItem('cart_bigwave', JSON.stringify(localItems));
		this.syncDatasourceAndLocalAndRemote();
	}

	updateQuantity(profile_id: number, item: Item, increase?: boolean) {
		const localItems: CartItem[] = JSON.parse(localStorage.getItem('cart_bigwave')) || []; //Obtenemos la información de ls o [] en caso de ser null

		localItems.forEach(//Iteramos
			(cartItem) => {
				if (cartItem.profile_id === profile_id) {//Condicionando si hacen match el profileid
					//Iteramos en los items del profile
					cartItem.items.forEach(
						(i) => {
							//Obtenemos los valores de:
							const customFieldIds = i.custom_field_ids;
							const itemOptsValues = i.option_values;
							//Luego validamos que tengan match los attr del item que entra con los del consultado
							const hasSameCustomFieldsIds = customFieldIds.every((cf) => item.custom_field_ids.includes(cf));
							const hasSameOptsValues = itemOptsValues.every((iov) => Boolean(item.option_values.filter((i) => i.value === iov.value)[0])); // No había otra manera mas optima bro

							//Si existe esa concordancia
							if (i.product_id === item.product_id && hasSameCustomFieldsIds && hasSameOptsValues) {
								// Si "increase" entonces asigna el valor anterior +1 si la cantidad actual es menor que el stock
								// de lo contrario verifica que sea > 0 si no es el valor anterior -1 o 0
								i.quantity = increase && (i.quantity <= i.stock) ? ++i.quantity : i.quantity > 1 ? --i.quantity : 1;
								return; //Finalizamos el bucle
							}
						}
					);
					return; //Finalizamos el bucle
				}
			}
		);

		localStorage.setItem('cart_bigwave', JSON.stringify(localItems));
		this.syncDatasourceAndLocalAndRemote();
		this.onQuantityUpdateSource.next(true);
	}

	deleteItem(profile_id: number, item: Item) {
		const localItems: CartItem[] = JSON.parse(localStorage.getItem('cart_bigwave')) || []; //Obtenemos la información de ls o [] en caso de ser null

		localItems.forEach(//Iteramos
			(cartItem, pindex) => {
				if (cartItem.profile_id === profile_id) {//Condicionando si hacen match el profileid
					//Iteramos en los items del profile
					cartItem.items.forEach(
						(i, index) => {
							//Obtenemos los valores de:
							const customFieldIds = i.custom_field_ids;
							const itemOptsValues = i.option_values;
							//Luego validamos que tengan match los attr del item que entra con los del consultado
							const hasSameCustomFieldsIds = customFieldIds.every((cf) => item.custom_field_ids.includes(cf));
							const hasSameOptsValues = itemOptsValues.every((iov) => Boolean(item.option_values.filter((i) => i.value === iov.value)[0])); // No había otra manera mas optima bro

							//Si existe esa concordancia
							if (i.product_id === item.product_id && hasSameCustomFieldsIds && hasSameOptsValues) {
								// Hacemos splice para que elimine el producto del arreglo
								cartItem.items.splice(index, 1);
								if (!cartItem.items.length) {
									localItems.splice(pindex, 1);
								}
								return; //Finalizamos el bucle
							}
						}
					);
					return; //Finalizamos el bucle
				}
			}
		);

		if (!localItems.length) {
			this.deleteAll();
			return;
		}

		localStorage.setItem('cart_bigwave', JSON.stringify(localItems));
		this.syncDatasourceAndLocalAndRemote();
	}

	deleteByProfile(profile_id: number) {
		const localItems: CartItem[] = JSON.parse(localStorage.getItem('cart_bigwave')) || []; //Obtenemos la información de ls o [] en caso de ser null

		localItems.forEach(
			(cartItem, index) => {
				if (cartItem.profile_id === profile_id) {//Condicionando si hacen match el profileid
					// Eliminamos el profile y a su ves son elíminados los productos
					localItems.splice(index, 1);
					return; //Finalizamos el bucle
				}
			}
		);

		if (!localItems.length) {
			this.deleteAll();
			return;
		}

		localStorage.setItem('cart_bigwave', JSON.stringify(localItems));
		this.syncDatasourceAndLocalAndRemote();
	}

	deleteAll() {
		localStorage.removeItem('cart_bigwave');
		this.cartDataSource.next([]);
		this.syncDatasourceAndLocalAndRemote();
		this.triggerUpdateSource();
	}

}
