import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class GlobalUiService {

	protected globalLoadingSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
	globalLoading = this.globalLoadingSource.asObservable();

  constructor() { }

	/**
	 * Fuerza el global loading a abrir
	 */
	showGlobalBarLoading(){
  	this.globalLoadingSource.next(true);
	}

	/**
	 * Fuerza el global loading a cerrar
	 */
	hideGlobalBarLoading(){
  	this.globalLoadingSource.next(false);
	}

	/**
	 * Togglea el global loading dependiendo de su estado actual
	 * setea el valor contrario
	 * show/hide
	 */
	toggleGlobalBarLoading(){
  	this.globalLoadingSource.next(!this.globalLoadingSource.getValue());
	}
}
