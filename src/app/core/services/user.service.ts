import {Injectable, Injector} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {BehaviorSubject} from 'rxjs';
import {AngularTokenService} from 'angular-token';
import {ActionCableBigwaveService} from './action-cable-bigwave.service';
import {CartService} from './cart.service';
import {User} from '../abstract/interfaces/user.interface';
import {SocialUser} from 'angularx-social-login';
import {OmniauthLoginResponse} from '../abstract/interfaces/omniauth-login.interface';

@Injectable()
export class UserService  {

  private userLoggedInState: BehaviorSubject<boolean> = new BehaviorSubject(false);
  userLoggedIn = this.userLoggedInState.asObservable();

  constructor(
  	private http: HttpClient,
		private injector: Injector
	) {
  	const token = injector.get(AngularTokenService);
		this.userLoggedInState.next(token.userSignedIn());
  }

  login(user){
    this.setUserLoggedState(true);
    localStorage.setItem('user', JSON.stringify(user));
    const actionCable = this.injector.get(ActionCableBigwaveService);
    const cartService = this.injector.get(CartService);
    actionCable.createCableInstance();
    cartService.hasToRetrieveRemoteDataSource.next(true);
  }

  loginOmniauth(body: SocialUser, provider: 'facebook' | 'google'){
    return this.http.post<OmniauthLoginResponse>(`${environment.api_url}users/omniauth/${provider}`, body);
  }

  setUserLoggedState(logged: boolean){
  	this.userLoggedInState.next(logged);
	}

	resetPassword(data){
    return this.http.post(`${environment.api_url}auth/password/reset`, data);
  }

  getCurrentUserDataInStorage(){
    const user = localStorage.getItem('user');
    if(!user){
      return null;
    }
    return JSON.parse(user) as User;
  }

  setUserDataInStorage(user: User){
    localStorage.setItem('user',JSON.stringify(user));
  }

  updateNickNameDataStorage(nickname){
    const user = this.getCurrentUserDataInStorage();
    user.nickname = nickname;
    this.setUserDataInStorage(user);
  }

  updateStorageData(user: User){
    localStorage.setItem('user', JSON.stringify(user));
  }

  updateCurrentUserData(data){
    return this.http.put(`${environment.api_url}auth`, data, {reportProgress: true, observe: 'events'});
  }

  regeneratePassword(data){
    return this.http.post(`${environment.api_url}auth/password/regenerate`, data);
  }

  validateUsername(nickname){
    const params = new HttpParams()
      .set('nickname', nickname);
    return this.http.get(`${environment.api_url}users/validates/nickname`, {params});
  }

  updateNickname(data){
    return this.http.put(`${environment.api_url}users/update/nickname`, data);
  }
}
