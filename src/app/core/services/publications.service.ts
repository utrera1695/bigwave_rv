import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BigwaveResponse, GenericClass} from '../abstract/interfaces/response.interface';
import {Post} from '../abstract/interfaces/post';

@Injectable({
  providedIn: 'root'
})
export class PublicationsService {

  constructor(
  	private http: HttpClient
	) { }

	getAll(){
  	return this.http.get<BigwaveResponse<GenericClass<Post>[]>>(`${environment.api_url}posts`);
	}

	getHistoryByProfile(profileId){
		return this.http.get<BigwaveResponse<GenericClass<Post>[]>>(`${environment.api_url}posts/profile/${profileId}/history`);
	}

	getOne(id){
		return this.http.get<any>(`${environment.api_url}posts/${id}`);
	}

	add(body){
		return this.http.post<any>(`${environment.api_url}posts/create`, body);
	}

	update(id, body){
		return this.http.put<any>(`${environment.api_url}posts/${id}/update`, body);
	}

	delete(id){
		return this.http.delete<any>(`${environment.api_url}posts/${id}/destroy`);
	}
}
