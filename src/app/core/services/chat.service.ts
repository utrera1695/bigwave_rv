import {Injectable, Injector} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {API_ROUTES} from '../../app.constants';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Channel} from 'angular2-actioncable';
import {ActionCableBigwaveService} from './action-cable-bigwave.service';
import {filter, takeUntil} from 'rxjs/operators';
import {CableStatus} from '../abstract/enums/cable-status.enum';
import {Conversation, Conversations, ConversationsUser, Cotization, Message} from '../abstract/interfaces/messages.interface';
import {MatSidenav} from '@angular/material';
import {SocketMetadata} from '../abstract/interfaces/chat/conversation-metadata-type';
import {ConversationMetadataType} from '../abstract/enums/conversation-metadata-type.enum';
import {CotizationTransactionType} from '../abstract/interfaces/chat/cotization-transaction-type';
import {UpdatedChatOpen} from '../abstract/interfaces/cotization/updated-chat-open.interface';
import {UIComponentsService} from './material-ui.service';
import {BigwaveUtils} from '../abstract/class/BigwaveUtils.class';
import {AdminService} from './admin.service';


@Injectable()
export class ChatService {

	/**
	 * Fuente de las conversaciones que tiene el usuario para el componente de message-profile
	 */
  protected displayedConversationsSource: BehaviorSubject<ConversationsUser> = new BehaviorSubject(null);
  displayedConversations = this.displayedConversationsSource.asObservable();

	/**
	 * Fuente de los chats abiertos para el componente chat-list
	 */
	protected chatListSource: BehaviorSubject<Conversation[]> = new BehaviorSubject([]);
  chatList = this.chatListSource.asObservable();

	/**
	 * Fuente del chat abierto por el usuario para el componente chat-container
	 */
	protected currentChatOpenedSource: BehaviorSubject<Conversation> = new BehaviorSubject(null);
  currentChatOpened = this.currentChatOpenedSource.asObservable();

  protected isConnectedToChatSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isConnectedToChat = this.isConnectedToChatSource.asObservable();

	/**
	 * Fuente del sidenavAdmin main donde estarán las opciones del menú admin
	 */
	chatSidenavSource: MatSidenav;

  channel: Channel;
  private cableService: ActionCableBigwaveService;
  private ui: UIComponentsService;
  private adminService: AdminService;
  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.cableService = this.injector.get(ActionCableBigwaveService);
    this.ui = this.injector.get(UIComponentsService);
    this.adminService = this.injector.get(AdminService);
    this.initSuscriptions();
  }

	/**
	 * Fuente para el sidenavAdmin del chat
	 * @param sidenav
	 */
	setSidenavSource(sidenav: MatSidenav) {
		this.chatSidenavSource = sidenav;
	}

	/**
	 * Toggle para el sidenavAdmin del chat
	 */
	toggleSidenav() {
		return this.chatSidenavSource.toggle();
	}

  /**
   * Suscripción inicial para atender a los eventos de conexión/desconexión del cable
   * La misma tiene como concecuencia el control y manejo en la sesión.
   */
  protected initSuscriptions(){
    this.cableService.onInstance.subscribe(
      (status) => {
        console.warn('status cable para -> *Chat*', status);
        if (status === CableStatus.INSTANCED) {

          this.channel = this.cableService.getActualCable().channel('ConversationChannel');
          this.onRecieveSubscription();
          this.onConnectedSubscription();
        }

        // Importante vaciar las fuentes para evitar que al cerrar sesión no sigan llenas
        if (status === CableStatus.DESTROYED){
        	this.emptyAllSources();
				}
      }
    );
  }

   /**
   * Subscripción al momento de recibir datos desde el socket
   */
  protected onRecieveSubscription() {
    this.channel.received().pipe(
      takeUntil(this.cableService.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED))),//es fundamental tomar el takeUntil() mejor que unsuscribe
    ).subscribe(
      (data: SocketMetadata<any, ConversationMetadataType>) =>  {
				console.log('Channel data conversaciones ->', data);
				this.handleDataRecievedByChannel(data);
      }
    );
  }

	/**
	 * Subscripción que escucha los eventos al conectarse con el canal
	 * Al conectarse llama a @function currentUserConversationsAction()
	 * para obtener la lista de mensajes actuales
	 * Y llama a @function onDisconectSubscription()
	 * Que es la suscripción para cuando se desconecta el channel
	 */
	protected onConnectedSubscription() {
		this.channel.connected().pipe(
			takeUntil(this.cableService.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
		).subscribe(
			() => {
				this.isConnectedToChatSource.next(true);
				this.allOpenChatsAction();
				this.onDisconectSubscription();
			}
		);
	}

	/**
	 * Subscripción que escucha las desconexiones, para tener un manejo mas apropiado
	 * ....
	 */
	protected onDisconectSubscription() {
		this.channel.disconnected().pipe(
			takeUntil(this.cableService.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
		).subscribe(
			(willAttempReconect) => {
				console.warn('was disconected channel body_messages', willAttempReconect);
				this.isConnectedToChatSource.next(false);
			}
		);
	}

  protected handleDataRecievedByChannel(data: SocketMetadata<any, ConversationMetadataType>){
  	switch (data.type) {
			case ConversationMetadataType.CURRENT_USER_CONVERSATIONS:
				this.handleCurrentUserConversations(data.body as Conversations);
				break;
			case ConversationMetadataType.OWN_PROFILE_CONVERSATIONS:
				this.handleOwnProfileConversations(data.body as Conversations);
				break;
			case ConversationMetadataType.UPDATE_COTIZATION:
				this.handleUpdateCotization(data.body as CotizationTransactionType);
				break;
			case ConversationMetadataType.DESTROY_CONVERSATION:
				this.handleDeleteConversation(data.body as CotizationTransactionType);
				break;
			case ConversationMetadataType.NEW_CONVERSATION:
				this.handleNewConversation(data.body as Conversation);
				break;
			case ConversationMetadataType.NEW_COTIZATION:
				this.handleNewConversation(data.body as Conversation);
				break;
			case ConversationMetadataType.MEW_MESSAGE:
				this.handleNewMessage(data.body as Message);
				break;
			case ConversationMetadataType.UPDATE_OPEN_CONVERSATION:
				this.handleOpenChat(data.body as UpdatedChatOpen);
				break;
			case ConversationMetadataType.GET_ALL_OPEN_CHATS:
				this.handleAllOpenChats(data.body.conversations as Conversation[]);
				break;
			case ConversationMetadataType.CHANGED_COTIZATION:
				this.handleChangedCotization(data.body);
				break;
			default:
				console.error('Error al identificar el tipo de datos -> ConversationsChannel', data.type);
		}

	}

	/**
	 * Inserta las conversaciones del usuario
	 * @param data
	 */
	protected handleCurrentUserConversations(data: Conversations){
		const conversations: ConversationsUser = data.conversations;
		this.displayedConversationsSource.next(conversations);
	}

	/**
	 * Inserta las conversaciones del profile
	 * @param data
	 */
	protected handleOwnProfileConversations(data: Conversations){
		const conversations: ConversationsUser = data.profile_conversations[0];
		this.displayedConversationsSource.next(conversations);
	}

	/**
	 * Actualiza la fuente de conversaciones al actualizar el estatus de la cotización
	 * @param data
	 */
	protected handleUpdateCotization(data: CotizationTransactionType){
  	const conversations = this.getAllCurrentDisplayedConversations();
  	const conversationUpdated = data.conversation;

  	const inDisplayedCotizationsIndex = this.inDisplayedCotizationsIndex(conversationUpdated.id);
  	if(inDisplayedCotizationsIndex != -1){
  		conversations.cotizations_conversations[inDisplayedCotizationsIndex].cotization = conversationUpdated.cotization;
			this.ui.showSnackNotification(`Se ha cambiado el estatus de la cotización a ${BigwaveUtils.translateCotizationStatus(conversationUpdated.cotization.stage)}`, 'OK');
			this.displayedConversationsSource.next(conversations);
		}
	}

	/**
	 * Actualiza la fuente de conversaciones al eliminar
	 * @param data
	 */
	protected handleDeleteConversation(data: CotizationTransactionType){
		const inChatListIndex = this.inChatListIndex(data.conversation.id);
		if(inChatListIndex != -1){
			const allChatList = this.getAllChatsList();
			allChatList.splice(inChatListIndex, 1);
			this.chatListSource.next(allChatList);

			const currentChatOpened = this.getCurrentChatOpened();
			if(currentChatOpened && currentChatOpened.id === data.conversation.id){
				this.clearCurrentOpenedChat();
			}
		}

		const conversations = this.getAllCurrentDisplayedConversations();

		const inDisplayedConversationsIndex = this.inDisplayedConversationsIndex(data.conversation.id);
		if(inDisplayedConversationsIndex != -1){
			conversations.user_conversations.splice(inDisplayedConversationsIndex, 1);
			this.displayedConversationsSource.next(conversations);
		}

		const inDisplayedCotizationsIndex = this.inDisplayedCotizationsIndex(data.conversation.id);
		if(inDisplayedCotizationsIndex != -1){
			conversations.cotizations_conversations.splice(inDisplayedCotizationsIndex, 1);
			this.displayedConversationsSource.next(conversations);
		}
	}

	/**
	 * Actualiza las fuentes de las conversaciones
	 * @param data
	 */
	protected handleNewConversation(data: Conversation){
  	const conversations = this.getAllCurrentDisplayedConversations();
  	const conversationToPush = data;

		if(conversationToPush.sender_messageable.type === 'Profile' && !this.adminService.isInAdmin()){
  		// Determinamos si la conversation es para un profile
			// Si no está en modo admin no pushea la conversation
  		return;
		}

  	if(conversations){
			if (conversationToPush.type_conversation === 'cotization'){
				conversations.cotizations_conversations.unshift(conversationToPush);
			}else{
				conversations.user_conversations.unshift(conversationToPush);
			}

			this.displayedConversationsSource.next(conversations);
		}
	}

	/**
	 * Actualiza todas las fuentes de conversaciones
	 * al llegar el mensaje
	 *
	 * Depende de la fuente actual de los mensajes que se esté manejando
	 * - Si la fuente de mensajes es de profile y el mensaje es para ese profile
	 * la data será agregada, visualmente en tiempo real, de lo contrario
	 * no agregará nada.
	 * Lo mismo pasa para la fuente de mensajes del usuario común
	 *
	 * @param data
	 */
	protected handleNewMessage(data: Message){
  	const message = data;

  	const inChatListIndex = this.inChatListIndex(message.conversation_id);
  	if(inChatListIndex !== -1){
			const chatList = this.getAllChatsList();
			chatList[inChatListIndex].body_messages.push(message);
			this.chatListSource.next(chatList);

			const currentChatOpened = this.getCurrentChatOpened();

			if(currentChatOpened && currentChatOpened.id === message.conversation_id){
				currentChatOpened.body_messages.push(message);
				this.currentChatOpenedSource.next(currentChatOpened);
			}
		}

		const conversations = this.getAllCurrentDisplayedConversations();

		const inCotizationDisplayedIndex = this.inDisplayedCotizationsIndex(message.conversation_id);
  	if(inCotizationDisplayedIndex != -1){
  		conversations.cotizations_conversations[inCotizationDisplayedIndex].body_messages.push(message);
  		this.displayedConversationsSource.next(conversations);
  		return
		}

  	const inConversationsDisplayedIndex = this.inDisplayedConversationsIndex(message.conversation_id);
  	if(inConversationsDisplayedIndex != -1){
  		conversations.user_conversations[inConversationsDisplayedIndex].body_messages.push(message);
  		this.displayedConversationsSource.next(conversations);
		}

	}

	/**
	 * Actualiza la fuente de la lista de chats
	 * si el socket responde {update_open_conversation}
	 * cambia el estado {open} a true o false
	 * @param updatedChatOpen
	 */
	protected handleOpenChat(updatedChatOpen: UpdatedChatOpen){
		const chatList = this.getAllChatsList();
		const currentOpened = this.getCurrentChatOpened();
		const inChatListIndex = this.inChatListIndex(updatedChatOpen.id);// Si el que viene por socket está en la lista

		if(inChatListIndex !== -1){
			if(!updatedChatOpen.open){// el estado de {open} es false
				chatList.splice(inChatListIndex, 1);// eliminamos el chat de la lista
				// si existe un chat abierto && si el chat abierto es el mismo del socket && si el {open} es false
				if (currentOpened && currentOpened.id === updatedChatOpen.id && !updatedChatOpen.open){
					this.clearCurrentOpenedChat(); // Cierra el chat abierto
				}
				this.chatListSource.next(chatList);
			}else{ // Si viene en true y está en la lista solo retornamos y no hacemos mas nada
				return;
			}
		}else{// Si no está en la lista se intenta agregar agrega de los chats que están siendo mostrados
			const conversations = this.getAllCurrentDisplayedConversations();
			if(!conversations){
				return;
			}

			const allConversations = conversations.user_conversations.concat(conversations.cotizations_conversations);
			// Buscamos en la lsita de conversations si existe
			const conversation = allConversations.filter(c =>  c.id === updatedChatOpen.id)[0];
			if(!conversation){
				throw new Error("Hubo un error al agregar el chat, no fue encontrado en la lista de conversations");
			}
			// Agregamos el chat al inicio de la lista
			chatList.unshift(BigwaveUtils.cloneObj(conversation));
			this.setChatOpenedSource(conversation);
			this.chatSidenavSource.open();
			this.chatListSource.next(chatList);
		}
	}

	protected handleAllOpenChats(data: Conversation[]){
		this.chatListSource.next(data);
	}

	/**
	 * Actualiza el objeto cotization en todas las fuentes
	 * si existe la conversation
	 * @param data
	 */
	protected handleChangedCotization(data: { conversation_id: number; cotization: Cotization }){
		const inChatListIndex = this.inChatListIndex(data.conversation_id);
		if(inChatListIndex !== -1){
			const chatList = this.getAllChatsList();
			chatList[inChatListIndex].cotization = data.cotization;
			this.chatListSource.next(chatList);

			const currentChat = this.getCurrentChatOpened();

			if (currentChat && currentChat.id === data.conversation_id){
				currentChat.cotization = data.cotization;
				this.currentChatOpenedSource.next(currentChat);
			}
		}

		const inDisplayedCotizationsIndex = this.inDisplayedCotizationsIndex(data.conversation_id);
		if(inDisplayedCotizationsIndex !== -1){
			const displayedConversations = this.getAllCurrentDisplayedConversations();
			displayedConversations.cotizations_conversations[inDisplayedCotizationsIndex].cotization = data.cotization;
			this.displayedConversationsSource.next(displayedConversations);
		}

	}

	/**
	 * Obtiene el index de un chat en la {chatListSource}
	 * @param id
	 */
	inChatListIndex(id): number{
		return this.getAllChatsList().findIndex(cl => cl.id === id);
	}

	/**
	 * Obtiene el index de un chat en la {displayedConversationsSource.user_conversations}
	 * para las conversaciones comunes
	 * @param id
	 */
	inDisplayedConversationsIndex(id): number{
		return !this.getAllCurrentDisplayedConversations() ? -1 : this.getAllCurrentDisplayedConversations().user_conversations.findIndex(c => c.id === id);
	}

	/**
	 * Obtiene el index de un chat en la {displayedConversationsSource.cotizations_conversations}
	 * para las cotizaciones
	 * @param id
	 */
	inDisplayedCotizationsIndex(id): number{
		return !this.getAllCurrentDisplayedConversations() ? -1 : this.getAllCurrentDisplayedConversations().cotizations_conversations.findIndex(c => c.id === id);
	}

	/**
	 * Obtiene todas las conversations que están siendo vistas
	 * por el usuario
	 *
	 * -si el mismo está en modo admin, las conversaciones serán
	 * de ese mismo admin
	 * -si está en modo user, las conversaciones serán de ese user
	 *
	 * Todos los mensajes y conversaciones nuevos que llegarán en tiempo
	 * real, serán actualizadas en esta fuente
	 */
	getAllCurrentDisplayedConversations(): ConversationsUser{
		return this.displayedConversationsSource.getValue();
	}

	getAllChatsList(): Conversation[]{
  	return this.chatListSource.getValue();
	}

	getCurrentChatOpened(): Conversation{
  	return this.currentChatOpenedSource.getValue();
	}

	/**
	 * Agrega un chat a la lista de chats
	 * si el chat ya existe dentro de la lista
	 * lo setea a la fuenta del currentOpenedChat
	 * y abre el sidenav de los chats
	 * @param chat
	 */
	addChatToList(chat: Conversation){
		const chats = this.chatListSource.getValue();

		// Si existe el chat en la lista solo se resalta y se abre el sidenav
		if (chats.filter(c =>  c.id === chat.id)[0]){
			this.setChatOpenedSource(chat);
			this.chatSidenavSource.open();
			return;
		}

		this.updateOpenConversation(chat.id, true, chat.sender_messageable.type === 'Profile' ? chat.sender_messageable.id: null);
	}

	/**
	 * Abre un chat
	 * Si el existe un current se compara el id con el entrante
	 * si son el mismo solo se cierra el current
	 * @param chat
	 */
	setOpenedChat(chat: Conversation){
		const current = this.currentChatOpenedSource.getValue();

		if (current && current.id === chat.id){
			this.clearCurrentOpenedChat();
			return;
		}

		this.setChatOpenedSource(chat);
	}

	/**
	 * Agregamos un chat a las fuentes de {currentChatOpenedSource} y {chatListSource}
	 * de manera forzosa, también se manda a ejecutar el 'open' vía socket
	 * @param chat
	 */
	addOpenedChatForced(chat: Conversation){
		const allChatsList = this.getAllChatsList();

		// Preguntamos si ya fue agregado a la lista de chats
		// Esto se debe a que pudo haber abierto el chat de otra manera
		const isInChatListIndex = this.inChatListIndex(chat.id);
		if(isInChatListIndex !== -1){
			// Si hay un chat abierto
			const openedChat = this.getCurrentChatOpened();
			if(openedChat && openedChat.id === chat.id){
				// no hacemos nada, solo abrimos el container
				this.chatSidenavSource.open();
				return;
			}
			// Seteamos solo el chat a abierto
			this.setChatOpenedSource(chat);
			this.chatSidenavSource.open();
			return;
		}
		// Agregamos a las correspondidas fuentes el chat
		allChatsList.unshift(chat);
		this.setChatOpenedSource(chat);
		this.chatSidenavSource.open();
		this.chatListSource.next(allChatsList);
		this.updateOpenConversation(chat.id, true, chat.sender_messageable.type === 'Profile' ? chat.sender_messageable.id : null);
	}

	/**
	 * Setea un nuevo chat en el contenedor
	 * Clona el objeto para evitar duplicados de memoria
	 * @param chat
	 */
	setChatOpenedSource(chat: Conversation){
		this.currentChatOpenedSource.next(BigwaveUtils.cloneObj(chat));
	}

	/**
	 * Elimina el chat que está abierto
	 */
	clearCurrentOpenedChat(){
		this.currentChatOpenedSource.next(null);
	}

	/**
	 * Elimina lo de la sesión actual
	 */
	emptyAllSources(){
		this.chatListSource.next([]);
		this.displayedConversationsSource.next(null);
		this.currentChatOpenedSource.next(null);
	}

	/**
   * Pide la lista de mensajes del usuario actual
   */
  currentUserConversationsAction(){
    this.channel.perform('current_user_conversations');
  }

  /**
	 * Pide todas las conversations de un profile
	 * O de varios profile dependiendo de la información que se mande a pedir
	 * por parámetro {profile_ids}
	 */
  ownUserProfileConversationsAction(from?: number[]){
    if(from){
      this.channel.send({action: 'own_profiles_conversations', profile_ids: from});
      return;
    }

    this.channel.perform('own_profiles_conversations');
  }

	/**
	 * Actualiza el estatus de la cotización Perdido/Vendido
	 * @param id
	 * @param stage
	 * @param currentable_id
	 */
  updateCotizationAction(id: string, stage: 'lost' | 'sold', currentable_id?: number){
  	if(currentable_id){
			this.channel.send({action: 'update_cotization', cotization_id: id, stage: stage, currentable_id });
			return;
		}

    this.channel.send({action: 'update_cotization', cotization_id: id, stage: stage });
  }

	/**
	 * Elimina la conversación de manera lógica
	 * @param id
	 * @param currentable_id
	 */
  destroyConversationAction(id: string, currentable_id: number){
  	if(currentable_id){
			this.channel.send({action: 'destroy_conversation', conversation_id: id, currentable_id });
			return;
		}
    this.channel.send({action: 'destroy_conversation', conversation_id: id });
  }

  sendMessage(body: string, conversation_id: number, messageable_type: string, messageable_id: number){
  	//this.channel.send({action: "create_message", message :{ body, conversation_id, messageable_type, messageable_id}})
  	this.channel.send({action: 'create_message', body, conversation_id, messageable_type, messageable_id});// TODO
	}

	updateOpenConversation(conversation_id: number, open: boolean, currentable_id?: number){
  	if(currentable_id){
			this.channel.send({action: 'update_open_conversation', conversation_id, open, currentable_id});
			return;
		}

  	this.channel.send({action: 'update_open_conversation', conversation_id, open});
	}

	allOpenChatsAction(){
  	this.channel.perform('get_all_open_chats');
	}

}
