import {CreateProfileComponent} from '../../modules/web/create-profile/create-profile.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Injectable} from '@angular/core';
import {ContactFormComponent} from '../../modules/web/contact-form/contact-form.component';
import {LoginComponent} from '../../modules/shared-components/login/login.component';
import {RegisterUserComponent} from '../../modules/shared-components/register-user/register-user.component';
import {SelectCountryComponent} from '../../modules/shared-components/select-country/select-country.component';
import {Actions} from '../abstract/enums/actions.enum';
import {ProfileType} from '../abstract/enums/profile-type.enum';
import {ProfileTypes} from '../abstract/enums/profile-types.enum';

@Injectable({
  providedIn: 'root'
})
export class ModalsService {

  constructor(
    public dialog: MatDialog
  ) {
  }

  openCreateProfileDialog(profileType: ProfileTypes) : MatDialogRef<CreateProfileComponent,any> {
    return this.dialog.open(CreateProfileComponent, {
      width: '800px',
      data: profileType,
      disableClose: true
    });
  }

  openLoginDialog (action?: Actions): MatDialogRef<LoginComponent> {
    return this.dialog.open(LoginComponent, {
      data: action,
      width: '350px'
    });
  }

  openRegisterDialog(action?: Actions): MatDialogRef<RegisterUserComponent> {
    return this.dialog.open(RegisterUserComponent, {
      data: action,
      width: '400px'
    });
  }

  openContactoDialog() : MatDialogRef<ContactFormComponent,any> {
    return this.dialog.open(ContactFormComponent, {
      width: '400px'
    });
  }

  openSelectCountry() : MatDialogRef<SelectCountryComponent,any>{
    return this.dialog.open(SelectCountryComponent, {
      width: '400px',
      disableClose: true
    });
  }

}
