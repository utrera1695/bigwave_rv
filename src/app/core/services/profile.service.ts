import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProfileTypes} from '../abstract/enums/profile-types.enum';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
  	private http: HttpClient
	) { }

	own(type: ProfileTypes){
		return this.http.get(`${environment.api_url}${type}/own`);
	}

	delete(type: ProfileTypes, id, pass){
  	const url = type === ProfileTypes.SELLERS ? 'current_user/destroy': `${id}/destroy`;
  	return this.http.put(`${environment.api_url}${type}/${url}`, pass);
	}

	create(type: ProfileTypes, body){
  	return this.http.post(`${environment.api_url}${type}/create`, body);
	}

	follow(type: ProfileTypes, id){
  	return this.http.get(`${environment.api_url}current_user/follow/${type}/${id}`);
	}

	unfollow(type: ProfileTypes, id){
  	const body = {
			unfollow: { profile_id: id }
		};
  	return this.http.post(`${environment.api_url}current_user/unfollow/${type}/${id}`, body);
	}

	isFollowing(type: ProfileTypes, id){
  	return this.http.get<boolean>(`${environment.api_url}profiles/${type}/${id}/followed`)
	}
}
