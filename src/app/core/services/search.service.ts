import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {GlobalSearchData, GlobalSearchQuery} from '../abstract/interfaces/globar-search-data.interface';
import {ProfileGlobalSearch} from '../abstract/interfaces/profile-global-search.interface';
import {ProductGlobalSearch} from '../abstract/interfaces/product-global-search.interface';
import {BigwaveResponse} from '../abstract/interfaces/response.interface';
import {Product} from '../abstract/interfaces/product.interface';

@Injectable()
export class SearchService {
  constructor(
    private http: HttpClient
  ) { }

  searchProducts(query: GlobalSearchQuery) {
    const params = new HttpParams({fromObject: query as any});
    return this.http.get<GlobalSearchData<ProductGlobalSearch[]>>(`${environment.api_url}searchs/products`, {params});
  }

  searchProfiles(query: GlobalSearchQuery){
    const params = new HttpParams({fromObject: query as any});
    return this.http.get<GlobalSearchData<ProfileGlobalSearch[]>>(`${environment.api_url}searchs/profiles`, {params});
  }

  recommendedProducts(countries_codes: string){
    const params = new HttpParams({fromObject: {countries_codes}});
    return this.http.get<BigwaveResponse<Product[]>>(`${environment.api_url}products/sort_by_wishes`, {params})
  }

  mostRecentProfiles(){
    return this.http.get<BigwaveResponse<ProfileGlobalSearch[]>>(`${environment.api_url}profiles/most_recent`);
  }
}
