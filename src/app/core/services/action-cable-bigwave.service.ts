import {Injectable} from '@angular/core';
import {ActionCableService, Cable} from 'angular2-actioncable';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {CableStatus} from '../abstract/enums/cable-status.enum';
import {User} from '../abstract/interfaces/user.interface';

@Injectable()
export class ActionCableBigwaveService {

  protected cableInstance: Cable;
  protected onInstanceSourceStatusSource: BehaviorSubject<CableStatus> = new BehaviorSubject(CableStatus.UNDEFINED);
  onInstance: Observable<CableStatus> = this.onInstanceSourceStatusSource.asObservable();

  protected onDisconnectSubscription: Subscription;

  constructor(
    private cableService: ActionCableService
  ) {
  }

  /**
   * Los valores admitidos son correspondientes al cable status
   *
   * @param status
   */
  updateInstancedSoruce(status: CableStatus): void {
    this.onInstanceSourceStatusSource.next(status);
  }


  /**
   * Crea una instancia del ActionCable y devuelve su valor si es de utilidad
   *
   * @returns Cable
   */
  createCableInstance(): Cable {
    const client = localStorage.getItem('client');
    const token = localStorage.getItem('accessToken');
    const user = JSON.parse(localStorage.getItem('user')) as User;

    if (client && token && user) {
      const endpoint = `${environment.api_cable}?access-token=${token}&client=${client}&uid=${user.uid}`;

      this.cableInstance = this.cableService.cable(endpoint);

      this.onDisconnectSubscrption();
      this.updateInstancedSoruce(CableStatus.INSTANCED);
      return this.cableInstance;
    }

    return;
  }

  /**
   * Retorna la instancia del cable actual
   */
  getActualCable(): Cable {
    return this.cableInstance;
  }

  /**
   * Desconecta el cable
   */
  disconectCable(): void {
    this.onDisconnectSubscription.unsubscribe();
    this.updateInstancedSoruce(CableStatus.DESTROYED);
    this.cableInstance.disconnect();
  }

  /**
   * Si el subscriptor emite una respuesta es por que está desconectado por lo tanto
   * actualiza la fuente de onConnected
   */
  protected onDisconnectSubscrption(): void {
    this.onDisconnectSubscription = this.cableInstance.disconnected().subscribe(
      () => {
        console.warn('Se ha desconectado del cable');
        this.updateInstancedSoruce(CableStatus.UNDEFINED);
        this.onDisconnectSubscription.unsubscribe();
        this.cableInstance.disconnect();
      }
    );
  }
}
