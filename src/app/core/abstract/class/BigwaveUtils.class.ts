import {CotizationStatusDicc, ProfileTypesFromProfileType} from '../functions/dictionary.function';
import {CotizationStatus} from '../enums/cotization-status.enum';
import {HttpParams, HttpProgressEvent} from '@angular/common/http';
import * as moment from 'moment';
import {ProfileType} from '../enums/profile-type.enum';

export class BigwaveUtils{

	/**
	 * Para llenar el combo de los estatus de las cotizaciones
	 * iteramos el diccionario, y cuantas posiciones tenga
	 * se va agregando opciones
	 */
	public static getCotizationStatusAsArray(): CotizationStatus[]{
		const cotizationStatusKeys = [];
		const cotizationDicc = CotizationStatusDicc();

		cotizationDicc.forEach(
			(value, key) => {
				cotizationStatusKeys.push(key);
			}
		);

		return cotizationStatusKeys;
	}

	public static translateCotizationStatus(stage: CotizationStatus){
		return CotizationStatusDicc().get(stage);
	}

	public static cloneObj(from: any){
		const strObj = JSON.stringify(from);
		return JSON.parse(strObj);
	}

	public static parseObjectToParams(obj){
		let params = new HttpParams();
		for(const prop in obj){
			if(BigwaveUtils.isNullOrEmpty(obj[prop])) continue;
			params = params.append(prop, obj[prop]);
		}
		return params;
	}

	public static parseDateToParam(date){
		if(!date){
			return '';
		}
		return moment(date).format('YYYY-MM-DD').toString();
	}

	public static formatSearch(str = ''){
		return str.replace(/\s\s+/g, ' ')
	}

	public static isFormEmpty(form){
		let isEmpty = true;
		for(const prop in form){
			if (!BigwaveUtils.isNullOrEmpty(form[prop])){
				isEmpty = false;
				break;
			}
		}
		return isEmpty;
	}

	public static isNullOrEmpty(val: any){
		if(typeof val === 'boolean'){
			return false;
		}
		return !(typeof val === 'string' ? !!val.length: typeof val === 'number' ? false: !!val);
	}

	public static idsToParam(ids: number[] = []){
		return ids.toString().replace(',', '-');
	}

	public static getProgressFromEvent(ev: HttpProgressEvent){
	  return Math.round(ev.loaded / ev.total * 100)
  }

  public static profileTypeToProfileTypes(profileType: ProfileType){
	  return ProfileTypesFromProfileType().get(profileType);
  }
}
