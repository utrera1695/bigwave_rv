import {AngularTokenOptions} from 'angular-token';
import {environment} from '../../../../environments/environment';

export const ANGULAR_TOKEN_CONFIG: AngularTokenOptions = {
  apiBase: environment.api_url_at,
  apiPath: 'v1',

  signInPath: 'auth/sign_in',

  signOutPath: 'auth/sign_out',
  validateTokenPath: 'auth/validate_token',
  signOutFailedValidate: false,

  registerAccountPath: 'auth',
  deleteAccountPath: 'auth',

  updatePasswordPath: 'auth/password',
  resetPasswordPath: 'auth/password',

  oAuthBase: environment.api_url_at,
  oAuthPaths: {
    google: 'omniauth/google_oauth2',
    facebook: ''
  },
  oAuthWindowType: 'newWindow',
};
