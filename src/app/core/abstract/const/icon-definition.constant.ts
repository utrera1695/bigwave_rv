/**
 * [Los regulares deben ser llamados] as far*** (font awesome regular)
 * [Los solidos debe ser llamados] as fas*** (font awesome solid)
 * [Los brands deben ser llamads] as fab*** (font awesome brand)
 * Para definir un estandar de la iconografía
 *
 * Evita esto
 * import { fas } from '@fortawesome/free-solid-svg-icons';
 * import { far } from '@fortawesome/free-regular-svg-icons';
 * No queremos llenar el bundle de toda la iconografía de fontawesome, solo
 * los que sean necesarios para la app
 *
 * @example
 * import {faStar as fasStar} from '@fortawesome/free-solid-svg-icons'; //solid icons
 *
 * @implementation
 * <fa-icon [icon]="['fas','star']" size="lg"></fa-icon>
 * ->[prefix, iconName]<-
 *
 * @tutorial https://github.com/FortAwesome/angular-fontawesome#usage
 *
 */

import {
  faBook as fasBook,
  faSignInAlt as  fasSignInAlt,
  faPlus as fasPlus,
  faChalkboardTeacher as fasChalkboardTeacher,
  faComments as fasComments,
  faTicketAlt as fasTicketAlt,
  faClipboardList as fasClipboardList,
  faUsers as fasUsers,
  faChartPie as fasChartPie,
  faHeadset as fasHeadset,
  faQuestionCircle as fasQuestionCircle,
  faShapes as fasShapes,
  faBullhorn as fasBullhorn,
  faCog as fasCog,
  faMapMarkedAlt as fasMapMarkedAlt,
	faGlobeAmericas as fasGlobeAmericas,
	faFlag as fasFlag,
	faUserPlus as fasUserPlus,
	faUserMinus as fasUserMinus,
	faHeart as fasHeart,
	faLayerGroup as fasLayerGroup,
	faHandHoldingHeart as fasHandHoldingHeart,
	faHeartBroken as fasHeartBroken,
	faEye as fasEye,
	faTimes as fasTimes,
	faSortAmountUp as fasSortAmountUp,
	faSortAmountUpAlt as fasSortAmountUpAlt,
  faTools as fasTools
} from '@fortawesome/free-solid-svg-icons';

import {
  faCalendar as farCalendar,
	faHeart as farHeart
} from '@fortawesome/free-regular-svg-icons';

import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faGooglePlusG } from '@fortawesome/free-brands-svg-icons/faGooglePlusG';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { faInstagram } from '@fortawesome/free-brands-svg-icons/faInstagram';


import {
  faFacebook as fabFacebook,
  faTwitter as fabTwitter,
  faGooglePlusSquare as fabGooglePlusSquare,
  faGoogle as faGoogle
} from '@fortawesome/free-brands-svg-icons';

const ICONS: any = [
  fasBook, // No está en uso
  farCalendar, // No está en uso
  fabFacebook,
  fabTwitter,
  fabGooglePlusSquare,
  faGoogle,
  faFacebookF,
  faTwitter,
  faGooglePlusG,
  fasSignInAlt,
  fasPlus,
  fasChalkboardTeacher,
  fasComments,
  fasTicketAlt,
  fasClipboardList,
  fasUsers,
  fasChartPie,
  fasHeadset,
  fasQuestionCircle,
  fasShapes,
  fasBullhorn,
  fasCog,
	fasMapMarkedAlt,
	faGoogle,
	faFacebookF,
	faTwitter,
	faGooglePlusG,
	faWhatsapp,
	faInstagram,
	fasGlobeAmericas,
	fasFlag,
	fasUserMinus,
	fasUserPlus,
	fasHeart,
	farHeart,
	fasLayerGroup,
	fasHandHoldingHeart,
	fasHeartBroken,
	fasEye,
	fasTimes,
	fasSortAmountUp,
	fasSortAmountUpAlt,
  fasTools
];
export const ICONS_DEFINITIONS = ICONS;
