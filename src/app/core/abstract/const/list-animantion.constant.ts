import { trigger, transition, stagger, query, style, animate, keyframes } from '@angular/animations';


export const LIST_ANIMATION_EASEIN = trigger('listAnimation', [
    transition('* => *', [
      query(':enter', style({ opacity: 0 }), { optional: true }),

      query(':enter', stagger('100ms', [
        animate('.5s ease-in', keyframes([
          style({opacity: 0, transform: 'translateY(-45px)', offset:0}),
          style({opacity: .5, transform: 'translateY(10px)', offset:0.3}),
          style({opacity: 1, transform: 'translateY(0)', offset:1})
        ]))
      ]),{optional: true}),

      query(':leave', stagger('100ms', [
        animate('.5s ease-in', keyframes([
          style({opacity: 1, transform: 'translateY(0)', offset:1}),
          style({opacity: .5, transform: 'translateY(10px)', offset:0.3}),
          style({opacity: 0, transform: 'translateY(-45px)', offset:0})
        ]))
      ]),{optional: true}),
    ])
])

export const LIST_ANIMATION = trigger('listAnimation', [
  transition('* => *', [ 
    query(':leave', [
      stagger(100, [
        animate('0.5s', style({ opacity: 0 }))
      ])
    ], { optional: true }),
    query(':enter', [
      style({ opacity: 0 }),
      stagger(100, [
        animate('0.5s', style({ opacity: 1 }))
      ])
    ], { optional: true })
  ])
])