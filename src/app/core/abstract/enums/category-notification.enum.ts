/**
 * Estos son los enum para las category de notificaciones
 * cada category debe tener una funcionalidad distinta
 *
 * @enum {COMMENT} cada vez que se realiza cualquier comentario.
 * @enum {CONVERSATION} cuando se crea un nuevo chat, o cada vez que se envía un mensaje.
 * @enum {ADMIN}, estos son generados desde Bigwave Admin, para publicidad.
 * @enum {COTIZATION} cada vez que se crea una nueva cotizacion.
 * @enum {FOLLOW} cada vez que alguien te sigue a ti o a alguno de tus profiles.
 * @enum {OFFER} cuando un producto está en oferta.
 * @enum {POST} cada vez que un profile que sigues, publica
 * @enum {PRODUCT} TODO
 * @enum {PROFILE} TODO
 * @enum {WISH} cuando un producto en deseo baja de precio
 */

export enum NotificationCategory {
	COMMENT = 'comment',
	CONVERSATION = 'conversation',
	COTIZATION = 'cotization',
	FOLLOW = 'follow',
	ADMIN = 'admin',
	OFFER = 'offer',
	POST = 'post',
	PRODUCT = 'product',
	PROFILE = 'profile',
	WISH = 'wish'
}
