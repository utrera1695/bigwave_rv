export enum CategoryIcono {
    Vehículos = "Vehículos",
    Inmuebles = "Inmuebles",
    Tecnología = "Tecnología",
    Hogar = "Hogar",
    Deportes_salud = "Deportes y salud",
    Bebés_niños = "Bebés y niños",
    Ropa_calzado_complementos = "Ropa, calzado y complementos",
    Otras_Categorías = "Otras Categorías",
    Servicios = "Servicios"
}