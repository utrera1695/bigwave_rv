export enum ConversationMetadataType {
	CURRENT_USER_CONVERSATIONS = 'current_user_conversations',
	OWN_PROFILE_CONVERSATIONS = 'own_profile_conversations',
	UPDATE_COTIZATION = 'update_cotization',
	DESTROY_CONVERSATION = 'destroy_conversation',
	NEW_CONVERSATION = 'new_conversation',
	NEW_COTIZATION = 'new_cotization',
	MEW_MESSAGE = 'new_message',
	UPDATE_OPEN_CONVERSATION = 'update_open_conversation',
	GET_ALL_OPEN_CHATS = 'get_all_open_chats',
	CHANGED_COTIZATION = 'changed_cotization'
}
