export enum CableStatus {
    INSTANCED = 'instanced',
    DESTROYED = 'destroyed',
    UNDEFINED = 'undefined'
}
