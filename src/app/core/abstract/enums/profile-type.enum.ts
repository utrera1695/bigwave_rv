export enum ProfileType {
    PYME = "pyme",
    INDEPENDENT = "independent",
    SELLER = "seller"
}
