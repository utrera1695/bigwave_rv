export enum ProfileTypes {
  INDEPENDENTS = 'independents',
  SELLERS = 'sellers',
  PYMES = 'pymes'
}
