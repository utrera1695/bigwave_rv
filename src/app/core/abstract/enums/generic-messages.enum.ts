export enum GenericMessages{
    OK = "OK",
    INTERNAL_ERR = "Ha ocurrido un error interno, vuelve a intentarlo mas tarde",
    WEIRD_ERR = "Ha ocurrido algo inesperado, vuelve a intentarlo"
}