import { Response } from "@angular/http";

export function mapResponse(response: Response){
    return Object.assign(response,{body: JSON.parse(response['_body'])});
}