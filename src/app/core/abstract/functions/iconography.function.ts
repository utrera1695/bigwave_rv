import { NotificationCategory } from '../enums/category-notification.enum';
import { ProfileType } from '../enums/profile-type.enum';
import { Type } from '../enums/product-profile.enum';
import { CategoryIcono } from '../enums/category_iconos.enum';

export function getIconography(): Map<NotificationCategory, string> {
    return new Map<NotificationCategory, string>()
        .set(NotificationCategory.CONVERSATION, "chat")
        .set(NotificationCategory.COMMENT, "comment")
        .set(NotificationCategory.COTIZATION, "add_shopping_cart")
        .set(NotificationCategory.FOLLOW, "person_add")
        .set(NotificationCategory.WISH, "favorite")
    //TODO: Agregar mas
}

export function getIconographyProfile(): Map<ProfileType, string> {
    return new Map<ProfileType, string>()
        .set(ProfileType.INDEPENDENT, "work")
        .set(ProfileType.PYME, "business")
        .set(ProfileType.SELLER, "shopping_basket")
}

export function getIconographyType(): Map<Type, string> {
    return new Map<Type, string>()
        .set(Type.PRODUCT, "local_offer")
        .set(Type.PROFIL, "local_convenience_store")
}

export function getIconographyCategory(): Map<CategoryIcono, string> {
    return new Map<CategoryIcono, string>()
        .set(CategoryIcono.Bebés_niños, "child_friendly")
        .set(CategoryIcono.Deportes_salud, "fitness_center")
        .set(CategoryIcono.Hogar, "store_mall_directory")
        .set(CategoryIcono.Inmuebles, "event_seat")
        .set(CategoryIcono.Otras_Categorías, "dns")
        .set(CategoryIcono.Ropa_calzado_complementos, "shopping_basket")
        .set(CategoryIcono.Tecnología, "memory")
        .set(CategoryIcono.Vehículos, "local_taxi")
        .set(CategoryIcono.Servicios, "room_service")
}
