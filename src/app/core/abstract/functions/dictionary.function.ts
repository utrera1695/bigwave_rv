import {CotizationStatus} from '../enums/cotization-status.enum';
import {ProfileType} from '../enums/profile-type.enum';
import {ProfileTypes} from '../enums/profile-types.enum';

/**
 * Este diccionario tiene como fin
 * Traducir los estados de las cotizaciones
 * a un texto legible por el usuario
 * @constructor
 */
export function CotizationStatusDicc(){
    return new Map<CotizationStatus, string>()
			.set(CotizationStatus.ANSWERED, 'Respondida')
			.set(CotizationStatus.RECIEVED, 'Recibida')
			.set(CotizationStatus.SOLD, 'Vendida')
			.set(CotizationStatus.LOST, 'Perdida');
}

export function ProfileTypesFromProfileType() {
  return new Map<ProfileType, ProfileTypes>()
    .set(ProfileType.PYME, ProfileTypes.PYMES)
    .set(ProfileType.SELLER, ProfileTypes.SELLERS)
    .set(ProfileType.INDEPENDENT, ProfileTypes.INDEPENDENTS);
}
