import {ProfileType} from '../enums/profile-type.enum';

export interface UserWish {
	name:         string;
	budget:       number;
	description:  string;
	user:         User;
	response:     boolean;
	sent:         boolean;
	wisheable:    Wisheable;
	priority:     string;
	private:      boolean;
	created_time: string;
	deleted_at:   null;
	updated_at:   string;
	owner:         Owner;
}

export interface User {
	id:                        number;
	provider:                  string;
	uid:                       string;
	allow_password_change:     boolean;
	request_change_password:   null;
	custom_token:              null;
	read_notification_count:   null;
	unread_notification_count: null;
	name:                      string;
	lastname:                  null;
	banner:                    Avatar;
	nickname:                  string;
	avatar:                    Avatar;
	email:                     string;
	phone_one:                 null;
	phone_two:                 null;
	url:                       URL;
	slug:                      string;
	description:               null;
	censured:                  boolean;
	dni:                       null;
	birth_date:                null;
	age:                       null;
	gender:                    null;
	country:                   null;
	created_at:                string;
	updated_at:                string;
}


export interface Owner {
	id:                 number;
	user_id:            number;
	title:              string;
	email:              string;
	country:            string;
	banner:             string;
	photo:              string;
	score:              string;
	prominent:          boolean;
	launched:           string;
	phone:              string;
	url:                string;
	address:            string;
	vision:             string;
	mission:            string;
	description:        string;
	web:                string;
	profile:            string;
	experience:         string;
	validation:         string;
	slug:               string;
	states_codes:       Array<string[]>;
	countries_codes:    string[];
	censured:           boolean;
	deleted_at:         string;
	created_at:         string;
	updated_at:         string;
	location_prominent: boolean;
}


export interface Avatar {
	url: null | string;
}

export interface Wisheable {
	id:                number;
	name:              string;
	description:       string;
	rate:              null;
	weight:            null;
	height:            null;
	width:             null;
	files:             null;
	price:             number;
	service_type:      null;
	cover:             Avatar;
	product_relations: any[];
	tags:              any[];
	prominent:         boolean | null;
	virtual_product:   null;
	stock:             number;
	status:            boolean;
	num_ref:           string;
	bar_code:          string;
	brand:             string;
	currency:          string;
	productable_type:  string;
	productable_id:    number;
	type_profile:      ProfileType;
	states_codes:      Array<string[]>;
	countries_codes:   string[];
	deleted_at:        null;
	created_at:        string;
	updated_at:        string;
	tag_list:          any[];
	productable_url:   string;
	url_get:           string;
}
