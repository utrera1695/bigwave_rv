export interface Categories {
	cover:         string;
	name:          string;
	subcategories: Subcategory[];
}

export interface Subcategory {
	id:               number;
	name:             string;
	category_id:      number;
	products_charged: number;
}
