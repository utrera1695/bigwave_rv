export interface User {
  id:                        number;
  email:                     string;
  slug:                      string;
  provider:                  string;
  avatar:                    Avatar;
  banner:                    Avatar;
  nickname:                  string;
  uid:                       string;
  url:                       string;
  allow_password_change:     boolean;
  request_change_password:   null;
  custom_token:              null;
  read_notification_count:   null;
  unread_notification_count: null;
  name:                      string;
  lastname:                  null;
  phone_one:                 null;
  phone_two:                 null;
  description:               null;
  censured:                  boolean;
  dni:                       null;
  birth_date:                null;
  age:                       null;
  gender:                    null;
  country:                   null;
  type:                      string;
}

export interface Avatar {
  url: null | string;
}
