import {ProfileTypes} from '../enums/profile-types.enum';

export interface Admin {
  id:         string;
  type:       ProfileTypes;
  attributes: Attributes;
}

export interface Attributes {
  user_id:        number;
  type_profile:   string;
  title:          string;
  slug:           string;
  score:          any;
  email:          string;
  banner:         Cover;
  photo:          Cover;
  launched:       any;
  phone:          string;
  url:            string;
  address:        string;
  vision:         any;
  mission:        any;
  description:    any;
  web:            any;
  profile:        any;
  experience:     any;
  validation:     any;
  censured:       boolean;
  social_account: SocialAccount;
  categories:     SubcategoryElement[];
  subcategories:  Subcategory[];
  locations:      Location[];
  options:        Option[];
  custom_fields:  CustomField[];
  products:       Product[];
}

export interface SubcategoryElement {
  id:   number;
  name: string;
}

export interface CustomField {
  id:                number;
  name:              string;
  value:             string;
  product_id:        any;
  customizable_type: string;
  customizable_id:   number;
  created_at:        string;
  updated_at:        string;
}

export interface Location {
  id:             number;
  address:        string;
  latitude:       number;
  longitude:      number;
  state:          string;
  state_code:     string;
  country:        string;
  country_code:   string;
  locatable_type: string;
  locatable_id:   number;
  created_at:     string;
  updated_at:     string;
}

export interface Option {
  id:              number;
  name:            string;
  values:          string[];
  product_id:      any;
  optionable_type: string;
  optionable_id:   number;
  created_at:      string;
  updated_at:      string;
}

export interface Product {
  id:                number;
  name:              string;
  description:       string;
  rate:              any;
  weight:            any;
  height:            any;
  width:             any;
  files:             any;
  price:             number;
  service_type:      any;
  cover:             Cover;
  product_relations: any[];
  tags:              any[];
  prominent:         boolean;
  virtual_product:   any;
  stock:             number;
  status:            boolean;
  num_ref:           string;
  bar_code:          string;
  brand:             string;
  currency:          string;
  productable_type:  string;
  productable_id:    number;
  type_profile:      string;
  states_codes:      Array<string[]>;
  countries_codes:   string[];
  deleted_at:        any;
  created_at:        string;
  updated_at:        string;
  tag_list:          any[];
  url_get:           Cover;
  subcategories:     SubcategoryElement[];
  categories:        PurpleCategory[];
}

export interface PurpleCategory {
  id:         number;
  name:       string;
  cover:      string;
  created_at: string;
  updated_at: string;
}

export interface Cover {
  url: string | null;
}

export interface SocialAccount {
  id:          number;
  facebook:    any;
  twitter:     any;
  instagram:   any;
  google_plus: any;
  tripadvisor: any;
  pinterest:   any;
  flickr:      any;
  behance:     any;
  dribbble:    any;
  tumblr:      any;
  github:      any;
  linkedin:    any;
  soundcloud:  any;
  youtube:     any;
  skype:       any;
  vimeo:       any;
  profile_id:  number;
  created_at:  string;
  updated_at:  string;
}

export interface Subcategory {
  id:              number;
  name:            string;
  product_charged: number;
}
