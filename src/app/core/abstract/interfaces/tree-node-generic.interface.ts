export interface TreeNodeGeneric {
	id: number;
	name: string;
	children?: TreeNodeGeneric[];
	parent_id?: number;
}
