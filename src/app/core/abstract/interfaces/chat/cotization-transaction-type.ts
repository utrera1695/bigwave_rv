import {CotizationStatus} from '../../enums/cotization-status.enum';
import {Conversation, Cotization} from '../messages.interface';

export interface CotizationTransactionType {
	conversation: Conversation;
	cotization: Cotization;
	status_cotization?: CotizationStatus
	status_transaction: string;
}
