export interface SocketMetadata<T, U> {
	type: U;
	body: T;
}
