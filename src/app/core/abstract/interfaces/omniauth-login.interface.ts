export interface OmniauthLoginResponse {
  email: string;
  temporal_password: string;
  provider: string;
  change_password: boolean;
}
