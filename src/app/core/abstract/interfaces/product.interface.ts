import {ProfileType} from '../enums/profile-type.enum';
import {Cover} from './admin';

export interface Product {
    id:         string;
    type:       string;
    attributes: Attributes;
}

export interface Attributes {
    name:                     string;
    bar_code:                 string;
    cover:                    Cover;
    brand:                    string;
    custom_fields:            CustomField[];
    currency:                 string;
    description:              string;
    files:                    any;
    height:                   any;
    num_ref:                  string;
    options:                  Option[];
    price_ranges:             any[];
    price:                    number;
    prominent:                boolean;
    rate:                     any;
    service_type:             any;
    status:                   boolean;
    stock:                    number;
    virtual_product:          any;
    weight:                   any;
    width:                    any;
    photos:                   any[];
    documents:                any[];
    tags:                     any[];
    subcategories:            Subcategory[];
    build_products_relations: any[];
    productable:              Productable;
    url_get:                  { url: string };
	type_profile: ProfileType;
}

export interface CustomField {
    id:                number;
    name:              string;
    value:             string;
    product_id:        any;
    customizable_type: string;
    customizable_id:   number;
    created_at:        string;
    updated_at:        string;
}

export interface Option {
    id:              number;
    name:            string;
    values:          string[];
    product_id:      any;
    optionable_type: string;
    optionable_id:   number;
    created_at:      string;
    updated_at:      string;
}

export interface Productable {
    id:              number;
    user_id:         number;
    title:           string;
    name:            string;
    email:           string;
    country:         any;
    banner:          any;
    photo:           string;
    score:           any;
    prominent:       boolean;
    launched:        any;
    phone:           string;
    url:             string;
    address:         string;
    vision:          any;
    mission:         any;
    description:     any;
    web:             any;
    profile:         any;
    experience:      any;
    validation:      any;
    slug:            string;
    states_codes:    Array<string[]>;
    countries_codes: string[];
    censured:        boolean;
    deleted_at:      any;
    created_at:      string;
    updated_at:      string;
}

export interface Subcategory {
    id:          number;
    name:        string;
    category_id: number;
    created_at:  string;
    updated_at:  string;
}
