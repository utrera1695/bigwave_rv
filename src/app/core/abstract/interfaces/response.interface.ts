export interface BigwaveResponse<T>{
    data: T;
		meta?: Meta;
}

export interface GenericClass<T> {
	id:         string;
	type:       string;
	attributes: T;
}

export interface Meta {
	pagination: Pagination;
	links:      MetaLinks;
}

export interface MetaLinks {
	self:  string;
	first: string;
	prev:  string;
	next:  string;
	last:  string;
}

export interface Pagination {
	per_page:      number;
	total_pages:   number;
	total_objects: number;
}
