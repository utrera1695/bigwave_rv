export interface Post {
	id:            number;
	title:         string;
	content:       string;
	banner:        string;
	postable_type: string;
	postable_id:   number;
	created_at:    string;
	updated_at:    string;
}
