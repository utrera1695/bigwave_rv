export interface CategoriesByListingProductCarousel {
	id:             number;
	name:           string;
	cover:          string;
	products_count: number;
}
