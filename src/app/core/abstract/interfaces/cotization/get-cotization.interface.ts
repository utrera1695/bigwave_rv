import {CotizationStatus} from '../../enums/cotization-status.enum';
import {Option} from '../product.interface';
import {ItemOptsValues} from '../cart-item.interface';

export interface CotizationAtributes {
	price:      number;
	currency:   string;
	stage:      CotizationStatus;
	status:     boolean;
	created_at: string;
	updated_at: string;
	details:    Detail[];
	client:     Client;
	cotizable:  Cotizable;
	products:   Product[];
}

export interface Client {
	id:                        number;
	provider:                  string;
	uid:                       string;
	name:                      string;
	lastname:                  null;
	banner:                    Avatar;
	nickname:                  string;
	avatar:                    Avatar;
	email:                     string;
	phone_one:                 null;
	phone_two:                 null;
	url:                       string;
	slug:                      string;
	description:               string;
	censured:                  boolean;
	country:                   null;
	created_at:                string;
	updated_at:                string;
}

export interface Avatar {
	url: null | string;
}

export interface Cotizable {
	id:                 number;
	user_id:            number;
	title:              string;
	email:              string;
	country:            string;
	banner:             Avatar;
	photo:              string;
	phone:              string;
	url:                string;
	address:            string;
	vision:             string;
	mission:            string;
	description:        string;
	web:                string;
	slug:               string;
	states_codes:       Array<string[]>;
	countries_codes:    string[];
	deleted_at:         null;
	created_at:         string;
	updated_at:         string;
	location_prominent: boolean;
}

export interface Detail {
	item_id:        number;
	product_id:     number;
	name:           string;
	options_values: {[key: number]: any}[];
	quantity:       number;
}

export interface Product {
	id:                number;
	name:              string;
	description:       string;
	price:             number;
	cover:             Avatar;
	stock:             number;
	status:            boolean;
	currency:          string;
	states_codes:      Array<string[]>;
	options:           Option[];
	countries_codes:   string[];
	deleted_at:        string;
	created_at:        string;
	updated_at:        string;
	tag_list:          any[];
}
