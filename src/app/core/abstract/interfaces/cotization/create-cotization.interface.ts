export interface CotizationRequestToCreate {
    profile_id: number;
    price: number;
    items:      ItemCotizationRequest[];
}

export interface ItemCotizationRequest {
    product_id: number;
    custom_field_ids: number[];
    option_ids:       number[];
    option_values:    string[];
    quantity:         number;
}
