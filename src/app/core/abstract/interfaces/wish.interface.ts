import {Avatar} from './cotization/get-cotization.interface';
import {ProfileType} from '../enums/profile-type.enum';

export interface Wish {
    name?: string;
    description?: string;
    budget?: number;
    wisheable_id: number;
    wisheable_type: string;
}


export interface WishDetail{
	id:             number;
	user_id:        number;
	name:           string;
	budget:         number;
	priority:       string;
	response:       boolean;
	sent:           boolean;
	description:    string;
	private:        boolean;
	wisheable_type: string;
	wisheable_id:   number;
	deleted_at:     null;
	created_at:     string;
	updated_at:     string;
	user: User;
	wisheable: Wisheable;
	sended_wish:   SendedWish;
}

export interface SendedWish {
	id:          number;
	profile_id:  number;
	wish_id:     number;
	answer_wish: null;
}


export interface User {
	id:                        number;
	provider:                  string;
	uid:                       string;
	allow_password_change:     boolean;
	request_change_password:   null;
	custom_token:              null;
	read_notification_count:   null;
	unread_notification_count: null;
	name:                      string;
	lastname:                  null;
	banner:                    Avatar;
	nickname:                  string;
	avatar:                    Avatar;
	email:                     string;
	phone_one:                 null;
	phone_two:                 null;
	url:                       URL;
	slug:                      string;
	description:               null;
	censured:                  boolean;
	dni:                       null;
	birth_date:                null;
	age:                       null;
	gender:                    null;
	country:                   null;
	created_at:                string;
	updated_at:                string;
}

export interface Wisheable {
	id:                number;
	name:              string;
	description:       string;
	rate:              null;
	weight:            null;
	height:            null;
	width:             null;
	files:             null;
	price:             number;
	service_type:      null;
	cover:             Avatar;
	product_relations: any[];
	tags:              any[];
	prominent:         null;
	virtual_product:   null;
	stock:             number;
	status:            boolean;
	num_ref:           string;
	bar_code:          string;
	brand:             string;
	currency:          string;
	productable_type:  string;
	productable_id:    number;
	type_profile:      ProfileType;
	states_codes:      any[];
	countries_codes:   any[];
	deleted_at:        null;
	created_at:        string;
	updated_at:        string;
	tag_list:          any[];
}

export interface QueryWishDetail {
	page: string;
	per_page: string;
	response?: string;
	start_date?: string;
	end_date?: string;
}
