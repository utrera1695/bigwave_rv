import {Cover} from './admin';
import {ProfileType} from '../enums/profile-type.enum';

export interface ProductGlobalSearch {
  id:                number;
  name:              string;
  price:             number;
  cover:             Cover;
  stock:             number;
  type_profile:      ProfileType;
  states_codes:      string[];
  countries_codes:   string[];
  created_at:        string;
  updated_at:        string;
  productable:       Productable;
  category_ids:      number[];
  subcategory_ids:   number[];
  links:             any;
  url_get:           Cover;
  is_wished_by_user: null;
}

export interface Productable {
  id:                 number;
  user_id:            number;
  title:              string;
  email:              string;
  country:            null;
  banner:             Cover;
  photo:              Cover;
  score:              null;
  prominent:          boolean;
  launched:           null;
  phone:              string;
  url:                string;
  address:            string;
  vision:             null;
  mission:            null;
  description:        null;
  web:                null;
  profile:            null;
  experience:         null;
  validation:         null;
  slug:               string;
  states_codes:       string[];
  countries_codes:    string[];
  censured:           boolean;
  location_prominent: number;
  deleted_at:         null;
  created_at:         string;
  updated_at:         string;
}
