import {ProfileType} from '../enums/profile-type.enum';
import {Cover} from './admin';

export interface ProfileGlobalSearch {
  id:              number;
  type_profile:    ProfileType;
  title:           string;
  photo:           Cover;
  slug:            string;
  states_codes:    string[];
  countries_codes: string[];
  created_at:      string;
  updated_at:      string;
  category_ids:    number[];
}
