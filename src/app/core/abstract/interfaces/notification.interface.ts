import { NotificationCategory } from "../enums/category-notification.enum";
import {Conversation} from './messages.interface';

export interface NotificationBigwave<T = any>{
    id: number;
    read: false;
    metadata: {
        title: string;
    };
    image: {
        url: string;
    }
    created_at: Date;
    category: NotificationCategory;
    conversation?: Conversation;
    body: T;
}
