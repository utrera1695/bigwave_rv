import {BigwaveResponse, Meta} from './response.interface';

export interface GlobalSearchData<T> extends BigwaveResponse<T>{
  filters_by_objects: FiltersByObjects;
}


export interface FiltersByObjects {
  type_profiles_by_objects: TypeProfilesByObjects;
  categories_by_objects:    CategoriesByObject[];
  countries_by_objects:     CountriesByObject[];
}

export interface CategoriesByObject {
  id:                        number;
  name:                      string;
  total:                     number;
  subcategories_by_objects?: CategoriesByObject[];
}

export interface CountriesByObject {
  country_name: string;
  country_code: string;
  total:        number;
  cities:       City[];
}

export interface City {
  city_name: string;
  city_code: string;
  total:     number;
}

export interface TypeProfilesByObjects {
  pyme:        boolean;
  seller:      boolean;
  independent: boolean;
}

export interface GlobalSearchQuery {
  q: string;
  profiles: string;
  countries_codes: string;
  categories: string;
  page: string;
  per_page: string;
}
