import {CotizationStatus} from '../enums/cotization-status.enum';

export interface Conversations {
	conversations?: ConversationsUser;
	profile_conversations?: ConversationsUser[];
}

export interface ConversationsUser {
	user_conversations: Conversation[];
	cotizations_conversations: Conversation[];
	profile?: ProfileConversation
}

export interface ProfileConversation{
	email: string
	id: number
	photo: string
	title: string
	type_profile: string
}

export interface Conversation {
  id:                   number;
  type_conversation:    'user' | 'profile' | 'admin' | 'cotization';
  sender_messageable:   Messageable;
  receptor_messageable: Messageable;
  body_messages:             Message[];
  open?:                boolean;
  cotization?:          Cotization;
}

export interface Cotization {
	address: string
	client_id: number
	conversation_id: number
	cotizable_id: number
	cotizable_type: string
	created_at: Date
	currency: string
	deal_type_id: number
	details: ProductDetailCotization[]
	id: number
	price: any
	stage: CotizationStatus
	status: true
	text: string
	token: string
	updated_at: Date
}

export interface ProductDetailCotization{
	name: string;
	options_values: any[];
	product_id: number;
	quantity: number;
}

export interface Message {
  id:               number;
  body:             string;
  read:             boolean;
  conversation_id:  number;
  image:            null;
  file:             null;
  messageable_type: string;
  messageable_id:   number;
  created_at:       string;
}

export interface Messageable {
  id:            number;
  type:          'User' | 'Profile'; // TODO
  name:          string;
  custom_avatar: null | string;
}
