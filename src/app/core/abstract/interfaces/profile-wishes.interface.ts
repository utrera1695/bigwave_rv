import {Cover} from './admin';
import {ProfileType} from '../enums/profile-type.enum';

export interface ProfileWish {
	id:           number;
	name:         string;
	price:        number;
	cover:        Cover;
	created_at:   string;
	owner:        Owner;
	total_wishes: number;
	links:        Link;
	url_get:      Cover;
}

export interface Link {
	self: string;
}

export interface Owner {
	id:           number;
	title:        string;
	type_profile: ProfileType;
}

export interface QueryProfileWish {
	q?: string;
	categories?: string;
	subcategories?: string;
	start_date?: string;
	end_date?: string;
	page: string;
	per_page: string;
}
