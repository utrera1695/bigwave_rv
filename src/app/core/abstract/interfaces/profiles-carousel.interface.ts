import {ProfileType} from '../enums/profile-type.enum';
import {Cover} from './admin';

export interface ProfileCarousel {
	id:           number;
	type_profile: ProfileType;
	title:        string;
	photo:        Cover;
	slug:         string;
	category_ids: number[];
	is_followed_by_user:boolean;
	created_at?:  string;
}
