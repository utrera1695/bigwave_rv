import {Cover} from './admin';
import {ProfileType} from '../enums/profile-type.enum';

export interface ProductsByCarousel {
	id:         number;
	name:       string;
	price:      number;
	cover:      Cover;
	created_at: string;
	owner:      Owner;
	is_wished_by_user: boolean | null;
	links:      Links;
	url_get:    Cover;
}

export interface Links {
	self: string;
}

export interface Owner {
	id:           number;
	title:        string;
	type_profile: ProfileType;
}
