export interface CartItem {
    profile_name: string;
    profile_url: string;
    profile_id: number;
    items: Item[];
}

export interface Item {
    product_name?: string;
    product_id?: string;
    description?: string;
    url_get?: string;
    price?: number;
    custom_field_ids: number[];
    option_ids: number[];
    option_values: ItemOptsValues[];
    quantity: number;
    stock: number;
}

export interface ItemOptsValues {
    id: string;
    name?: string;
    value: string;
}
