import {AfterContentChecked, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {MatSidenav} from '@angular/material';
import {AdminService} from './core/services/admin.service';
import {takeWhile} from 'rxjs/operators';
import {fadeAnimation} from './core/abstract/const/route-animations';
import {AngularTokenService} from 'angular-token';
import {ChatService} from './core/services/chat.service';
import {Observable} from 'rxjs';
import {of} from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
	animations: [fadeAnimation]
})
export class AppComponent implements OnInit, OnDestroy, AfterContentChecked {
	@ViewChild('sidenavAdmin') public sidenavAdmin: MatSidenav;
	@ViewChild('sidenavChat') public sidenavChat: MatSidenav;
  mobileQuery: MediaQueryList;
  private readonly _mobileQueryListener: () => void;
  isInAdminMode = false;
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    public adminService: AdminService,
		public ats: AngularTokenService,
		private chatService: ChatService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  ngOnInit() {
  }

  ngAfterContentChecked(): void {
    if (this.sidenavAdmin && !this.adminService.sidenavSource) {
      this.initSidenavSubscriptions();
    }

    // Si el sidenav está definido y su fuente en chatService no, se setea
    if (this.sidenavChat && !this.chatService.chatSidenavSource){
    	this.chatService.setSidenavSource(this.sidenavChat);
		}

    // Importante: Si esta fuera de sesión se debe eliminar el source
    if (this.chatService.chatSidenavSource && !this.ats.userSignedIn()){
    	this.chatService.setSidenavSource(null);
		}
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  initSidenavSubscriptions() {
    this.adminService.isInAdminMode.pipe(takeWhile(() => Boolean(this.sidenavAdmin))).subscribe(
      (bool) => {
        if (bool) {
          this.adminService.setSidenavSource(this.sidenavAdmin);
        }
      }
    );
  }

	// Importante para obtener el estatus de el router-outlet y así hacer animaciones
	public getRouterOutletState(outlet) {
		return outlet.isActivated ? outlet.activatedRoute : '';
	}


}
