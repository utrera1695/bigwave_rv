import {Component, Input, OnInit} from '@angular/core';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {ProfileCarousel} from '../../../core/abstract/interfaces/profiles-carousel.interface';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';
import {LocationService} from '../../../core/services/location.service';
import {ProfileGlobalSearch} from '../../../core/abstract/interfaces/profile-global-search.interface';

@Component({
  selector: 'app-profiles-carousel-items',
  templateUrl: './profiles-carousel-items.component.html',
  styles: [`
		.button-absolute {
			position: absolute;
			z-index: 999;
			top: 40%;
			background: lightslategray;
			color: white;
		}
		`]
})
export class ProfilesCarouselItemsComponent implements OnInit {

	@Input('loading') loading = true;
	@Input('items') items: ProfileCarousel[] = [];
	@Input('profilesGlobalSearch') profilesGlobalSearch: ProfileGlobalSearch[] = [];
	@Input('profileType') profileType: ProfileTypes;
	@Input('visitLabel') visitLabel: string = 'MAS INFORMACIÓN';
	@Input('customOpts') options: OwlOptions = {
		loop: true,
		autoplay: false,
		autoplayTimeout: 2000,
		nav: false,
		mouseDrag: false,
		touchDrag: false,
		pullDrag: false,
		dots: true,
		navSpeed: 700,
		responsive: {
			0: {
				items: 1
			},
			400: {
				items: 2
			},
			740: {
				items: 3
			},
			940: {
				items: 4
			}
		}
	};

  constructor(
  	private locationService: LocationService
	) { }

  ngOnInit() {
  }

}
