import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../../core/services/user.service';
import {Router} from '@angular/router';
import {API_ROUTES} from '../../../app.constants';
import {AngularTokenService} from 'angular-token';
import {MatDialog, MatMenuItem, MatMenuTrigger} from '@angular/material';
import {SidenavService} from '../../../core/services/sidenav.service';
import {ActionCableBigwaveService} from '../../../core/services/action-cable-bigwave.service';
import {LocationService} from '../../../core/services/location.service';
import {CableStatus} from '../../../core/abstract/enums/cable-status.enum';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {AdminService} from '../../../core/services/admin.service';
import {ModalsService} from '../../../core/services/modals.service';
import {Country} from '../../../core/abstract/interfaces/country-state.interface';
import {delay, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatMenuItem) trigger2: MatMenuItem;
  public user: any = {
    nickname: '',
    email: '',
    name: '',
    password: '',
    password_confirmation: '',
    avatar: {
      url: null
    }
  };

  public loading: boolean = false;
  public loadingCategories: boolean = false;
  public categories: any = [];
  public category: null;
  public search = false;

  countryState: Country;

  isInAdmin = false;

  showSearchAction = false;

  constructor(
    private userService: UserService, private router: Router,
    public dialog: MatDialog,
    public _tokenService: AngularTokenService,
    private sidenav: SidenavService,
    private actionCable: ActionCableBigwaveService,
    public locationService: LocationService,
    private ui: UIComponentsService,
    private http: HttpClient,
    private adminService: AdminService,
    private modalsService: ModalsService
  ) { }

	ngOnInit() {
		this.getUser();
		this.getCategories();
		this.onCableDisconectSubscription();
		this.initCountrySelectedSub();
		this.adminService.isInAdminMode.subscribe((bool) => this.isInAdmin = bool);
	}

  openAdminSidenav() {
    this.adminService.toggleSidenav();
  }

  addProfile(){
    if(this._tokenService.userSignedIn()){
      this.router.navigate(['/profile/comenzar-ahora']);
    }else{
      this.openLoginDialog();
    }
  }

  openLoginDialog() {
    this.modalsService.openLoginDialog().afterClosed().subscribe(result => {
      this.getUser();
    });
  }

  openCountryModal(){
  	this.modalsService.openSelectCountry();
	}

  getUser() {
    if (window.localStorage.getItem('user')) {
      this.user = JSON.parse(window.localStorage.getItem('user'));
    }
  }

  initCountrySelectedSub(){
  	this.locationService.currentCountryState.subscribe(
			(state) => {
				this.countryState = state;
			}
		)
	}


  getCategories() {
    this.loadingCategories = true;
    let url = API_ROUTES.getCategories();
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

        if (data['data'].length)
          this.categories = data['data'];
        this.categories = this.categories.map(function (op) {
          return { "id": op.id, "name": op.attributes.name, "subcategories": op.attributes.subcategories }
        });

        this.loadingCategories = false;
      },
      error => {
        this.loadingCategories = false;
        this.ui.internalGenericError(error);
      }
    );
  }

  productsByCategory(id, nombre) {
    this.router.navigate(["products", "category", id, nombre]);
    this.trigger.closeMenu();
  }

  onCableDisconectSubscription() {
    this.actionCable.onInstance.pipe(distinctUntilChanged(),delay(2000))
      .subscribe(
        (status) => {
          if (status === CableStatus.UNDEFINED) {
            console.log('Reconectando...', status);
            this.actionCable.createCableInstance();
          }
        }
      );
  }


}
