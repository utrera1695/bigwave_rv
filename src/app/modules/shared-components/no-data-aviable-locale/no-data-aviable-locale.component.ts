import {Component, Input, OnInit} from '@angular/core';
import {LocationService} from '../../../core/services/location.service';

@Component({
  selector: 'app-no-data-aviable-locale',
  template: `
		<div class="jumbotron m-2 p-3">
			<h3>{{title}}
				<mat-icon>sentiment_very_dissatisfied</mat-icon>
			</h3>
			<mat-divider class="m-2"></mat-divider>
			<p class="lead">Lo sentimos, actualmente no tenemos información que mostrarte</p>
			<p class="lead">Posiblemente sea por tu localidad ({{locationService.getCurrentCountryName() || ''}})</p>
			<p class="lead">Puedes intentar cambiando de ubicación a <b>Global</b> para ver de todo el mundo
				<fa-icon [icon]="['fas','globe-americas']"></fa-icon>
			</p>
			<div class="d-flex justify-content-center align-items-center">
				<button mat-button (click)="locationService.clearCountryState()" 
								matTooltip="Atención, esta acción cambiará tu localización actual a Global">
					<fa-icon [icon]="['fas','globe-americas']"></fa-icon>
					VER TODO
				</button>
			</div>
		</div>
  `,
  styles: [`		
		.lead{
			font-size: 1rem;
			font-weight: 300;
			line-height: 1;
		}
	`]
})
export class NoDataAviableLocaleComponent implements OnInit {

	@Input('title') title = 'No hay registros disponibles';

  constructor(
  	public locationService: LocationService
	) { }

  ngOnInit() {
  }

}
