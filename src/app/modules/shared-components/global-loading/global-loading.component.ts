import { Component, OnInit } from '@angular/core';
import {GlobalUiService} from '../../../core/services/global-ui.service';
import {NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {type} from 'os';

@Component({
  selector: 'app-global-loading',
  templateUrl: './global-loading.component.html',
  styleUrls: ['./global-loading.component.scss']
})
export class GlobalLoadingComponent implements OnInit {

	show = false;

  constructor(
  	private gui: GlobalUiService,
		private router: Router
	) { }

  ngOnInit() {
  	this.gui.globalLoading.subscribe( (bool) => this.show = bool);
  	this.initRouterEventsSubscription();
  }

  initRouterEventsSubscription(){
  	this.router.events.subscribe(
			(event) => {
				if (event instanceof NavigationStart){
					this.gui.showGlobalBarLoading();
				}

				if (event instanceof NavigationEnd){
					this.gui.hideGlobalBarLoading();
				}

				if (event instanceof NavigationError){
					this.gui.hideGlobalBarLoading();
				}
			}
		)
	}

}
