import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../../../core/abstract/interfaces/product.interface';
import {ProfileType} from '../../../../core/abstract/enums/profile-type.enum';
import {Router} from '@angular/router';
import {ProductGlobalSearch} from '../../../../core/abstract/interfaces/product-global-search.interface';
import {getIconographyProfile} from '../../../../core/abstract/functions/iconography.function';

@Component({
  selector: 'app-generic-product-item',
  templateUrl: './generic-product-item.component.html',
  styleUrls: ['./generic-product-item.component.scss']
})
export class GenericProductItemComponent implements OnInit {

  @Input('item') product: Product;
  @Input('globalSearchItem') globalSearchProduct: ProductGlobalSearch;

  iconographyProfile = getIconographyProfile();
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  goDetail(type_profile: ProfileType, owner_id, product_id) {
    this.router.navigate(["/product", type_profile, owner_id, product_id]);
  }

  goToProfile(type_profile: ProfileType, profile_id){
    this.router.navigate(['/profiles', type_profile, profile_id])
  }

  toNumber(val: any){
    return Number(val);
  }
}
