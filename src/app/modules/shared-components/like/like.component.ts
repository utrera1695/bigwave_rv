import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Wish} from '../../../core/abstract/interfaces/wish.interface';
import {AngularTokenService} from 'angular-token';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {ModalsService} from '../../../core/services/modals.service';
import {WishService} from '../../../core/services/wish.service';
import {UserService} from '../../../core/services/user.service';
import {MatDialog} from '@angular/material';
import {CreateWishComponent} from '../../web/user/user-wishes/create-wish/create-wish.component';
import {filter, takeUntil, takeWhile} from 'rxjs/operators';

@Component({
	selector: 'app-like',
	templateUrl: './like.component.html',
	styleUrls: ['./like.component.scss']
})
export class LikeComponent implements OnInit, OnDestroy {
	@Input() id: number;
	@Input() price: number = 0;
	@Input('overrideWished') overrideWished = null;

	isWished = false;
	isFetching = false;
	isAlive: boolean;

	constructor(
		private _ts: AngularTokenService,
		private ui: UIComponentsService,
		private modalService: ModalsService,
		private wishService: WishService,
		private userService: UserService
	) {
	}

	ngOnInit() {
	  this.isAlive = true;
		this.userLoggedInSub()
	}

	ngOnDestroy(): void {
	  this.isAlive = false;
  }

  userLoggedInSub(){
		this.userService.userLoggedIn.subscribe(
			(bool) => {
				if(bool){
					if(this.overrideWished === null){
						this.isProductWished();
					}else{
						this.isWished = this.overrideWished;
						this.overrideWished = null;
					}
				}
			}
		)
	}

	async toggleWish() {
	  let continueToggle = true;

		if (!this._ts.userSignedIn()) {
      continueToggle = await this.openLogin().afterClosed().toPromise();
		}

		if(!continueToggle){
		  return;
		}

		if (this.isWished) {
			this.unwish();
		} else {
			this.wish();
		}
	}

	wish() {
		this.isFetching = true;
		const wish: Wish = {
			wisheable_id: this.id,
			wisheable_type: 'Product',
			name: 'Deseo simple',
			budget: this.price
		};

		this.wishService.create(wish).subscribe((data: any) => {
			this.isWished = true;
			this.isFetching = false;
		}, (error) => {
			this.ui.showSnackNotification('Ha ocurrido un error al agregar deseo, vuelve a intentarlo', 'OK');
			this.isFetching = false;
		});
	}

	unwish(){
	  this.isFetching = true;

	  this.wishService.unwished(this.id).subscribe(
      (data) => {
        this.isWished = false;
        this.isFetching = false;
      }, (error) => {
        this.ui.showSnackNotification('Ha ocurrido un error al eliminar deseo, vuelve a intentarlo', 'OK');
      }
    )
  }

	isProductWished() {
		this.isFetching = true;
		if (!this._ts.userSignedIn()) {
			return;
		}

		this.wishService.wished(this.id).pipe(takeUntil(this.userService.userLoggedIn.pipe(filter(v => v === false))), takeWhile(() => this.isAlive))
      .subscribe((data: boolean) => {
			this.isWished = data;
			this.isFetching = false;
		});
	}

	openLogin() {
		return this.modalService.openLoginDialog();
	}

	get showIsLiked(){
		return this._ts.userSignedIn() && this.isWished;
	}
}
