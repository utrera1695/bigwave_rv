import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-alert-generic-message',
  templateUrl: './alert-generic-message.component.html',
  styleUrls: ['./alert-generic-message.component.scss']
})
export class AlertGenericMessageComponent implements OnInit {
	@Input('icon') icon = 'info';
	@Input('message') message = 'hello world';
	@Input('tittle') tittle = 'Información:';
  constructor() { }

  ngOnInit() {
  }

}
