import {Component, Input, OnInit} from '@angular/core';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {ProductsByCarousel} from '../../../core/abstract/interfaces/products-by-carousel.interface';
import {Router} from '@angular/router';
import {Product} from '../../../core/abstract/interfaces/product.interface';

@Component({
  selector: 'app-products-carousel-items',
  templateUrl: './products-carousel-items.component.html',
  styles: [`
		.button-absolute {
			position: absolute;
			z-index: 999;
			top: 40%;
			background: lightslategray;
			color: white;
		}
		.card-carousel-demo .mat-card-image {
			height: 250px; /* adjust as needed */
			object-fit: cover;
      width: calc(100% + 32px);
		}

		.card-carousel-demo ::ng-deep .mat-card-header-text {
			margin: 0;
		}`]
})
export class ProductsCarouselItemsComponent implements OnInit {

	@Input('loading') loading = true;
	@Input('displayProfile') displayProfile = false;
	@Input('items') items: ProductsByCarousel[] = [];
	@Input('itemsFullyAttr') itemsFullyAttr: Product[] = [];
	@Input('isFullyProduct') isFullyProduct = false;
	@Input('customOpts') options: OwlOptions = {
		loop: true,
		autoplay: false,
		autoplayTimeout: 2000,
		nav: false,
		mouseDrag: false,
		touchDrag: false,
		pullDrag: false,
		dots: true,
		navSpeed: 700,
		responsive: {
			0: {
				items: 1
			},
			400: {
				items: 2
			},
			740: {
				items: 3
			},
			940: {
				items: 4
			}
		}
	};

  constructor(
  	private router: Router
	) { }

  ngOnInit() {
  }

	goDetail(type_profile, owner_id, product_id) {
		this.router.navigate(["/product", type_profile, owner_id, product_id]);
	}

	goToProfile(type_profile, profile_id){
    this.router.navigate(['/profiles', type_profile, profile_id])
  }

  toNumber(val: any){
    return Number(val);
  }

}
