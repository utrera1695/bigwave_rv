import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AngularTokenService} from 'angular-token';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ProfileService} from '../../../core/services/profile.service';
import {Admin} from '../../../core/abstract/interfaces/admin';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-are-you-sure',
  templateUrl: './are-you-sure-confirm-password.component.html'
})
export class AreYouSureConfirmPasswordComponent implements OnInit{

  form: FormGroup;
  loading: boolean = false;
	hide = true;
  constructor(
    public dialogRef: MatDialogRef<AreYouSureConfirmPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Admin,
    private ui: UIComponentsService,
    private fb: FormBuilder
  ) { }

	ngOnInit() {
    this.form = this.fb.group({
      current_password: ['', [Validators.required, Validators.min(8)]]
    })
	}

  onConfirm(){
    this.close(this.form.value);
  }

  close(b: any){
  	this.dialogRef.close(b);
	}
}
