import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfileType} from '../../../core/abstract/enums/profile-type.enum';
import {getIconographyProfile} from '../../../core/abstract/functions/iconography.function';
import {Admin} from '../../../core/abstract/interfaces/admin';
import {AdminService} from '../../../core/services/admin.service';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';

@Component({
  selector: 'app-sidenav-bigwave',
  templateUrl: './sidenav-bigwave.component.html',
  styleUrls: ['./sidenav-bigwave.component.scss']
})
export class SidenavBigwaveComponent implements OnInit, OnDestroy {
  TypesIcons = ProfileType;
  TypesProfiles = ProfileTypes;
  iconographyProfile = getIconographyProfile();
  adminData: Admin;

  constructor(
    private adminService: AdminService,
  ) { }

  ngOnInit() {
    this.adminService.currentAdminData.subscribe((data) => this.adminData = data);
  }

  ngOnDestroy(): void {
  }

}
