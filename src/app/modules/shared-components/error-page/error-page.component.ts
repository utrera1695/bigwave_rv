import { Component,OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
	styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {

	code: number;

  constructor(
  	public location: Location,
		private route: ActivatedRoute
	) { }

  ngOnInit() {
  	this.route.params.subscribe(
			(params) => {
				this.code = +params['id'];
			}
		)
  }

}
