import {Component, OnInit} from '@angular/core';
import { MatDialogRef} from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { API_ROUTES } from '../../../../app.constants';
import { AngularTokenService } from 'angular-token';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-politics-privacy',
  templateUrl: './politics-privacy.component.html',
  styleUrls: ['./politics-privacy.component.scss']
})
export class PoliticsPrivacyComponent implements OnInit {

  terms:string;
  file:string;

  constructor(public dialogRef: MatDialogRef<PoliticsPrivacyComponent>,
              private http:HttpClient,
              public _tokenService: AngularTokenService)
  {}


  ngOnInit() {
    this.getPolitics();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getPolitics(){

    let url = API_ROUTES.getPolitics();

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        
        this.terms = data['terms'];
        let imgUrl = data['file']['url'];
        this.file = 'http://bigwave-api.herokuapp.com'+imgUrl;
      },

      error => {
      }
    );

  }

}
