import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { PoliticsPrivacyComponent } from './politics-privacy/politics-privacy.component';
import { ModalsService } from '../../../core/services/modals.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private modalService: ModalsService
     ) {

     }

  ngOnInit() {
  }

  openPoliticsDialog(): void {
    const dialogRef = this.dialog.open(PoliticsPrivacyComponent)

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed')
    });
  }

  openContactDialog(): void {

    this.modalService.openContactoDialog().afterClosed().subscribe(result => {
      //console.log('The dialog was closed')
    });
  }



}
