import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-no-data-aviable-display',
  template: `
		<div class="jumbotron">
			<h1 class="display-4">{{title}}</h1>
			<p class="lead">{{content}}</p>
		</div>
  `,
  styles: []
})
export class NoDataAviableDisplayComponent implements OnInit {

	@Input('title') title: string = 'No hay registros';
	@Input('content') content: string = 'Actualmente no hay información que mostrar, puede intentar añadiendo nuevos registros';

  constructor() { }

  ngOnInit() {
  }

}
