import { Component, OnInit } from '@angular/core';
import {AngularTokenService} from 'angular-token';
import {ActionCableBigwaveService} from '../../../core/services/action-cable-bigwave.service';
import {CartService} from '../../../core/services/cart.service';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {Router} from '@angular/router';
import {ProfileType} from '../../../core/abstract/enums/profile-type.enum';
import {getIconographyProfile} from '../../../core/abstract/functions/iconography.function';
import {UserService} from '../../../core/services/user.service';
import {AuthService} from 'angularx-social-login';
import {User} from '../../../core/abstract/interfaces/user.interface';

@Component({
  selector: 'app-profile-options-menu',
  templateUrl: './profile-options-menu.component.html',
  styleUrls: ['./profile-options-menu.component.scss']
})
export class ProfileOptionsMenuComponent implements OnInit {
  TypesIcons = ProfileType;
  iconographyProfile = getIconographyProfile();

  get user(){
    return this.userService.getCurrentUserDataInStorage();
  }

  socialAuthState = false;

  constructor(
    private _tokenService: AngularTokenService,
    private actionCable: ActionCableBigwaveService,
    private cartService: CartService,
    private ui: UIComponentsService,
    private router: Router,
		private userService: UserService,
    private sas: AuthService
  ) { }

  ngOnInit() {
    this.sas.authState.subscribe((user) => this.socialAuthState = !!user);
  }

  logout() {
    this._tokenService.signOut().subscribe(
      res => {
        if(this.socialAuthState){
          this.sas.signOut();
        }
        this.actionCable.disconectCable();
        this.userService.setUserLoggedState(false);
        localStorage.clear();
        this.cartService.deleteAll();
        this.ui.showSnackNotification('¡Hasta luego!', 'OK');
        this.router.navigate(['/']);

      },
      error => {
        this.ui.showSnackNotification('Ha ocurrido un error al cerrar sesión', 'OK');
        console.error(error);
      }
    );
  }

}
