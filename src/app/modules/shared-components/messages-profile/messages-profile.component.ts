import {Component, OnDestroy, OnInit} from '@angular/core';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {MatBottomSheet} from '@angular/material';
import {ChangeCotizacionStatusBottomshetComponent} from './change-cotizacion-status-bottomshet/change-cotizacion-status-bottomshet.component';
import {CotizationStatusDicc} from '../../../core/abstract/functions/dictionary.function';
import {ChatService} from '../../../core/services/chat.service';
import {Conversation, ConversationsUser} from '../../../core/abstract/interfaces/messages.interface';
import {CotizationStatus} from '../../../core/abstract/enums/cotization-status.enum';
import {AdminService} from '../../../core/services/admin.service';
import {BigwaveUtils} from '../../../core/abstract/class/BigwaveUtils.class';
import {takeWhile} from 'rxjs/operators';


@Component({
  selector: 'app-messages-profile',
  templateUrl: './messages-profile.component.html',
  styleUrls: ['./messages-profile.component.scss']
})
export class MessagesProfileComponent implements OnInit, OnDestroy {

	loading = true;

	// Combo de estatus cotizaciones
  cotizationDicc = CotizationStatusDicc(); // Diccionario de estados de cotizaciones
  cotizationStatusKeys: CotizationStatus[] = BigwaveUtils.getCotizationStatusAsArray(); // Para llenar el combo de estados de cotizaciones

  conversations: ConversationsUser = {user_conversations: [], cotizations_conversations: []};
  filteredCotizations: Conversation[] = [];

  isInAdminMode: boolean;

  alive: boolean;

  constructor(
    private bottomSheet: MatBottomSheet,
    private ui: UIComponentsService,
    public chatService: ChatService,
		private as: AdminService
  ) {}

  ngOnInit() {
		this.isInAdminMode = this.as.isInAdmin();
		this.alive = true;
		this.initConectedSuscription();
  }

  ngOnDestroy(): void {
		this.alive = false;
	}

	/**
	 * Para poder llenar el arreglo de conversaciones
	 * escuchamos la suscripción de {displayedConversations}
	 */
  initConectedSuscription() {
  	this.chatService.isConnectedToChat.subscribe(
			(bool) => {
				if(bool){
					this.initDisplayedChatsSubscription();

					setTimeout(() => {
						if(this.isInAdminMode){
							this.chatService.ownUserProfileConversationsAction([Number(this.as.getCurrentAdminId())]);
							return;
						}
						this.chatService.currentUserConversationsAction();
					},1000);
				}
			}
		);
  }

  initDisplayedChatsSubscription(){
		this.chatService.displayedConversations.pipe(takeWhile(() => this.alive)).subscribe(
			(conversations) => {
				if (conversations) {
					this.loading = false;
					this.conversations = conversations;
					this.filteredCotizations = conversations.cotizations_conversations;
				}
			}
		);
	}


	/**
	 * Para cambiar el estado de la cotización
	 * @param idCotization proviene del template
	 * @param status es importante para no permitir cambiar el estatus
	 * cuando se deja en SOLD o LOST
	 * @param currentable_id si es profile debe mandar el id
	 */
  async changeStatusCotization(idCotization: number, status: CotizationStatus, currentable_id: number) {
  	if (!this.as.isInAdmin()){
  		this.ui.showDialogNotification('Información','En este caso, estatus de la cotización solo puede ser cambiado por el vendedor');
  		return;
		}

  	if (status === CotizationStatus.SOLD || status === CotizationStatus.LOST){
  		this.ui.showDialogNotification(
  			'Información',
				'No se puede cambiar el estado de la cotización por que ya fue marcada como <b>Vendida</b> o <b>Perdida</b>. ' +
				'<br><br>Esto significa que es el último estado que puede poseer',
				'Entendido'
			);
  		return;
		}

  	// Abrimos el bottomSheet y posteriormente esperamos a la respuesta de la promesa
    const who = await this.bottomSheet.open(ChangeCotizacionStatusBottomshetComponent).afterDismissed().toPromise();
    // Si es false quiere decir que no seleccionó opción
    if (!who) {
      return;
    }
    // De lo contrario mandamos a actualizar el estatus de la cotización
    this.chatService.updateCotizationAction(idCotization.toString(), who, currentable_id);
  }

	/**
	 * Para mandar a eliminar la cotización
	 * @param id proviene del template
	 * @param currentable_id si es profile debe mandar el id
	 */
  async dismissConversation(id: number, currentable_id: number) {
		// Abrimos el modal y esperamos el valor bool de la promesa
    const bool = await this.ui.showDialogNotification(
      '¿Desea eliminar la conversación?',
			'A continuación va a eliminar una conversación. <br><br> Tenga en cuenta que ' +
			'para las cotizaciones si su estatus actual es <b>Vendida</b> o <b>Perdida</b> será movida al historial <br> <br>' +
			'De lo contrario para ambos usuarios la conversación será eliminada',
			'Sí'
      ).afterClosed().toPromise();
    // Si este valor es falsey quiere decir que no seleccionó o clickeo no
    if (!bool) {
      return;
    }
    // De lo contrario mandamos a eliminar
    this.chatService.destroyConversationAction(id.toString(), currentable_id);
  }

  filterByStatus(status: CotizationStatus | string){
  	if (status === 'all'){
  		this.filteredCotizations = this.conversations.cotizations_conversations.filter(c => c);
  		return;
		}

  	this.filteredCotizations = this.conversations.cotizations_conversations.filter(c => c.cotization.stage === status);
  }

	/**
	 * Agrega un chat a la lista de chats abiertos
	 * @param chat
	 */
	addChatToList(chat: Conversation){
  	this.chatService.addChatToList(chat);
	}

	getClassByCotizationStatus(status: CotizationStatus): string {
		return status === CotizationStatus.LOST ? 'badge-secondary': status === CotizationStatus.SOLD ? 'badge-success' : 'badge-primary';
	}

}
