import { Component, OnInit } from '@angular/core';
import { CotizationStatus } from '../../../../core/abstract/enums/cotization-status.enum';
import { MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'app-change-cotizacion-status-bottomshet',
  templateUrl: './change-cotizacion-status-bottomshet.component.html',
  styleUrls: ['./change-cotizacion-status-bottomshet.component.scss']
})
export class ChangeCotizacionStatusBottomshetComponent implements OnInit {

  CotizationStatus = CotizationStatus

  constructor(
    private bottomSheetRef: MatBottomSheetRef<ChangeCotizacionStatusBottomshetComponent>
  ) { }

  ngOnInit() {
  }

  onChange(status: CotizationStatus){
    this.bottomSheetRef.dismiss(status);
  }

}
