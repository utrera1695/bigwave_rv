import { Component, OnInit, Input } from '@angular/core';
import { CategoryIcono } from '../../../../core/abstract/enums/category_iconos.enum';
import { getIconographyCategory } from '../../../../core/abstract/functions/iconography.function';

@Component({
  selector: 'app-category-icono',
  templateUrl: './category-icono.component.html',
  styleUrls: ['./category-icono.component.scss']
})
export class CategoryIconoComponent implements OnInit {

  @Input() icono: string;
  
  @Input() tamano: string;

  @Input() colour: string;

  iconography = getIconographyCategory()

  Iconmane : string =''

  constructor() {
  }



  ngOnInit() {  
    this.getIcono();
  }

  getIcono() {
    if (this.icono =='Vehículos') {
      this.Iconmane =  "local_taxi";
    }
    if (this.icono =='Inmuebles') {
      this.Iconmane =  "event_seat";
    }
    if (this.icono =='Tecnología') {
      this.Iconmane =  "memory";
    }
    if (this.icono =='Hogar') {
      this.Iconmane =  "store_mall_directory";

    }
    if (this.icono =='Deportes y salud') {
      this.Iconmane =  "fitness_center";

    }
    if (this.icono =='Bebés y niños') {
      this.Iconmane =  "child_friendly";

    }
    if (this.icono =='Ropa, calzado y complementos') {
      this.Iconmane =  "shopping_basket";

    }
    if (this.icono =='Otras Categorías') {
      this.Iconmane =  "dns";

    } 
    if (this.icono =='Servicios') {
      this.Iconmane =  "room_service";

    }

  }

}
