import { Component, OnInit } from '@angular/core';
import { API_ROUTES } from '../../../app.constants';
import { AngularTokenService } from 'angular-token';
import { MatDialogRef } from '@angular/material';
import { LocationService } from '../../../core/services/location.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Country} from '../../../core/abstract/interfaces/country-state.interface';

@Component({
  selector: 'app-select-country',
  templateUrl: './select-country.component.html',
  styleUrls: ['./select-country.component.scss']
})
export class SelectCountryComponent implements OnInit {


  countries: Country[] = [];
  select = true;

  form: FormGroup;

  loading = true;
  constructor(
    public dialogRef: MatDialogRef<SelectCountryComponent>,
    public locationService: LocationService,
		private ui: UIComponentsService,
		private fb: FormBuilder
) { }

  ngOnInit() {
  	this.form = this.fb.group({
			country: ['', Validators.required]
		});

    this.getCountries();
  }

  getCountries() {
    this.locationService.getAllCountries().subscribe(
      (data) => {
        this.countries = data.countries;
        this.loading = false;
      },
      error => {
      	this.ui.internalGenericError(error);
      	this.loading = false;
      }
    );
  }

  clearCountryState(){
  	this.locationService.clearCountryState();
	}

  assignCountryState(){
  	const country = this.form.value['country'] as Country;
  	this.locationService.setCountryState(country.name, country.code);
	}

  onClose(b?: boolean){
  	this.dialogRef.close(b);
	}

}
