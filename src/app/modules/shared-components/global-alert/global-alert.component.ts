import {Component, OnInit, ViewChild} from '@angular/core';
import {UIComponentsService} from '../../../core/services/material-ui.service';

@Component({
  selector: 'app-global-alert',
  templateUrl: './global-alert.component.html',
  styleUrls: ['./global-alert.component.scss']
})
export class GlobalAlertComponent implements OnInit {

  showTemplate = 0;
  constructor(
    private ui: UIComponentsService
  ) { }

  ngOnInit() {
    this.ui.onShowGenericGlobalMessage.subscribe(
      (tmpl) => {
        this.showTemplate = tmpl;
      }
    )
  }

  removeTemplate(){
    this.showTemplate = null;
  }
}
