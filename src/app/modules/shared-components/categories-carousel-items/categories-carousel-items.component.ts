import {Component, Input, OnInit} from '@angular/core';
import {ProductsByCarousel} from '../../../core/abstract/interfaces/products-by-carousel.interface';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {Router} from '@angular/router';
import {CategoriesByListingProductCarousel} from '../../../core/abstract/interfaces/categories-by-listing-product-carousel.iterface';

@Component({
  selector: 'app-categories-carousel-items',
  templateUrl: './categories-carousel-items.component.html',
  styles: [`
		.logo-store{
			text-align: left;
			background-color: #FFF;
			border-radius: 5px;
			box-shadow: 0 2px 15px 0 rgba(0,0,0,.1);
			height: 70px;
			width: 70px;
			margin: -30px auto 10px auto;
			overflow: hidden;
			position: relative;
			display: flex;
			align-items: center;
			justify-content: center;
		}
		
		.button-absolute {
			position: absolute;
			z-index: 999;
			top: 40%;
			background: lightslategray;
			color: white;
		}
		.card-carousel-demo .mat-card-image {
			height: 150px; /* adjust as needed */
			width: 100%;
			margin: 0;
		}

		.card-carousel-demo ::ng-deep .mat-card-header-text {
			margin: 0;
		}`]
})
export class CategoriesCarouselItemsComponent implements OnInit {

	@Input('loading') loading = true;
	@Input('items') items: CategoriesByListingProductCarousel[] = [];
	@Input('customOpts') options: OwlOptions = {
		loop: true,
		autoplay: false,
		autoplayTimeout: 2000,
		nav: false,
		mouseDrag: false,
		touchDrag: false,
		pullDrag: false,
		dots: true,
		navSpeed: 700,
		responsive: {
			0: {
				items: 1
			},
			400: {
				items: 2
			},
			740: {
				items: 3
			},
			940: {
				items: 4
			}
		}
	};

	constructor(
		private router: Router
	) { }

	ngOnInit() {
	}

	goCategory(id, nombre) {
		this.router.navigate(["products", "category", id, nombre]);
	}

}
