import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AngularTokenService} from 'angular-token';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {UserService} from '../../../core/services/user.service';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {Actions} from '../../../core/abstract/enums/actions.enum';
import {PATTERNS} from '../../../core/abstract/const/patterns';

@Component({
  selector: 'app-register',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {
  form: FormGroup;

  public loading: boolean = false;
  hidePC: boolean;
  hideP: boolean;


  constructor(
    public dialogRef: MatDialogRef<RegisterUserComponent>,
    @Inject(MAT_DIALOG_DATA) public action: Actions,
    private fb: FormBuilder, private userService: UserService,
    private router: Router,
    private _tokenService: AngularTokenService,
    private ui: UIComponentsService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.generateForm();
  }

  generateForm(){
    this.form = this.fb.group({
      name: ['', Validators.required],
      nickname: ['', [Validators.required, Validators.minLength(2), Validators.pattern(PATTERNS.USERNAME)]],
      login: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      password_confirmation: ['', [Validators.required, Validators.minLength(8)]]
    })
  }

  onDialogClose(): void {
    this.dialogRef.close();
  }

  register(){
    this.loading = true;
    const data = this.form.value;
    if(data.password !== data.password_confirmation){
      this.ui.showSnackNotification('Las contraseñas no coinciden', 'OK');
      this.loading = false;
      return;
    }

    this._tokenService.registerAccount(data).subscribe(
      (data: any) => {
        this.loading = false;
        this.userService.login(data['data']);
        this.onDialogClose();
        if(this.action === Actions.CREATE_PROFILE){
          this.router.navigate(['/profile']);
        }else{
          this.router.navigate(['/'])
        }
      },
      error => {
        this.loading = false;
        this.ui.internalGenericError(error);
      }
    );
  }

}
