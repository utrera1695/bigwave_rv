import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-loading-bar-progress',
  templateUrl: './loading-bar-progress.component.html',
  styleUrls: ['./loading-bar-progress.component.scss']
})
export class LoadingBarProgressComponent implements OnInit {
  @Input('uploadingBarProgress') uploadingBarProgress = 0;

  constructor() { }

  ngOnInit() {
  }

}
