import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material';
import {TreeNodeGeneric} from '../../../core/abstract/interfaces/tree-node-generic.interface';
import {Tree} from '@angular/router/src/utils/tree';

@Component({
  selector: 'app-tree-node-generic',
  templateUrl: './tree-node-generic.component.html',
  styleUrls: ['./tree-node-generic.component.scss']
})
export class TreeNodeGenericComponent implements OnInit {

	treeControl = new NestedTreeControl<TreeNodeGeneric>(node => node.children);
	dataSource = new MatTreeNestedDataSource<TreeNodeGeneric>();

	_data: TreeNodeGeneric[] = [];

	@Input('data') set data(_data: TreeNodeGeneric[]){
		this.dataSource.data = _data;
		this._data = _data;
	}

	get data(){
		return this._data;
	}

	@Output('selected') selected = new EventEmitter<TreeNodeGeneric>();

	constructor() {
	}

	hasChild = (_: number, node: TreeNodeGeneric) => !!node.children && node.children.length > 0;

  ngOnInit() {
  }

  onSelect(node: TreeNodeGeneric){
  	this.selected.emit(node);
	}

}
