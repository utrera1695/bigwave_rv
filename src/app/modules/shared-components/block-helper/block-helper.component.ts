import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-block-helper',
  templateUrl: './block-helper.component.html',
  styleUrls: ['./block-helper.component.scss']
})
export class BlockHelperComponent implements OnInit {

  @Input() text: string;
  constructor() { }

  ngOnInit() {
  }

}
