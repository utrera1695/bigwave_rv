import { Component, OnInit } from '@angular/core';
import {AngularTokenService} from 'angular-token';
import {ChatService} from '../../../../core/services/chat.service';

@Component({
  selector: 'app-chat-toggle-button-fab',
  templateUrl: './chat-toggle-button-fab.component.html',
  styleUrls: ['./chat-toggle-button-fab.component.scss']
})
export class ChatToggleButtonFabComponent implements OnInit {

  constructor(
  	public ats: AngularTokenService,
		public cs: ChatService
	) { }

  ngOnInit() {
  }

}
