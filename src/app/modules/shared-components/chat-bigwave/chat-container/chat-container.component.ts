import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../../../core/services/chat.service';
import {Conversation, Message, ProductDetailCotization} from '../../../../core/abstract/interfaces/messages.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {CotizationDetailComponent} from '../cotization-detail/cotization-detail.component';

@Component({
  selector: 'app-chat-single',
  templateUrl: './chat-container.component.html',
  styleUrls: ['./chat-container.component.scss']
})
export class ChatContainerComponent implements OnInit {

	currentChatOpened: Conversation = null;
	form: FormGroup;

	@ViewChild('Scrollable') private messagesContainer: ElementRef;

  constructor(
  	public chatService: ChatService,
		private fb: FormBuilder,
		private dialog: MatDialog
	) { }

  ngOnInit() {
  	this.form = this.fb.group({
			send_message: ['']
		});

  	this.initSubscriptions();
  }

	initSubscriptions(){
		this.chatService.currentChatOpened.subscribe((current) =>  {
			this.currentChatOpened = current;
			setTimeout(() => this.scrollToBottom(), 1);
		});
	}

	scrollToBottom(): void {
  	if(!this.messagesContainer){
  		return;
		}

		try {
			this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;
		} catch(err) {
			console.error('Error al scroll', err);
		}
	}

	sendMessage(event?: Event){
  	if(event){
			event.preventDefault();
		}

  	const messageBox = this.form.controls['send_message'];
  	const body = messageBox.value as string;
  	if(!body){
  		return;
		}

  	this.chatService.sendMessage(body, this.currentChatOpened.id, this.currentChatOpened.sender_messageable.type, this.currentChatOpened.sender_messageable.id);
  	messageBox.setValue(null);
  	this.form.controls['send_message'].setErrors(null);
	}

	getCotizationByIndex(index: number): ProductDetailCotization{
  	return  this.currentChatOpened.cotization.details[index];
	}

	openDetailCotization(id){
  	this.dialog.open(CotizationDetailComponent, {
  		data: id,
			width: '80%'
		})
	}

	isCurrentUser(message: Message){
  	return message.messageable_id !== this.currentChatOpened.receptor_messageable.id;
	}

}
