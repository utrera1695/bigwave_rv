import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CotizationService} from '../../../../core/services/cotization.service';
import {CotizationStatus} from '../../../../core/abstract/enums/cotization-status.enum';
import {CotizationStatusDicc} from '../../../../core/abstract/functions/dictionary.function';
import {BigwaveUtils} from '../../../../core/abstract/class/BigwaveUtils.class';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {CotizationAtributes} from '../../../../core/abstract/interfaces/cotization/get-cotization.interface';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AdminService} from '../../../../core/services/admin.service';

@Component({
  selector: 'app-cotization-detail',
  templateUrl: './cotization-detail.component.html',
  styleUrls: ['./cotization-detail.component.scss']
})
export class CotizationDetailComponent implements OnInit {

	form: FormGroup;

	cotizationStatusKeys: CotizationStatus[] = BigwaveUtils.getCotizationStatusAsArray();
	diccStatus = CotizationStatusDicc();

	isOwnProfile: boolean;

	inFetch = true;

	cotizationData: CotizationAtributes;

  constructor(
  	private cotizationService: CotizationService,
		@Inject(MAT_DIALOG_DATA) public id: any,
		private ui: UIComponentsService,
		private dialogRef: MatDialogRef<CotizationDetailComponent>,
		private fb: FormBuilder,
		private adminService: AdminService
	) { }

  ngOnInit() {
  	this.getCotizationDetail()
			.then(this.getIsOwnProfile.bind(this))
			.then(() => this.inFetch = false);
	}

	async getCotizationDetail(){
		try{
			this.cotizationData = (await this.cotizationService.getOne(this.id).toPromise()).body.data.attributes;
		}catch (e) {
			this.ui.internalGenericError(e);
			this.close();
		}
	}

	async getIsOwnProfile(){
		try{
			this.isOwnProfile = (await this.adminService.isOwnProfile(this.cotizationData.cotizable.id).toPromise()).body;
			this.createAndSetForm();
		}catch (e) {
			this.ui.internalGenericError(e);
			this.close();
		}
	}

	close() {
		this.dialogRef.close();
	}

	createAndSetForm(){
		const form = this.fb.group({items: this.fb.array([]), stage: [this.cotizationData.stage, Validators.required]});
		const formArray = form.get('items') as FormArray;
		this.cotizationData.details.forEach(
			(detail) => {
				const product = this.getProductById(detail.product_id);
				const control = { // TODO
					id: [product.id],
					item_id: [detail.item_id],
					quantity: [detail.quantity, [Validators.required, Validators.max(product.stock)]]
				};

				// Iteramos sobre las opciones seleccionadas
				detail.options_values.forEach(
					(option) =>{
						// Como es inpredecible saber cuales son los nombres de las keys
						// Se ootiene las keys de la posición actual para poder obtener su nombre y su value
						Object.keys(option).forEach(
							(key) =>{
								Object.assign(control, {[key]: [option[key], Validators.required]});
							}
						);
					}
				);

				formArray.push(this.fb.group(control));
			}
		);
		this.form = form;
		console.log(this.form);
	}

	getProductById(id: number){
  	return this.cotizationData.products.filter(p => p.id === id)[0];
	}

	getDetailById(id: number){
  	return this.cotizationData.details.filter(d => d.product_id === id)[0];
	}

	getFormControlsFromItems(){
  	return (this.form.get('items') as FormArray).controls
	}

	stageIsSoldOrLost(){
  	const stage = this.cotizationData ? this.cotizationData.stage : null;
  	return stage === CotizationStatus.SOLD || stage === CotizationStatus.LOST;
	}

	save(){
  	/*if(!this.isOwnProfile){
  		return;
		}*/

  	const values = this.form.value as any;
  	const toUpdate = {stage: values['stage'], price: 0 , items: []};

		(values['items'] as any[]).forEach(
			(value) => {
				const itemToPush = {item_id: value['item_id'], product_id: value['id'], quantity: value['quantity'], option_values: []};
				const product = this.getProductById(itemToPush.product_id);

				toUpdate.price += (itemToPush.quantity * product.price);

				product.options.forEach(
					(opt) =>{
						itemToPush.option_values.push({[opt.id]: value[opt.id]});
					}
				);

				toUpdate.items.push(itemToPush);
			}
		);

		this.inFetch = true;
		this.cotizationService.update(this.id, toUpdate).subscribe(
			(resp) => {
				this.inFetch = false;
				this.ui.showSnackNotification('Se ha modificado la información exitosamente', 'OK');
				this.close();
			}, (error) => {
				this.ui.internalGenericError(error);
			}
		);
	}

}
