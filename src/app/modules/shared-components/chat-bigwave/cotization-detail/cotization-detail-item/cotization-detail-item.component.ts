import {Component, Input, OnInit} from '@angular/core';
import {Detail, Product} from '../../../../../core/abstract/interfaces/cotization/get-cotization.interface';
import {AbstractControl, FormArray, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-cotization-detail-item',
  templateUrl: './cotization-detail-item.component.html',
  styleUrls: ['./cotization-detail-item.component.scss']
})
export class CotizationDetailItemComponent implements OnInit {

	@Input('detail') detail: Detail;
	@Input('form') form: FormGroup;
	@Input('productList') productList: Product[];
	@Input('index') index;
	@Input('isSoldOrLost') isSoldOrLost: boolean;
	@Input('isAdmin') isAdmin: boolean;

  constructor() { }

  ngOnInit() {
  }

  getControlsCount(){
  	return (this.form.get('items') as FormArray).length
	}

	getControlByIndex(index: number) : AbstractControl{
		return (this.form.get('items') as FormArray).at(index)
	}

	getProductById(id){
		return this.productList.filter(p => p.id === id)[0];
	}

	increaseOrDecrease(increase: boolean, index){
		const control = this.getControlByIndex(index).get('quantity');
		control.setValue(increase ? control.value + 1: control.value - 1);
	}

	deleteItem(index){
		const form = this.form.get('items') as FormArray;
		form.removeAt(index);
	}

}
