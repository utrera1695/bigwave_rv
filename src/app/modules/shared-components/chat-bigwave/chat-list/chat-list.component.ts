import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../../../core/services/chat.service';
import {Conversation} from '../../../../core/abstract/interfaces/messages.interface';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {

	chats: Conversation[] = [];

  constructor(
  	private chatService: ChatService
	) { }

  ngOnInit() {
  	this.chatService.chatList.subscribe(
			(msgs) => {
				if (msgs.length) {
					this.chats = msgs;
				}
			}
		);
  }

  setOpenedChat(chat: Conversation){
  	this.chatService.setOpenedChat(chat);
	}

  remove(chatId, profileId?){
  	this.chatService.updateOpenConversation(chatId, false, profileId);
	}

}
