import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularTokenService} from 'angular-token';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {ModalsService} from '../../../core/services/modals.service';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';
import {UserService} from '../../../core/services/user.service';
import {ProfileService} from '../../../core/services/profile.service';
import {takeWhile} from 'rxjs/operators';


@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.scss']
})
export class FollowComponent implements OnInit, OnDestroy {

  @Input('id') id: number;
	@Input('profileType') profileType: ProfileTypes;
	@Input('followOverride') followOverride: boolean = null;

	following: boolean;
	loading: boolean;
	isAlive: boolean;

  constructor(
    private _ts: AngularTokenService,
    private ui: UIComponentsService,
		private modalsService: ModalsService,
		private userService: UserService,
		private profileService: ProfileService
  ) {
  }

  ngOnInit() {
    this.isAlive = true;
  	this.userLoggedInSub();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  get showIsFollowing(){
  	return this._ts.userSignedIn() && this.following;
	}

	userLoggedInSub(){
		this.userService.userLoggedIn.subscribe(
			(bool) => {
				if(bool){
					if(this.followOverride === null){
						this.isProfileFollowed();
					}else{
						this.following = this.followOverride;
						this.followOverride = null;
					}
				}
			}
		)
	}

	isProfileFollowed(){
  	this.loading = true;
  	this.profileService.isFollowing(this.profileType, this.id).pipe(takeWhile(() => this.isAlive)).subscribe(
			(data) => {
				this.following = data;
				this.loading = false;
			}, (error) => {
				this.loading = false;
			}
		)
	}

	toggleFollow(){
		if(!this._ts.userSignedIn()){
			this.openLogin();
			return;
		}

		if(this.following){
			this.unfollow();
		}else{
			this.follow();
		}
	}

  follow() {
  	this.loading = true;
  	this.profileService.follow(this.profileType, this.id).subscribe(
			(data) => {
				this.ui.showSnackNotification(`Has comenzado a seguir`, 'OK');
				this.following = true;
				this.loading = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loading = false;
			}
		);
  }

  async unfollow() {
  	const confirm = await this.ui.showDialogNotification(
  		'¿Desea dejar de seguir?',
			null,
			'Sí'
		).beforeClosed().toPromise();

  	if(!confirm){
  		return;
		}

		this.loading = true;
		this.profileService.unfollow(this.profileType, this.id).subscribe(
			(data) => {
				this.ui.showSnackNotification('Has dejado de seguir', 'OK');
				this.following = false;
				this.loading = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loading = false;
			}
		);
  }

  openLogin() {
    this.modalsService.openLoginDialog()
  }

}
