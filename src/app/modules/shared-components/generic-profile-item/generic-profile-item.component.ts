import {Component, Input, OnInit} from '@angular/core';
import {ProfileCarousel} from '../../../core/abstract/interfaces/profiles-carousel.interface';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';
import {ProfileGlobalSearch} from '../../../core/abstract/interfaces/profile-global-search.interface';
import {BigwaveUtils} from '../../../core/abstract/class/BigwaveUtils.class';

@Component({
  selector: 'app-generic-profile-item',
  templateUrl: './generic-profile-item.component.html',
  styleUrls: ['./generic-profile-item.component.scss']
})
export class GenericProfileItemComponent implements OnInit {

  @Input('itemCarousel') itemCarousel: ProfileCarousel;
  @Input('itemSearch') itemSearch: ProfileGlobalSearch;
  @Input('profileTypes') profileTypes: ProfileTypes;
  @Input('visitLabel') visitLabel: string;

  BigwaveUtils = BigwaveUtils;
  constructor() { }

  ngOnInit() {
  }

}
