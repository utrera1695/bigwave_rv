import {Component, Inject, OnInit} from '@angular/core';
import {UserService} from '../../../core/services/user.service';
import {Router} from '@angular/router';
import {AngularTokenService, SignInData} from 'angular-token';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ActionCableBigwaveService} from '../../../core/services/action-cable-bigwave.service';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {CartService} from '../../../core/services/cart.service';
import {RegisterUserComponent} from '../register-user/register-user.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {User} from '../../../core/abstract/interfaces/user.interface';
import {Actions} from '../../../core/abstract/enums/actions.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loading = false;
  hide: boolean;
  form: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router,
    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public action: Actions,
    private _tokenService: AngularTokenService,
    private actionCable: ActionCableBigwaveService,
    private ui: UIComponentsService,
    private cartService: CartService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private sas: AuthService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.form = this.fb.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  loginUser() {
    this.loading = true;
    const data = this.form.value;
    this.doLogin(data, 'loading')
  }

  doLogin(body: SignInData, loading?: string){
    this._tokenService.signIn(body).subscribe(
      (resp) => {
        this.closeLoadings();
        const user = resp.body.data as User;
        this.userService.login(user);
        this.dialogRef.close(true);
        this.ui.showSnackNotification(`Bienvenido ${user.name}`, 'OK');
      }, (error) => {
        this[loading] = false;
        if(error.status === 401){
          this.ui.showSnackNotification('Usuario o contraseña incorrectos, vuelve a intentarlo', 'OK');
          return;
        }

        this.ui.internalGenericError(error);
      }
    );
  }

  openRegisterDialog() {
    this.onNoClick();
    if (this.action === Actions.CREATE_PROFILE) {
      this.dialog.open(RegisterUserComponent, {
        data: this.action,
        width: '400px'
      });
    } else {
      this.dialog.open(RegisterUserComponent, {
        width: '400px'
      });
    }

  }

  openRememberPasswordDialog(){
    this.onNoClick();
    this.router.navigate(['forgot-password']);
  }

  loadingSocialFacebook = false;
  signInWithFacebook(){
    this.loadingSocialFacebook = true;
    this.sas.signIn(FacebookLoginProvider.PROVIDER_ID).then(
      (socialUser) => {
        this.loginOmniauth(socialUser, 'facebook')
      }
    ).catch(
      (error) => {
        console.error(error);
        this.closeLoadings();
        this.ui.showSnackNotification('Ha ocurrido un problema al intentar conectarse con Facebook o intentelo de nuevo', 'OK');
      }
    )
  }

  loadingSocialGoogle = false;
  signInWithGoogle(){
    this.loadingSocialGoogle = true;
    this.sas.signIn(GoogleLoginProvider.PROVIDER_ID).then(
      (socialUser) => {
        this.loginOmniauth(socialUser, 'google')
      }
    ).catch(
      (error) => {
        console.error(error);
        this.closeLoadings();
        this.ui.showSnackNotification('Ha ocurrido un problema al intentar conectarse con Google o intentelo de nuevo', 'OK');
      }
    )
  }

  loginOmniauth(body: SocialUser, provider: 'facebook' | 'google'){
    this.userService.loginOmniauth(body, provider).subscribe(
      (resp) => {
        if(resp.change_password){
          localStorage.setItem('tmp_pass', 'true');
          this.ui.showGenericGlobalMessage(1);
        }
        this.doLogin({login: resp.email, password: resp.temporal_password});
      }, (error) => {
        this.ui.internalGenericError(error);
        this.ui.forceCloseGenericGlobalMessage();
        this.sas.signOut();
      }
    )
  }

  closeLoadings(){
    this.loadingSocialGoogle = false;
    this.loadingSocialFacebook = false;
    this.loading = false;
  }
}
