import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {API_ROUTES} from '../../../app.constants';
import {AngularTokenService} from 'angular-token';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {AlertComponent} from '../../shared-components/alert/alert.component';
import {DomSanitizer, Meta, Title} from '@angular/platform-browser';
import {ChatService} from '../../../core/services/chat.service';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {ShareService} from '@ngx-share/core';
import {ProfileType} from '../../../core/abstract/enums/profile-type.enum';
import {getIconographyProfile} from '../../../core/abstract/functions/iconography.function';
import {MatIconRegistry} from '@angular/material/icon';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {ModalsService} from '../../../core/services/modals.service';

declare var $;

interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'Ordenar por Precio',
    children: [
      { name: 'Menor precio' },
      { name: 'Mayor precio' }
    ]
  }
];

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  private transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  @ViewChild('chat')

  removable = true;
  public profileType: string;
  public profileId: string;
  public profile: any = null;
  public loading = false;
  public user: any;
  public showChat = false;
  public category = null;
  public order = [
    { id: '1', name: 'Menor precio' },
    { id: '2', name: 'Mayor precio' }
  ];

  titles: string = 'My first AGM project';
  lat: number = 51.678418;
  lng: number = 7.809007;

  selectOrder = '';
  selectedFile: any = null;
  pageSize = 9;
  pageSizeOptions: number[] = [];
  resultsPage: any = [];
  resultsDestacados: any = [];
  tabs = 1;
  openComent = true;
  panelOpenState = false;
  follow = false;
  commentNew = '';
  listComments: any = [];
  owner: any = {};
  responseNew = '';
  optionComment = {
    comment: '',
    edit: false,
    delete: false,
    response: false,
    position: null
  };
  optionResponse = {
    comment: '',
    edit: false,
    delete: false,
    position: null,
    positionRes: null
  };

  paginateCom = {
    hideAllResponse: true,
    position: null
  };
  conversacion: any;
  iconography: Map<ProfileType, string> = getIconographyProfile();
  deseos: Array<boolean> = [];

  constructor(
    private _route: ActivatedRoute,
    private _routernav: Router,
    private http: HttpClient,
    private _tokenService: AngularTokenService,
    public dialog: MatDialog,
    private chatService: ChatService,
    private meta: Meta,
    private title: Title,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private ui: UIComponentsService,
    public share: ShareService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
		private modalService: ModalsService
  ) {
    this.matIconRegistry.addSvgIcon(
      "email",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../assets/iconmonstr-email-3.svg")
    );
    this.profileType = this._route.snapshot.paramMap.get('type_profile');
    this.type = this._route.snapshot.paramMap.get('type_profile');
    this.profileId = this._route.snapshot.paramMap.get('profile_id');
    this.dataSource.data = TREE_DATA;
  }

  type = '';

  logNode(node) {
    //console.log(node)
    if (this.resultsPage.length > 0) {
      if (node.name == "Menor precio") {
        //ascendente
        this.resultsPage.sort(function (a, b) {
          return a.price - b.price;
        });
      } else {
        this.resultsPage.sort(function (a, b) {
          return b.price - a.price;
        });
      }
    }
  }

  ngOnInit() {
    //console.log('param::', this._route.snapshot.paramMap)
    //console.log('id::', this.profileId)
    if (this.profileType == undefined || this.profileId == undefined) {
      this.profile = undefined;
    } else {
      this.profileType = this.profileType.toLocaleLowerCase();

      switch (this.profileType) {
        case 'pyme':
          this.getPyme();
          break;
        case 'independent':
          this.getIndependent();
          break;
        case 'seller':
          this.getSeller();
          break;
        default:
          this.loading = true;
          break;
      }
    }
  }

  change(): void { }


  view_ = false;

  share_view(view: boolean) {
    this.view_ = view;
  }

  getPyme() {
    this.ColorBanner = 'color-primary'
    //this.profile = {};
    this.loading = true;
    let url = API_ROUTES.getAPyme().replace(':profile_id', this.profileId);
   // console.log(url)
    //console.log(`${environment.api_url}${url}`)

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

      //  console.log(data)

        this.profile = data['data'];

        this.setMetaSeo(this.profile); //Create SEO meta-data
        this.profile.attributes.products = this.defineStructureProducts(this.profile.attributes.products)
        this.getSubCategoriesInAllProducts();
        this.paginator(this.profile.attributes.products);
        this.oldsProduct = this.profile.attributes.products;

        if (this._tokenService.userSignedIn()) {
          this.owner =  JSON.parse(window.localStorage.getItem('user'));
          this.getProfilesFollowed();
        }
        this.getCommets();
        let urldestacados = 'profiles/' + this.profile.id + '/products_prominent';
        this.http.get(`${environment.api_url}${urldestacados}`).subscribe((data: any) => {
        //  console.log(data.data)
          this.resultsDestacados = data.data;
          this.Destacados();
        });
        this.loading = false;

        /*const dePaso = [];

        for (let i = 0; i < this.profile.attributes.products.length; i++) {
          for (let j = 0; j < this.profile.attributes.products[i].subcategories.length; j++) {
            dePaso.push(this.profile.attributes.products[i].subcategories[j])
          }
        }

        this.subCategoryFiltro = Array.from(new Set(dePaso.map(s => s.id)))
          .map(id => {
            return {
              id: id,
              name: dePaso.find(s => s.id === id).name
            };
          });*/

          this.subCategoryFiltro = this.profile.attributes.subcategories;
      },
      error => {
        if ('_body' in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => { });
          }
          this.ui.showSnackNotification('Revisa tu conexión a internet', 'OK');
        }
      }
    );
  }

  Destacados() {
    $(document).ready(function () {
      $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 5,
        nav: true,
        autoWidth: true,
        responsive: {
          0: {
            items: 3,
          },
          600: {
            items: 4
          },
          1000: {
            items: 5
          }
        }
      });
    });
  }

  private setMetaSeo(profile): void {
    this.title.setTitle(
      `${this.title.getTitle().split('-')[0]}-${profile.attributes.name}`
    );
    const meta = [
      { name: 'title', content: profile.attributes.name },
      { name: 'image', content: profile.attributes.photo },
      { name: 'og:title', content: profile.attributes.name },
      { name: 'og:image', content: profile.attributes.photo },
      { name: 'og:type', content: 'article' },
      { name: 'og:description', content: 'Mira estos maravillosos productos!' },
      { name: 'twitter:title', content: profile.attributes.name },
      { name: 'twitter:card', content: 'summary' },
      {
        name: 'twitter:description',
        content: 'Mira estos maravillosos productos!'
      },
      { itemprop: 'name', content: profile.attributes.name },
      { itemprop: 'image', content: profile.attributes.photo },
      { itemprop: 'description', content: 'Mira estos maravillosos productos!' }
    ];
    this.meta.addTags(meta);
  }

  subCategoryFiltro: Array<any> = [];
  getSeller() {
    //this.profile = {};
    this.ColorBanner = 'color-accent'
    this.loading = true;
    let object = this;
    let url = API_ROUTES.getASeller().replace(':profile_id', this.profileId);
    //console.log(url);
    this.http.get(`${environment.api_url}${url}`).subscribe( (data: any) => {
      //  console.log(data)
        this.profile = data['data'];
        this.profile.attributes.products = this.defineStructureProducts(this.profile.attributes.products)
        this.setMetaSeo(this.profile); //Create SEO meta-data
        this.paginator(this.profile.attributes.products);
        this.oldsProduct = this.profile.attributes.products;
        if (this._tokenService.userSignedIn()) {
          this.owner = JSON.parse(window.localStorage.getItem('user'));
          this.getProfilesFollowed();
        }
        this.getCommets();
        this.loading = false;
        let urldestacados = 'profiles/' + this.profile.id + '/products_prominent';
        this.http.get(`${environment.api_url}${urldestacados}`).subscribe((data: any) => {
        //  console.log(data.data)
          this.resultsDestacados = data.data;
          this.Destacados();
        });
        /*
        const dePaso = [];
        for (let i = 0; i < this.profile.attributes.products.length; i++) {
          for (let j = 0; j < this.profile.attributes.products[i].subcategories.length; j++) {
            //this.subCategoryFiltro =[];
            dePaso.push(this.profile.attributes.products[i].subcategories[j])
          }
        }

        this.subCategoryFiltro = Array.from(new Set(dePaso.map(s => s.id)))
          .map(id => {
            return {
              id: id,
              name: dePaso.find(s => s.id === id).name
            };
          });*/

          this.subCategoryFiltro = this.profile.attributes.subcategories;

      },
      error => {
        if ('_body' in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              //object.errors.push(element);
            });
          }
          this.ui.showSnackNotification('Revisa tu conexión a internet', 'OK');
        }
      }
    );



  }

  banner: string = null;

  getIndependent() {
    //this.profile = {};
    this.ColorBanner = 'color-secondary'
    this.loading = true;
    let object = this;
    let url = API_ROUTES.getAIndependent().replace(
      ':profile_id',
      this.profileId
    );
    //console.log(url);
    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {
      this.profile = data['data'];
      //console.log(this.profile)
      this.banner = this.profile.attributes.banner;
      this.profile.attributes.products = this.defineStructureProducts(this.profile.attributes.products)
      this.setMetaSeo(this.profile); //Create SEO meta-data
      this.paginator(this.profile.attributes.products);
      this.oldsProduct = this.profile.attributes.products;
      if (this._tokenService.userSignedIn()) {
        this.owner = JSON.parse(window.localStorage.getItem('user'));
        this.getProfilesFollowed();
      }
      this.getCommets();
      this.loading = false;

      let urldestacados = 'profiles/' + this.profile.id + '/products_prominent';
      this.http.get(`${environment.api_url}${urldestacados}`).subscribe((data: any) => {
       // console.log(data.data)
        this.resultsDestacados = data.data;
        this.Destacados();
      });

      /*
      const dePaso = [];
      for (let i = 0; i < this.profile.attributes.products.length; i++) {
        for (let j = 0; j < this.profile.attributes.products[i].subcategories.length; j++) {
          //this.subCategoryFiltro =[];
          dePaso.push(this.profile.attributes.products[i].subcategories[j])
        }
      }

      this.subCategoryFiltro = Array.from(new Set(dePaso.map(s => s.id)))
        .map(id => {
          return {
            id: id,
            name: dePaso.find(s => s.id === id).name
          };
        }); */

        this.subCategoryFiltro = this.profile.attributes.subcategories;
    },
      error => {
        if ('_body' in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              //object.errors.push(element);
            });
          }
          this.ui.showSnackNotification('Revisa tu conexión a internet', 'OK');
        }
      }
    );

  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  onPageChanged(e, totalResul) {
    let firstCut = e.pageIndex * e.pageSize;
    let secondCut = firstCut + e.pageSize;
    this.resultsPage = totalResul.slice(firstCut, secondCut);
  }

  paginator(results) {
    if (results.length > this.pageSize) {
      let length = results.length;
      let i = 1;
      let interval = [];
      while (length - this.pageSize > 0) {
        interval.push(this.pageSize * i);
        i += 1;
        length -= this.pageSize;
      }
      interval.push(results.length);
      this.pageSizeOptions = interval;
      this.resultsPage = results.slice(0, this.pageSize);
    } else {
      this.pageSize = results.length;
      this.resultsPage = results;
    }
    for (let index = 0; index < this.resultsPage.length; index++) {
      this.deseos.push(true);
    }
  }

  deseo(i, dentro) {
    this.deseos[i] = dentro;
  }

  orderOn() {
    ////console.log(this.selectOrder)
    if (this.resultsPage.length > 0) {
      if (this.selectOrder == '1') {
        //ascendente
        this.resultsPage.sort(function (a, b) {
          return a.price - b.price;
        });
      } else {
        this.resultsPage.sort(function (a, b) {
          return b.price - a.price;
        });
      }
    }
  }

  detailProduct(product) {
    ////console.log(product)
    this._routernav.navigate([
      'product',
      this.profileType,
      this.profile.id,
      product.id
    ]);
  }

  adminProofile() {
    ////console.log(product)
    this._routernav.navigate(['admin', this.profileType, this.profile.id]);
  }

  openDialog(i, item, follow?, comment?, response?) {
    ////console.log(item)
    if (!this._tokenService.userSignedIn()) {

      this.modalService.openLoginDialog().afterClosed().subscribe(result => {
        if (this._tokenService.userSignedIn()) {
          this.getMyWish();
          this.getProfilesFollowed();
        }
      });

    } else {
      if (i != null && item != null) {
        if (item.wish == null) {
          this.createWishes(item, i);
        } else {
          const dialogRef = this.dialog.open(AlertComponent, {
            data: {
              title: JSON.parse(localStorage.getItem('user')).name,
              message: 'Eliminaras de la lista de deseos a' + item.name,
              NO: 'Cancelar',
              YES: 'Aceptar'
            }
          });

          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              this.deleteWish(item, i);
            }
          });
        }
      }

      if (follow) {
        if (!this.follow) this.followProfile(this.profile);
        else this.unfollowProfile(this.profile);
      }

      if (comment) {
        this.createComment(response, i);
      }
    }
  }

  categoryName = undefined;

  oldsProduct : Array <any> = []

  loading_filtro = false;


  addSearchs(category?, delete_category?) {

    this.loading_filtro = true;

    //console.log(category)
    //console.log(this.subCategoryFiltro)
    if (category) {
      this.categoryName = category.name;
      this.selectedFile = category;
      let url = 'services/products_by_filters?profile_id='+this.profile.id+'&subcategories='+category.id

      this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        //console.log(data)

        this.resultsPage = data;

        this.loading_filtro = false;

      });
    }
    if (delete_category) {
      this.categoryName = undefined;
      this.selectedFile = null;
      this.resultsPage = this.oldsProduct;
      this.loading_filtro = false;
    }




  }

  Color() {
    let color = '';
    if (this.profileType == 'pyme') {
      color = 'warn';
    } if (this.profileType == 'independent') {
      color = 'accent';
    } if (this.profileType == 'seller') {
      color = 'primary';
    }
    return color;
  }

  createWishes(product, i) {
    let url = API_ROUTES.createWishes();
    //"name": "test", "description": "example", "budget": "1200",
    let params = {
      wish: { wisheable_id: product.id, wisheable_type: 'product' }
    };
    this.http.post(`${environment.api_url}${url}`, params).subscribe(
      (data: any) => {

        ////console.log("response de deseo", data)
        if (data != null) {
          //this.results[i].wish = data["data"].id;
          this.resultsPage[i].wish = data['data'].id;
          this.ui.showSnackNotification('Agregado a la lista de deseos', 'OK');
        }
      },
      error => { }
    );
  }

  getMyWish() {
    let wishes = [];
    let url = API_ROUTES.myWishes();
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

        if (data['data'].length) {
          wishes = data['data'];
          ////console.log("wishes", wishes)
          for (let j = 0; j < this.profile.attributes.products.length; j++) {
            for (let i = 0; i < wishes.length; i++) {
              if (
                wishes[i].attributes.wisheable.id ==
                this.profile.attributes.products[j].id
              ) {
                this.profile.attributes.products[j].attributes.wish =
                  wishes[i].id;
              }
            }
          }
        }
      },
      error => { }
    );
  }

  deleteWish(wish, i) {
    let url = API_ROUTES.deleteWishes().replace(':wish_id', wish.wish);
    this.http.put(`${environment.api_url}${url}`, null).subscribe(
      (data: any) => {
        ////console.log("eliminado de deseo", data)

        if (data['data'] != null) {
          // this.results[i].wish = null;
          this.resultsPage[i].wish = null;
          this.ui.showSnackNotification('Eliminado de la lista de deseos', 'OK');
        }
      },
      error => {
        //console.log(error);
      }
    );
  }

  getSubCategoriesInAllProducts() {
    let subCatePro = new Set();

    this.profile.attributes.products.forEach(element => {
      element.subcategories.forEach(ele => {
        subCatePro.add(ele.id);
      });
    });

    this.profile.attributes.categories = this.profile.attributes.categories.map(
      ele => ({
        label: ele.name,
        data: ele.id,
        subcategories: ele.subcategories
        /*.map(ele => ({
          label: ele.name,
          data: ele.id
        }))
        .filter(x => subCatePro.has(x.data))*/
      })
    );




  }

  changeTab(option) {
    // //console.log("event", option)
    this.tabs = option.index;
  }

  viewSection(option) {
    this.tabs = option;
    this.Destacados();
  }

  getCommets() {
    let url = API_ROUTES.getcommmets(this.profileType, this.profile.id);
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        this.listComments = data['data'];
      },
      error => {
        //console.log(error);
      }
    );
  }

  createComment(id?, pos?) {
    var info;
    if (this.commentNew != '' || this.responseNew != '') {
      let url = API_ROUTES.createComment();
      info = {
        comment: {
          body: this.commentNew,
          commentable_id: this.profile.id,
          commentable_type: `${this.profileType.charAt(0).toUpperCase()}${this.profileType.slice(1).toLowerCase()}`
        }
      };
      if (id != undefined) {
        info = {
          comment: {
            body: this.responseNew,
            commentable_id: this.profile.id,
            commentable_type: `${this.profileType.charAt(0).toUpperCase()}${this.profileType.slice(1).toLowerCase()}`,
            comment_id: id
          }
        };
      }

      this.http.post(`${environment.api_url}${url}`, info).subscribe(
        (data: any) => {
          //console.log(data)
          if (id == undefined) this.listComments.unshift(data['data']);
          else {
            let resul = data['data'];
            // //console.log(resul);
            if (this.listComments[pos].attributes.childrens == null) {
              this.listComments[pos].attributes.childrens = [];
            }
            let resp = {
              id: resul.id,
              body: resul.attributes.body,
              user: resul.attributes.user,
              commentable_id: resul.attributes.commentable_id,
              type: resul.type
            };
            this.listComments[pos].attributes.childrens.push(resp);

          }
          this.ui.showSnackNotification('Mensaje publicado', 'OK');
          this.commentNew = '';
          this.responseNew = '';
          this.optionComment.response = false;
        },
        error => {
          //console.log(error)
        }
      );
    } else {
      this.ui.showSnackNotification('Ops tu mensaje esta vacio', 'OK');
    }
  }

  focusMethod() {
    //console.log('focus')
    document.getElementById("new_comentario").focus();
  }

  deleteComments(id, pos, resp?) {
    let url = API_ROUTES.deleteComment(id);
    const dialogRef = this.dialog.open(AlertComponent, {
      data: {
        title: JSON.parse(localStorage.getItem('user')).name,
        message: 'Eliminaras de forma permanente el comentario',
        NO: 'Cancelar',
        YES: 'Continuar'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.http.delete(`${environment.api_url}${url}`).subscribe(
          (data: any) => {

            // let x = this.listComments.findIndex(ele=>{ return ele.id == id})
            // //console.log(x, this.listComments)
            if (resp == undefined) this.listComments.splice(pos, 1);
            else {
              this.listComments[pos].attributes.childrens.splice(resp, 1);
            }
            this.ui.showSnackNotification('Mensaje eliminado', 'OK');
          },
          error => {
            //console.log(error);
          }
        );
      }
    });
  }

  updateComment(id, pos, resp?) {
    let url = API_ROUTES.updateComment(id);
    let text =
      resp == undefined
        ? this.optionComment.comment
        : this.optionResponse.comment;
    let editComment = {
      comment: {
        body: text
      }
    };

    if (this.optionComment.comment != '' || this.optionResponse.comment != '') {
      this.http.put(`${environment.api_url}${url}`, editComment).subscribe(
        (data: any) => {

          //console.log(data);
          if (resp == undefined) {
            this.listComments[pos].attributes.body = text;
            this.listComments[pos].attributes.body_update = true;
          } else {
            this.listComments[pos].attributes.childrens[resp].body = text;
            this.listComments[pos].attributes.childrens[
              resp
            ].body_update = true;
          }
          this.ui.showSnackNotification('Mensaje editado', 'OK');

          if (resp == undefined) {
            this.optionComment.edit = false;
            this.optionComment.position = null;
            this.optionComment.comment = '';
          } else {
            this.optionResponse.edit = false;
            this.optionResponse.position = this.optionResponse.positionRes = null;
            this.optionResponse.comment = '';
          }
        }
      );
    } else {
      this.ui.showSnackNotification('Ops, tu mensaje esta vacio', 'OK');
    }
  }

  createUpdateEvaluation() {
    this.getRates();
    let url = API_ROUTES.createRate(this.profileType, this.profile.id);
    this.http.post(`${environment.api_url}${url}`, { rate: { score: '4' } }).subscribe(
      (data: any) => {
        //console.log(data);
      },
      error => {
        //console.log(error);
      }
    );
  }

  getRates() {
    let url = API_ROUTES.getRates(this.profileType, this.profile.id);
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        //console.log(data);
      },
      error => {
        //console.log(error);
      }
    );
  }

  followProfile(item) {
    let url = API_ROUTES.followProfile()
      .replace(':type_profile', item.type)
      .replace(':profile_id', item.id);
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        ////console.log(data)

        var name = item.attributes.name;
        this.follow = true;
        this.ui.showSnackNotification('Ahora sigues a ' + name, 'OK');
      },
      error => { }
    );
  }

  unfollowProfile(item) {
    let url = API_ROUTES.unfollowProfile()
      .replace(':type_profile', item.type)
      .replace(':profile_id', item.id);
    let params = { unfollow: { profile_id: item.id } };
    this.http.post(`${environment.api_url}${url}`, params).subscribe(
      (data: any) => {

        var name = item.attributes.name;
        if (data != null) {
          this.follow = false;
          this.ui.showSnackNotification('has dejado de seguir a' + name, 'OK');
        }
      },
      error => { }
    );
  }

  getProfilesFollowed() {
    let url = API_ROUTES.getFollowing().replace(
      ':type_profile',
      this.profileType
    );
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

        let resul = data['data'];
        if (resul.length > 0) {
          let x = resul.find(ele => {
            return ele.id == this.profile.id;
          });
          if (x != undefined) this.follow = true;
        }
      },
      error => { }
    );
  }

  openContactDialog(): void {
    this.modalService.openContactoDialog();
  }

  defineStructureProducts(array) {
    return array = array.map(
      element => ({
        id: element.id,
        type_profile: element.type_profile,
        photo: '',
        name: element.name,
        wish: null,
        price: element.price,
        subcategories: element.subcategories,
        stock: element.stock,
        url_get: element.url_get
      })
    );
  }

  ColorBanner;

  getMyStyles() {

    let myStyles;

    if (this.banner != null) {
      myStyles = {
        'background': '#fff url(' + this.banner + ')'
      };
    }
    return myStyles;

  }

  getMyClass() {
    let MyClass;
    if (this.banner == null) {
      MyClass = this.ColorBanner;
    }
    return MyClass
  }
}
