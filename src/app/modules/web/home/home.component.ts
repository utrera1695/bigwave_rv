import {Component, OnDestroy, OnInit} from '@angular/core';
import {API_ROUTES} from '../../../app.constants';
import {AngularTokenService} from 'angular-token';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {ModalsService} from '../../../core/services/modals.service';
import {AdminService} from '../../../core/services/admin.service';
import {LocationService} from '../../../core/services/location.service';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';
import {ProfileCarousel} from '../../../core/abstract/interfaces/profiles-carousel.interface';
import {ProductsByCarousel} from '../../../core/abstract/interfaces/products-by-carousel.interface';
import {CategoriesByListingProductCarousel} from '../../../core/abstract/interfaces/categories-by-listing-product-carousel.iterface';
import {takeWhile} from 'rxjs/operators';
import {Actions} from '../../../core/abstract/enums/actions.enum';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  loading = true;

  pymes: ProfileCarousel[] = [];
  sellers: ProfileCarousel[] = [];
  independents: ProfileCarousel[] = [];
  categories: CategoriesByListingProductCarousel[] = [];
  lastestsProducts: ProductsByCarousel[] = [];
  wishedProducts: ProductsByCarousel[] = [];

  storeName: string;

  ProfileTypes = ProfileTypes;

  constructor(private router: Router,
    public _tokenService: AngularTokenService,
    public dialog: MatDialog,
    private ui: UIComponentsService,
    private http: HttpClient,
    private commerceService: AdminService,
    private locationService: LocationService,
    private modalService: ModalsService
  ) { }

  ngOnInit() {
  	this.onChangeCountryStateSub();
  }

  onChangeCountryStateSub(){
  	this.locationService.currentCountryState.pipe(takeWhile(() => this.alive)).subscribe(
			() => {
				this.getHome();
			}
		)
	}

	alive = true;

	ngOnDestroy(): void {
		this.alive = false;
	}

  async submitStoreName(form: NgForm) {
    if (!form.value.storeName) {
      this.ui.showSnackNotification('Ingresa un nombre para empezar', 'OK');
      return;
    }

    this.commerceService.currentProfileCreateName.next(form.value.storeName);
    if (!this._tokenService.userSignedIn()) {
      const isLogged = await this.modalService.openLoginDialog(Actions.CREATE_PROFILE).afterClosed().toPromise();
      if(isLogged){
        this.goToDashboard();
      }
    } else {
      this.goToDashboard();
    }
  }

  goToDashboard(){
    this.router.navigate(['/profile/comenzar-ahora']);
  }

  //----------------------------------------------------------//
	clearAll(){
		this.pymes = [];
		this.sellers = [];
		this.independents = [];
		this.lastestsProducts = [];
		this.wishedProducts = [];
		this.categories = [];
	}

  getHome() {
		this.clearAll();
		this.loading = true;

    let url = API_ROUTES.getHome(this.locationService.getCurrentCode());

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (body: any) => {
      	const data = body['data'];
        this.pymes = data['profiles_by_created_products'];
        this.sellers = data['sellers_by_followers'];
        this.independents = data['independents_by_created_at'];
        this.lastestsProducts = data['products_by_created_at'];
        this.wishedProducts = data['products_by_wishes'];
        this.categories = data['categories_listing_by_products'];
				this.loading = false;
			},
      error => {
      	this.loading = false;
        this.ui.internalGenericError(error);
      }
    );
  }
}


