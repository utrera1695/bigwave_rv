import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { API_ROUTES, CONSTANTS } from '../../../app.constants';
import { AngularTokenService } from 'angular-token';
import { MatSnackBar, MatDialog } from '@angular/material';
import { UIComponentsService } from '../../../core/services/material-ui.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ShareService } from '@ngx-share/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalsService } from '../../../core/services/modals.service';
import { getIconographyProfile } from '../../../core/abstract/functions/iconography.function';
import { ProfileType } from '../../../core/abstract/enums/profile-type.enum';


@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.css']
})
export class DetailProductComponent implements OnInit {
  optionControl = new FormControl('', [Validators.required]);
  iconography: Map<ProfileType, string> = getIconographyProfile()
  public profileType: string;
  public profileId: string;
  public productId: string;
  public loading = false;
  blockAddItem = false;
  product: any = {};
  pymes = {};
  independents = {};
  sellers = {};
  products = [];
  categories: any = [];
  errors: any = [];
  type: string;
  basket: number;
  wish: boolean;
  followingStore: boolean;
  wishData: any;
  cart: any = {
    custom_field_ids: [],
    option_ids: [],
    option_values: {},
    quantity: '1',
    selectOption: [],
  };
  followers: any;
  images = [];
  imageData = [
    {
      srcUrl: 'https://preview.ibb.co/jrsA6R/img12.jpg',
      previewUrl: 'https://preview.ibb.co/jrsA6R/img12.jpg'
    },
    {
      srcUrl: 'https://preview.ibb.co/kPE1D6/clouds.jpg',
      previewUrl: 'https://preview.ibb.co/kPE1D6/clouds.jpg'
    },
    {
      srcUrl: 'https://preview.ibb.co/mwsA6R/img7.jpg',
      previewUrl: 'https://preview.ibb.co/mwsA6R/img7.jpg'
    },
    {
      srcUrl: 'https://preview.ibb.co/kZGsLm/img8.jpg',
      previewUrl: 'https://preview.ibb.co/kZGsLm/img8.jpg'
    }
  ];
  constructor(
    private _route: ActivatedRoute,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private ui: UIComponentsService,
    private _tokenService: AngularTokenService,
    public share: ShareService,
    private router: Router,
    private modalService: ModalsService
  ) {
    // this._tokenService.init({ apiBase: CONSTANTS.BACK_URL });
    //console.log(this._route.snapshot)
    this.profileType = this._route.snapshot.paramMap
      .get('type_profile')
      .toLocaleLowerCase();

    this.type = this._route.snapshot.paramMap
      .get('type_profile');

    this.profileId = this._route.snapshot.paramMap.get('profile_id');
    this.productId = this._route.snapshot.paramMap.get('product_id');

    this.basket = 1;
    this.wish = false;
    this.followingStore = false;
    this.followers = [];
  }

  ngOnInit() {
    this.getProduct();
  }

  getProduct() {
    this.product = {};
    this.loading = true;
    const object = this;
    const url = API_ROUTES.getProduct()
      .replace(':type_profile', this.profileType)
      .replace(':profile_id', this.profileId)
      .replace(':product_id', this.productId);
    //console.log('url detalle::', url);
    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {
     // console.log(data);
      this.product = data['data'];
      this.loading = false;


    },
      error => {
        this.loading = false;
        if ('_body' in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => { });
          }
          this.ui.showSnackNotification('Revisa tu conexión a internet', 'OK');
        }
      }
    );
  }


  addBasket() {
    if (this.product.attributes.stock > this.basket) {
      this.basket++;
      this.cart.quantity = this.basket;
    } else {
      this.ui.showSnackNotification('Ha alcanzado el máximo de stock disponible', 'OK');
    }
    //console.log(this.cart);
  }

  removeBasket() {
    if (this.basket > 1) {
      this.basket--;
      this.cart.quantity = this.basket;
    }
    //console.log(this.cart);
  }

  openProfile() {
    this.router.navigate(['/profiles/', this.type, this.product.attributes.productable.id]);
  }

  addItemToCart() {
    if (this._tokenService.userSignedIn()) {
      if (this.checkForm()) {
        this.blockAddItem = true;
        this.cart.quantity = this.basket;
        // //console.log(this.cart);
        const url = API_ROUTES.addCart(this.product.id);
        this.http.post(`${environment.api_url}${url}`, this.cart).subscribe(response => {
          this.blockAddItem = false;
          this.ui.showSnackNotification('Se ha guardado el producto en carrito', 'OK');

        });
      } else {
        this.ui.showSnackNotification('Debes seleccionar todas las opciones del producto para continuar', 'OK');
      }
    } else {
      this.ui.showSnackNotification('Registrate para agregar productos al carro de compras!', 'REGISTRO', 5000, 'center').subscribe(
        () => {
          this.modalService.openRegisterDialog();
        }
      )
    }
  }

  check_uno: boolean = false;
  check_dos: boolean = false;

  checkForm(): boolean {
    let result = false; // Valid form value
    this.product.attributes.options.forEach((option, index) => {
      if (!this.cart.option_values[option.id]) {
        result = true;
      }
    });
    return result;
  }


  valuechange(event, i, j) {
    if (this.basket < 1) {
      this.basket = 1;
      this.messages('Debe tener al menos un producto');
    }
    if (this.basket > this.product.attributes.stock) {
      this.basket = 1;
      this.messages(
        'La cantidad máxima en stock es de ' + this.product.attributes.stock
      );
    }
  }

  view_modal: boolean = true;

  messages(message) {
    this.ui.showSnackNotification(message, 'OK');
  }

  view(resp: boolean) {
    this.view_modal = resp;
  }

  selectOption:Array<any>=[];

  onSubmit(event,i) {
    //console.log(event,i)
    this.cart.selectOption[i]=event.value
    //console.log('actual',this.cart.selectOption)
  }

}
