import { Component, OnInit } from '@angular/core';
import { ImagesService } from '../../../../core/services/images.service';
declare var $: any;
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
  selector: 'app-imgs-product',
  templateUrl: './imgs-product.component.html',
  styleUrls: ['./imgs-product.component.scss']
})
export class ImgsProductComponent implements OnInit {
  images = [];
  mainImage = 0;
  constructor(
    private imgService: ImagesService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
  ) {
    this.matIconRegistry.addSvgIcon(
      "arrow_riht",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../assets/iconmonstr-arrow-79.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "arrow_left",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../assets/iconmonstr-arrow-37.svg")
    );
  }

  ngOnInit() {
    /**
     * Método para traer imágenes de pruebas
     */
    //this.meta.removeTag('name="imagen"');

    this.imgService.getTestImages().subscribe((data: any) => {
      data.forEach((images, index) => {
        if (index < 5) {
          this.images.push(images);
        }
      });

      //this.meta.addTag({ name: 'imagen', property: 'og:image', content: this.images[this.mainImage].url });

      $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
          loop: false,
          margin: 10,
          responsiveClass: true,
          navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 1
            },
            1000: {
              items: 1
            }
          }
        });
      });
    });

  }

  Mover(opcion) {
    var owl = $('.owl-carousel');
    owl.owlCarousel();
    if (opcion == 'siguiente') {
      owl.trigger('next.owl.carousel');
    } else {
      owl.trigger('prev.owl.carousel', [300]);
    }
  }

  changeMainImage(index: number) {
    this.mainImage = index;
  }

  mainImageUrl() {
    if (this.images[this.mainImage]) {
      return this.images[this.mainImage].url;
    }
    return false;
  }

  activeThumbnail(index: number) {
    return this.mainImage === index;
  }

}
