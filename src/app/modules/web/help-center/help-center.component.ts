import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ModalsService} from '../../../core/services/modals.service';
import {PoliticsPrivacyComponent} from '../../shared-components/footer/politics-privacy/politics-privacy.component';

@Component({
  selector: 'app-help-center',
  templateUrl: './help-center.component.html',
  styleUrls: ['./help-center.component.scss']
})
export class HelpCenterComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private modalService: ModalsService
  ) {

  }

  ngOnInit() {
  }

  openContactDialog(): void {
    this.modalService.openContactoDialog();
  }

  openPoliticsDialog(): void {
    const dialogRef = this.dialog.open(PoliticsPrivacyComponent)

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
