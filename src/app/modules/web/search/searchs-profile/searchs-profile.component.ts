import {Component, OnInit, Inject, ChangeDetectorRef, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { Router } from '@angular/router';
import { CONSTANTS, API_ROUTES } from '../../../../app.constants';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { MediaMatcher } from '@angular/cdk/layout';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { ProfileType } from '../../../../core/abstract/enums/profile-type.enum';
import { getIconographyProfile } from '../../../../core/abstract/functions/iconography.function';
import { LocationService } from '../../../../core/services/location.service';
import {takeWhile} from 'rxjs/operators';



interface FoodNode {
  name: string;
  id: string;
  children?: any[];
}

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: string
}

const TREE_DATA3: FoodNode[] = [
  {
    name: 'Orden alfabetico',
    id: '3',
    children: [
      { name: 'A-Z' },
      { name: 'Z-A' }
    ]
  }
];



@Component({
  selector: 'app-searchs-profile',
  templateUrl: './searchs-profile.component.html',
  styleUrls: ['./searchs-profile.component.scss']
})
export class SearchsProfileComponent implements OnInit, OnDestroy {

  page = 1;
  perpage = 40;
  filtro = '';
  iconography: Map<ProfileType, string> = getIconographyProfile()

  profileList = [
    { name: 'Tienda', id: 'pyme', state: false, id_may: 'Pyme' },
    { name: 'Independiente', id: 'independent', state: false, id_may: 'Independent' },
    { name: 'Persona', id: 'seller', state: false, id_may: 'Seller' }
  ];

  loadingCategories: boolean = false;
  loadingProfile: boolean = false;
  categories: any[];
  profiles: any;


  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  TREE_DATA2?: FoodNode[] = [];

  private transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id
    };
  }

  treeControl2 = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener2 = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource2 = new MatTreeFlatDataSource(this.treeControl2, this.treeFlattener2);

  treeControl3 = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener3 = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource3 = new MatTreeFlatDataSource(this.treeControl3, this.treeFlattener3);

  page_size: number = 8;
  page_number: number = 1;
  tamano: number = 0;
  pageSizeOptions: number[] = [];

  constructor(
    private _route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog, @Inject(MAT_DIALOG_DATA)
    private data: any,
    private http: HttpClient,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public locationService: LocationService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.dataSource2.data = this.TREE_DATA2;
    this.dataSource3.data = TREE_DATA3;
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.getParams();
		this.initCountrySelectedSub();
		this.getCategories();
    this.profileType();
  }

  getParams() {
    this._route.queryParams.subscribe(params => {
      if (params.profiles) {

        this.types = params.profiles;

        let prof = params.profiles.split("-");

        for (let index = 0; index < prof.length; index++) {
          for (let j = 0; j < this.profileList.length; j++) {
            if (prof[index] == this.profileList[j].id_may || prof[index] == this.profileList[j].id) {
              this.profileList[j].state = true;
            }
          }
        }
      }
    })
  }

	initCountrySelectedSub(){
		this.locationService.currentCountryState.pipe(takeWhile(() => this.alive)).subscribe(
			(state) => {

				const params = { profiles: this.types, categories: this.categorySelected ? this.categorySelected.id: null, countries_codes: state ? state.code: null};
				const criteria = this.criteriaSearchsCateggory(params);
				let url = API_ROUTES.searchsLookForProfiles(`${criteria}&page=${this.page}&per_page=${this.perpage}`);

				this.getProfiles(url);
			}
		);
	}

	alive = true;

	ngOnDestroy(): void {
		this.alive = false;
	}

  getCategories() {

    this.loadingCategories = true;
    let url = API_ROUTES.getCategories();
    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {

      ////console.log(data);
      if (data['data'].length) {
        this.categories = data['data'];
      }

      this.categories = this.categories.map(function (op) {
        return { "id": op.id, "name": op.attributes.name, "subcategories": op.attributes.subcategories }
      })


      this.TREE_DATA2.push({
        name: 'Filtrar por categoría',
        children: this.categories,
        id: '0'
      });

      this.dataSource2.data = this.TREE_DATA2;


      this.loadingCategories = false;
    },
      error => {
        this.loadingCategories = false;
        if ("_body" in error) {
          error = error._body;
          //console.log("error: ", error);
          if (error.errors && error.errors.full_messages) {

          }

        }
      }
    );
  }

  pagination: any

  getProfiles(url) {

    this.loadingProfile = true;
    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {

      this.pagination = data.meta;
      this.tamano = this.pagination.pagination.total_objects;

      this.profiles = data.data;
      this.profiles = this.profiles.map((element) => ({
        id: element.id,
        slug: element.slug,
        type_profile: element.type_profile,
        photo: element.photo,
        title: element.title,
        category_ids: element.category_ids,
        follow: false
      }))

      if (this.profiles.length <= 4) {
        this.pageSizeOptions = [4]
      } else {
        this.pageSizeOptions = [4, 8]
      }


      this.loadingProfile = false;
    });
  }



  categorySelected;
  OrderName = undefined;
  Old_proflies_all: Array<any> = [];

  logNode_category(node) {
    this.loadingProfile = true;
    this.categorySelected = node;
    this.Old_proflies_all = this.profiles;

    let parametros = {
      profiles : undefined,
      categories : undefined,
    };

    parametros.profiles = this.types;
    parametros.categories = node.id;

    let urlProfiles = API_ROUTES.searchsLookForProfiles(
      `${this.criteriaSearchsCateggory(parametros)}&page=${this.page}&per_page=${
      this.perpage
      }`
    );

    this.getProfiles(urlProfiles);
  }

  criteriaSearchsCateggory(params) {

    let criteria = '';

    if (params.profiles) {
      if (criteria != '') criteria += `&profiles=${params.profiles}`;
      else criteria += `profiles=${params.profiles}`;
    }

    if (params.categories) {
      if (criteria != '') criteria += `&categories=${params.categories}`;
      else criteria += `categories=${params.categories}`;
    }

    if (params.countries_codes) {
      if (criteria != '') criteria += `&countries_codes=${params.countries_codes}`;
      else criteria += `countries_codes=${params.countries_codes}`;
    }

    if (this.locationService.hasCountryState() && !params.countries_codes) {
      if (criteria != '') criteria += `&countries_codes=${this.locationService.getCurrentCode().toLowerCase()}`;
      else criteria += `countries_codes=${this.locationService.getCurrentCode().toLowerCase()}`;
    }

    return criteria;
  }

  logNode(node) {
    this.OrderName = node.name;
  }

  types = '';
  Disable_profil = [
    false,
    false,
    false
  ]

  profileType() {

    this.types = '';
    let contador = 0;
    for (let index = 0; index < this.profileList.length; index++) {
      if (this.profileList[index].state == true) {
        if (contador == 0) {
          this.types = this.types + this.profileList[index].id_may
        } else {
          this.types = this.types + '-' + this.profileList[index].id_may
        }
        contador++;
      }
    }

    let minTypeProfiles = 0;
    let indice;

    for (let index = 0; index < this.profileList.length; index++) {
      if (this.profileList[index].state) {
        minTypeProfiles++;
        indice = index;
      }
    }


    if (minTypeProfiles >= 1) {
      this._route.queryParams.subscribe(params => {
        this.router.navigate([`/profiles`], {
          queryParams: {
            profiles: this.types
          }
        })
      })
    }

    if (minTypeProfiles <= 1) {
      this.Disable_profil[indice] = true;
    }

    if (minTypeProfiles >= 2) {
      for (let index = 0; index < this.Disable_profil.length; index++) {
        this.Disable_profil[index] = false;
      }
    }

  }

  remove_category() {
    this.loadingProfile = true;
    this.categorySelected = null;
    this.profiles = this.Old_proflies_all;
    this.tamano = this.pagination.pagination.total_objects;
    this.loadingProfile = false;
  }

  remove_order() {
    this.loadingProfile = true;
    this.OrderName = undefined;
    this.loadingProfile = false;
  }

  handlePage(e: PageEvent) {
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
    this.showProducts_restantes(e);
  }

  showProducts_restantes(e) {

    if (this.profiles.length < this.pagination.pagination.total_objects) {
      if (e.pageIndex > 0) {

        let url = API_ROUTES.searchsLookForProfiles(
          this.pagination.links.next.replace(
            environment.api_url,
            ''
          ),
          true
        );

        this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {

          this.pagination = data.meta;

          let profiles = data.data.map((element) => ({
            id: element.id,
            slug: element.slug,
            type_profile: element.type_profile,
            photo: element.photo,
            title: element.title,
            category_ids: element.category_ids,
            follow: false
          }))

          this.profiles = this.profiles.concat(profiles);

          this.loadingProfile = false;

        });

      }
    }
  }

}
