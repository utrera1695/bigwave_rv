import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSidenav, PageEvent} from '@angular/material';
import {SearchService} from '../../../core/services/search.service';
import {LocationService} from '../../../core/services/location.service';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {ProfileType} from '../../../core/abstract/enums/profile-type.enum';
import {getIconographyProfile} from '../../../core/abstract/functions/iconography.function';
import {MediaMatcher} from '@angular/cdk/layout';
import {debounceTime, takeWhile} from 'rxjs/operators';
import {CategoriesByObject, CountriesByObject, GlobalSearchQuery} from '../../../core/abstract/interfaces/globar-search-data.interface';
import {ProductGlobalSearch} from '../../../core/abstract/interfaces/product-global-search.interface';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {ProfileGlobalSearch} from '../../../core/abstract/interfaces/profile-global-search.interface';
import {Meta} from '../../../core/abstract/interfaces/response.interface';
import {Product} from '../../../core/abstract/interfaces/product.interface';
import {MatPaginator} from '@angular/material/paginator';
import * as _ from 'lodash';

interface SearchParams extends GlobalSearchQuery {
  tab: ActiveTabType;
}

type ActiveTabType = 'profiles' | 'products';

interface ActiveExtraParam {
  id: any;
  name?: string;
}

@Component({
  selector: 'search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
  providers: [SearchService]
})
export class SearchPageComponent implements OnInit, OnDestroy {

  paginator: MatPaginator;

  @ViewChild(MatPaginator) set pag(paginator: MatPaginator) {
    this.paginator = paginator;
  }

  currentParams: SearchParams;
  loadingResults = false;
  loadingRecommendedProducts = false;
  loadingRecentProfiles = false;
  recommendedProducts: Product[] = [];
  recentProfiles: ProfileGlobalSearch[];
  iconography: Map<ProfileType, string> = getIconographyProfile();
  ProfileType = ProfileType;
  productsList: ProductGlobalSearch[] = [];
  profilesList: ProfileGlobalSearch[] = [];
  categories: CategoriesByObject[] = [];
  ubications: CountriesByObject[] = [];
  activeCategories: ActiveExtraParam[] = [];
  activeUbication: ActiveExtraParam;
  meta: Meta;
  orderByModel;
  public orderByValues = [
    {id: 'DESC', name: 'Menor precio'},
    {id: 'ASC', name: 'Mayor precio'}
  ];

  pageSizeOptions: number[] = [6, 12, 24];
  alive = true;


  @ViewChild('sidenav') public sidenav: MatSidenav;
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;


  constructor(
    private router: Router,
    private searchService: SearchService,
    private route: ActivatedRoute,
    public locationService: LocationService,
    private ui: UIComponentsService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.route.queryParams.subscribe((params: SearchParams) => {
      this.currentParams = _.cloneDeep(params);
      this.currentParams.per_page = this.currentParams.per_page || '6';
      this.currentParams.page = this.currentParams.page || '1';

      if (params.tab === 'profiles') {
        this.getProfiles(params);
      } else {
        this.getProducts(params);
      }
    });

    setTimeout(() => this.onCountryStateChangeSub(), 500);
  }

  retrieveData(params: GlobalSearchQuery) {
    this.router.navigate(['/searchs'], {
      relativeTo: this.route,
      queryParams: params
    });
  }

  onCountryStateChangeSub() {
    this.locationService.currentCountryState.pipe(takeWhile(() => this.alive)).subscribe(
      (state) => {
        this.handleExtraParams('ubication', state ? {id: state.code, name: state.name} : null);
      }
    );
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  handleCategorySetInExtraFilters() {
    const activeCategory = this.route.snapshot.queryParams['categories'] as string;
    if (!activeCategory) {
      return;
    }
    const categoriesIds = activeCategory.split('-');

    for (const catId of categoriesIds) {
      const category = this.categories.filter(c => c.id === Number(catId))[0];
      const inList = this.activeCategories.findIndex(c => c.id === category.id) !== -1;
      if (!inList) {
        this.activeCategories.push({id: category.id, name: category.name});
      }
    }

  }

  getProfiles(params: SearchParams) {
    this.loadingResults = true;
    this.searchService.searchProfiles(params).pipe(debounceTime(500)).subscribe(
      (data) => {
        this.profilesList = data.data;
        this.meta = data.meta;
        this.categories = data.filters_by_objects.categories_by_objects;
        this.ubications = data.filters_by_objects.countries_by_objects;
        this.handleCategorySetInExtraFilters();
        this.loadingResults = false;
        if(!this.meta.pagination.total_objects){
          this.getMostRecentProfiles();
        }
      }, error => {
        this.loadingResults = false;
        this.ui.internalGenericError(error);
      });
  }

  getProducts(params: SearchParams) {
    this.loadingResults = true;
    this.searchService.searchProducts(params).pipe(debounceTime(500)).subscribe((data) => {
        this.meta = data.meta;
        this.categories = data.filters_by_objects.categories_by_objects;
        this.ubications = data.filters_by_objects.countries_by_objects;
        this.productsList = data.data;
        this.handleCategorySetInExtraFilters();
        if (this.meta.pagination.total_objects === 0) {
          this.getRecommendedProducts();
        }
        this.loadingResults = false;
      }, error => {
        this.loadingResults = false;
        this.ui.internalGenericError(error);
      }
    );
  }

  getRecommendedProducts() {
    this.loadingRecommendedProducts = true;
    let countryCode = '';
    if (this.locationService.hasCountryState()) {
      countryCode = this.locationService.getCurrentCode().toLowerCase();
    }
    this.searchService.recommendedProducts(countryCode).subscribe((data: any) => {
      this.recommendedProducts = data.data;
      this.loadingRecommendedProducts = false;
    }, error => {
      this.loadingRecommendedProducts = false;
      this.ui.internalGenericError(error);
    });
  }

  getMostRecentProfiles(){
    this.loadingRecentProfiles = true;
    this.searchService.mostRecentProfiles().subscribe(
      (data) => {
        this.recentProfiles = data.data;
        this.loadingRecentProfiles = false;
      }, (error) => {
        this.ui.internalGenericError(error);
        this.loadingRecentProfiles = false;
      }
    )
  }

  setTab(tab: ActiveTabType) {
    this.currentParams.tab = tab;
    this.currentParams.page = '1';
    this.retrieveData(this.currentParams);
  }

  handleExtraParams(type: 'category' | 'ubication', value: ActiveExtraParam, removed?: boolean) {
    if (type === 'category') {
      this.handleExtraCategoryParam(value, removed);
    } else {
      this.currentParams.countries_codes = value ? String(value.id) : null;
      this.activeUbication = value;
    }
    this.currentParams.per_page = this.currentParams.per_page || '6';
    this.currentParams.page = '1';
    this.retrieveData(this.currentParams);
  }

  /**
   * Al agregar/quitar una categoría se lleva bajo esta función
   * @param value
   * @param removed
   */
  handleExtraCategoryParam(value: ActiveExtraParam, removed?: boolean) {
    this.currentParams.categories = this.handleMultiValueParam(this.currentParams.categories, String(value.id), removed);

    const index = this.activeCategories.findIndex(c => c.id === value.id);
    if (!removed && index === -1) {
      this.activeCategories.push(value);
    } else if (removed) {
      this.activeCategories.splice(index, 1);
    }
  }

  /**
   * Maneja el multivalor en un parámetro separandolo por '-'
   * En lo posible verificar que actualparams y added sean typeof 'string'
   * @param actualParams
   * @param added
   * @param deleted
   */
  handleMultiValueParam(actualParams: string, added: string, deleted: boolean) {
    let aux = actualParams ? actualParams.split('-') : [];
    const index = aux.findIndex(t => t === added);
    if (!deleted && index === -1) {
      aux.push(added);
    } else if(deleted) {
      aux.splice(index, 1);
    }

    return aux.length ? aux.join('-') : null; // Evitar parámetro vacío
  }

  handleProfileSwitching(type: ProfileType, event: MatCheckboxChange) {
    this.currentParams.profiles = this.handleMultiValueParam(this.currentParams.profiles, type, !event.checked);
    this.currentParams.page = '1';
    this.retrieveData(this.currentParams);
  }

  isLastProfileSwitch(type: ProfileType) {
    return this.currentParams.profiles === type;
  }

  onPageEvent($event: PageEvent) {
    let page = $event.pageIndex + 1;
    if (this.currentParams.per_page !== $event.pageSize.toString()) {
      page = 1;
      this.paginator.pageIndex = 0;
    }
    this.currentParams.page = String(page);
    this.currentParams.per_page = String($event.pageSize);

    this.retrieveData(this.currentParams);
  }
}
