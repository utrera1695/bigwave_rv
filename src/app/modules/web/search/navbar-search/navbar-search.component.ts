import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SearchService} from '../../../../core/services/search.service';
import {MapsAPILoader} from '@agm/core';
import {LocationService} from '../../../../core/services/location.service';
import {ModalsService} from '../../../../core/services/modals.service';

declare var google;


@Component({
  selector: 'navbar-search',
  templateUrl: './navbar-search.component.html',
  styleUrls: ['./navbar-search.component.scss'],
  providers: [SearchService]
})

export class NavbarSearchComponent implements OnInit {
  public loading: boolean = false;

  constructor(
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    public locationService :LocationService,
    private modalService: ModalsService
    )
    { }

  ngOnInit() {
		this.geoLocateUser();
  }

  goToSearch(search: string) {
    let searchFormat = search.replace(/\s\s+/g, ' ');

    if (this.locationService.hasCountryState()) {
    	const code = this.locationService.getCountryState().code;
      this.router.navigateByUrl(`/searchs?q=${searchFormat}&countries_codes=${code}&profiles=${'pyme-independent-seller'}&tab=${'all'}`);
    } else{
			this.router.navigateByUrl(`/searchs?q=${searchFormat}&profiles=${'pyme-independent-seller'}&tab=${'all'}`);
		}

  }

	geoLocateUser(){
		this.mapsAPILoader.load().then(() => {
			navigator.geolocation.getCurrentPosition(position => {
				this.doGeoCode({ lat: position.coords.latitude, lng: position.coords.longitude});
			}, (error) => {
				this.modalService.openSelectCountry();
			});

		});
	}

	doGeoCode(location){
		const geocoder = new google.maps.Geocoder();

		geocoder.geocode({ location: location }, (results: any[]) => {
			if(!results || !results.length){
				return;
			}
			for(location of results) {
				const types = location.types as any[];
				if(types.includes('country')){
					const adressesComponents = location.address_components as any[];
					this.locationService.setCountryState(adressesComponents[0].long_name, adressesComponents[0].short_name);
				}
			}
		});
	}
}
