import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AngularTokenService } from 'angular-token';
import { API_ROUTES } from '../../../app.constants';
import { NgForm } from '@angular/forms';
import { UIComponentsService } from '../../../core/services/material-ui.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocationService } from '../../../core/services/location.service';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  types: any = [];

  countries: any = [];

  contact: any = {
    name: null,
    comments: null,
    country: null,
    phone: null,
    email: null,
    contact_type_id: 0,
    profile_id: null
  }

  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;


  selected = ""
  private headers: any;
  constructor(
    public dialogRef: MatDialogRef<ContactFormComponent>,
    public http: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ui: UIComponentsService,
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    public locationService: LocationService,

  ) {
    this.contact.country = this.locationService.getCurrentCode();
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
  }

  ngOnInit() {

    this.mobileQuery = this.media.matchMedia('(max-width: 800px)');//Query para un breakpoint
    this._mobileQueryListener = () => this.changeDetectorRef.detectChanges();//Mobile Query devolverá cuando sea ejecutado el detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener);//Listener del rezise event

    this.mobileQuery.removeListener(this._mobileQueryListener);

    this.getTypeContact();
    this.getCountries();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getCountries() {

    let url = API_ROUTES.getCountrys();

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

        this.countries = data['countries'];
        ////console.log(this.countries);
      },
      error => {
      }
    );
  }

  getTypeContact() {
    let url = API_ROUTES.getTypeContact();

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

        this.types = data['data'];
        this.defaultType();
      },
      error => {
      }
    );

  }

  Contacto(){

  }

  onSubmit() {
    var data = {
      contact : this.contact
    };
   // console.log(data)

    ////console.log(data);
    ////console.log(form.value);
    let url = API_ROUTES.postContactForm();

    this.http.post<any>(`${environment.api_url}${url}`, data).subscribe(
      (data: any) => {
        //console.log("se guardo");
        if (data) {

          ////console.log(data);
          this.onNoClick();
          this.ui.showSnackNotification("Mensaje enviado, Gracias por comunicarte con nosotros", "OK");
        }
      },
      error => {
        console.error(error);
        this.ui.showSnackNotification("Ha ocurrido un error interno", "OK");
      }
    );

  }

  asignContactTypeID(event) {
    ////console.log(event);
    for (let i = 0; i < this.types.length; i++) {
      if (this.types[i].attributes.name == event) {
        this.contact.contact_type_id = this.types[i].id;
      }
    }
   // console.log(this.contact.contact_type_id)
  }

  defaultType() {
    if (this.data != undefined)
      this.selected = this.data.id;
  }

}
