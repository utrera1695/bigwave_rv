import { SidenavServiceService } from './../../../core/services/sidenav-service.service';
import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { API_ROUTES } from '../../../app.constants';
import { AngularTokenService } from 'angular-token';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { MatSidenav, PageEvent, MatDialog } from '@angular/material';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { MediaMatcher } from '@angular/cdk/layout';
import { LocationService } from '../../../core/services/location.service';
import { ModalsService } from '../../../core/services/modals.service';
import {takeWhile} from 'rxjs/operators';


interface FoodNode {
  name: string;
  id: string;
  products_charged: string;
  children?: any[];
}

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: string
}

const TREE_DATA3: FoodNode[] = [
  {
    name: 'Ordenar por Precio',
    id: '3',
    products_charged: "3",
    children: [
      { name: 'Menor precio' },
      { name: 'Mayor precio' }
    ]
  }
];

@Component({
  selector: 'app-products-by-category',
  templateUrl: './products-by-category.component.html',
  styleUrls: ['./products-by-category.component.scss']
})
export class ProductsByCategoryComponent implements OnInit, OnDestroy {

  mobileQuery: MediaQueryList;

  TREE_DATA?: FoodNode[] = [];
  TREE_DATA2?: FoodNode[] = [];

  private transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id,
      products_charged: node.products_charged
    };
  }

  tabs = 0;

  Alfa = undefined;

  view: boolean;

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  //data 2
  treeControl2 = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener2 = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource2 = new MatTreeFlatDataSource(this.treeControl2, this.treeFlattener2);

  //data 3

  treeControl3 = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener3 = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource3 = new MatTreeFlatDataSource(this.treeControl3, this.treeFlattener3);


  private _mobileQueryListener: () => void;

  @ViewChild('snav') public mainNav: MatSidenav;

  NotProducts = false;
  NotProfiles = false;

  constructor(
    private route: ActivatedRoute,
    public _tokenService: AngularTokenService,
    private http: HttpClient,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private sidenavService: SidenavServiceService,
    private router: Router,
    public locationService: LocationService,
    public dialog: MatDialog,
    private modalService: ModalsService
  ) {
    this.dataSource.data = this.TREE_DATA;
    this.dataSource2.data = this.TREE_DATA2;

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.dataSource3.data = TREE_DATA3;
  }

  order_by = undefined;

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  filtro: string = ""
  filtro2: string = ""

  categoryID?: string = '0';
  subcategoryID: number = 0;
  categoryName?: string = '';
  type: string;
  loadingProducts: boolean = false;
  loadingProducts_by_subacategory: boolean = false;
  loadingProfil: boolean = false;
  isCategory: boolean = false;
  isSubcategory: boolean = false;

  products: Array<any> = [];
  profiles: Array<any> = [];
  categories: any = [];
  subcategories_by_category: any = [];

  page_size: number = 8;
  page_number: number = 1;
  pageSizeOptions: number[] = [];

  Sub_categoryName = undefined;

  removable = true;

  //element_paginator profiless
  page_size_profil: number = 8;
  page_number_profil: number = 1;
  tamano_profil: number = 0;
  pageSizeOptionsProfil: number[] = [8, 4];

  ColorBanner = 'color-primary';

  old_product: Array<any> = [];

  ngOnInit() {
		this.mobileQuery.removeListener(this._mobileQueryListener);
		this.onRouteParamsChangeSub();
		this.initCountryStateSub();
  }

  getRouteAndSetData(){
		this.categoryID = this.route.snapshot.params['categoryID'];
		this.categoryName = this.route.snapshot.params['categoryName'];
		this.type = this.route.snapshot.params['type'];
		this.getCategories(this.categoryID);

		this.GetProfiles();
		this.isCategory = true;
		this.getCategoryProducts();
	}

  onRouteParamsChangeSub(){
  	this.route.params.subscribe(
			() => {
				this.getRouteAndSetData();
			}
		)
	}

  initCountryStateSub(){
  	this.locationService.currentCountryState.pipe(takeWhile(() => this.alive)).subscribe(
			(state) => {
				this.getRouteAndSetData();
			}
		)
	}

	alive = true;

	ngOnDestroy(): void {
		this.alive = false;
	}

  logNode(node) {

    if (node.name == "Menor precio") {
      this.order_by = 1
    } else {
      this.order_by = 2
    }
  }

  toggle() {
    this.sidenavService.setSidenav(this.mainNav);
    this.sidenavService.toggle();
  }

  GetProfiles() {

    let url = '';

    if (this.locationService.hasCountryState()) {
      url = API_ROUTES.getProfilesByCategoryByPais(this.categoryID, this.locationService.getCurrentCode());
    } else {
      url = API_ROUTES.getProfilesByCategory(this.categoryID);
    }

    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {

      if (data.data) {
        this.NotProfiles = true;
        this.profiles = [];
      } else {
        this.NotProfiles = false;
        this.profiles = data;
        if (data.length < 4 && data.length > 0) {
          this.page_size_profil = data.length;
          this.pageSizeOptionsProfil = [data.length];
        }
      }

      this.loadingProfil = true;

    });
  }

  handlePageProfil(e: PageEvent) {
    this.page_size_profil = e.pageSize;
    this.page_number_profil = e.pageIndex + 1;
  }

  getCategoryProducts() {

    let url = ''

    if (this.locationService.hasCountryState()) {
      url = API_ROUTES.getProductByCategoryByPais(this.categoryID, this.locationService.getCurrentCode());
    } else {
      url = API_ROUTES.getProductByCategory(this.categoryID);
    }

    this.loadingProducts = true;
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {

				if (data.data) {
          this.NotProducts = true;
          this.products = [];
        } else {
          this.NotProducts = false;
					this.products = data;
					this.old_product = this.products;
          if (data.length <= 4 && data.length > 0) {
            this.page_size = data.length;
            this.pageSizeOptions = [data.length];
          } else {
            this.pageSizeOptions = [4, 8]
            this.page_size = 4;
          }
        }

        this.loadingProducts = false;
      },
      error => {
      }
    );

  }

  openProfile(type_profile, slug) {
    this.router.navigate(['/profiles/', type_profile, slug]);
  }

  goToDetailProduct(result) {
    let type;
    if (this.isCategory == true) {
      type = result.type_profile;
      const typePascal = type.charAt(0).toUpperCase() + type.slice(1);
      this.router.navigate([
        'product',
        typePascal,
        result.productable.id,
        result.id
      ]);
    } else {
      type = result.attributes.type_profile;
      const typePascal = type.charAt(0).toUpperCase() + type.slice(1);
      this.router.navigate([
        'product',
        typePascal,
        result.attributes.productable.id,
        result.id
      ]);
    }

  }

  getSubcategoryProducts() {
    this.isCategory = false;
    this.loadingProducts_by_subacategory = true;
    let url = '';
    if (this.locationService.hasCountryState()) {
      url = API_ROUTES.getProductsbySubcategorybyPais(this.subcategoryID, this.locationService.getCurrentCode());
    } else {
      url = API_ROUTES.getProductsbySubcategory(this.subcategoryID);
    }
    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        this.products = data['data'];
        if (data.data.length <= 4 && data.data.length > 0) {
          this.page_size = data.data.length;
          this.pageSizeOptions = [data.data.length];
        } else {
          this.pageSizeOptions = [4, 8]
        }
        this.isSubcategory = true;
        this.loadingProducts_by_subacategory = false;
      },
      error => {
      }
    );
  }

  getCategories(categoryID?) {
    this.TREE_DATA = [];
    this.TREE_DATA2 = [];
    let url = API_ROUTES.getCategories();
    let data_res: any[] = [];
    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {
      if (data['data'].length)
        this.categories = data['data'];
      this.categories = this.categories.map(function (op) {
        if (categoryID == op.id) {
          data_res.push({
            name: 'Subcategoría',
            children: op.attributes.subcategories,
            id: categoryID,
            products_charged: '2'
          });
        }
        return { "id": op.id, "name": op.attributes.name, "subcategories": op.attributes.subcategories }
      })

      this.TREE_DATA = data_res;
      this.TREE_DATA2.push({
        name: 'Cambiar de categoría',
        children: this.categories,
        id: this.categoryID,
        products_charged: '3'
      });

      this.dataSource.data = this.TREE_DATA;
      this.dataSource2.data = this.TREE_DATA2;
    },
      error => {
        if ("_body" in error) {
					error = error._body;
				}
      }
    );
  }

  logNode_category(node) {
    this.isSubcategory = false;
    this.isCategory = true;
    this.categoryID = node.id;
    this.categoryName = node.name;
    this.Sub_categoryName = undefined;
    this.router.navigate(["products", "category", this.categoryID, this.categoryName]);
  }

  logNode_subcategory(node) {
    this.subcategoryID = node.id;
    this.Sub_categoryName = node.name;
    this.getSubcategoryProducts()
  }

  handlePage(e: PageEvent) {
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  getMyClass() {
    let MyClass;
    MyClass = this.ColorBanner;
    return MyClass
  }

  remove_subcategory() {
    this.isSubcategory = false;
    this.isCategory = true;
    this.Sub_categoryName = undefined
    //this.products = this.old_product;
    this.getCategoryProducts();
  }

  openSelectCountry() {
    this.modalService.openSelectCountry().afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

}
