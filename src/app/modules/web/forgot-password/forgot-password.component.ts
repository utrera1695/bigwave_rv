import {Component, OnInit, ViewChild} from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import {AngularTokenService} from 'angular-token';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {MatHorizontalStepper} from '@angular/material/stepper';
import {ModalsService} from '../../../core/services/modals.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  @ViewChild('stepper') stepper: MatHorizontalStepper;

  loading = false;

  giveDataForm: FormGroup;
  giveCodeForm: FormGroup;
  resetPasswordForm: FormGroup;

  resetPayload = {
    token: '',
    password: '',
    password_confirmation: ''
  };

  hide: boolean;
  hidePC: boolean;
  constructor(
    private userService: UserService,
    private ats: AngularTokenService,
    private fb: FormBuilder,
    private ui: UIComponentsService,
    public modalsService: ModalsService
  ) { }

  ngOnInit() {
    this.giveDataForm = this.fb.group({login: ['', [Validators.required, Validators.email]]});
    this.giveCodeForm = this.fb.group({token: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]});
    this.resetPasswordForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      password_confirmation: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  callResetPassword() {
    this.loading = true;
    const body = this.giveDataForm.value;
    this.ats.resetPassword(body).subscribe(
      () => {
        this.ui.showSnackNotification('Se ha enviado un correo a ' + body['login'] + ', sigue las instrucciones indicadas en el');
        this.loading = false;
        this.stepper.next();
      },
      (error: HttpErrorResponse) => {
        if(error.status === 404){
          this.loading = false;
          this.ui.showSnackNotification('Al parecer esta dirección de correo no se encuetra registrada en el sistema', 'OK');
          return;
        }

        this.loading = false;
        this.ui.internalGenericError(error);
      }
    )
  }

  setCodeAndContinue(){
    this.resetPayload.token = this.giveCodeForm.value['token'];
    this.stepper.next();
  }

  updatePassword(){
    const form = this.resetPasswordForm.value;
    if(form['password'] !== form['password_confirmation']){
      this.ui.showSnackNotification('Las contraseñas no coinciden', 'OK');
      return
    }

    this.loading = true;
    this.resetPayload.password = form['password'];
    this.resetPayload.password_confirmation = form['password_confirmation'];
    this.userService.resetPassword(this.resetPayload).subscribe(
      (data) => {
        this.loading = false;
        this.ui.showSnackNotification('Se ha actualizado la información de la contraseña éxitosamente, ya puedes probarla al iniciar sesión', 'OK');
        this.stepper.next();
      },(error) => {
        this.loading = false;
        this.ui.internalGenericError(error);
      }
    )
  }
}
