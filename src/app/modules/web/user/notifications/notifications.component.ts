import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Channel} from 'angular2-actioncable';
import {Subscription} from 'rxjs';
import {ActionCableBigwaveService} from '../../../../core/services/action-cable-bigwave.service';
import {CableStatus} from '../../../../core/abstract/enums/cable-status.enum';
import {NotificationService} from '../../../../core/services/notification.service';
import {NotificationBigwave} from '../../../../core/abstract/interfaces/notification.interface';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {Router} from '@angular/router';
import {NotificationCategory} from '../../../../core/abstract/enums/category-notification.enum';
import {getIconography} from '../../../../core/abstract/functions/iconography.function';
import {filter, takeUntil} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {SocketMetadata} from '../../../../core/abstract/interfaces/chat/conversation-metadata-type';
import {Conversation} from '../../../../core/abstract/interfaces/messages.interface';
import {ChatService} from '../../../../core/services/chat.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationsComponent implements OnInit, OnDestroy {

  inFetch: boolean;//cuando se encuentra conectandose con el cable
  willAttemptReconnect: boolean;//Valor si está reintentando reconectar por fallas del internet

  instanceSuscription: Subscription;//Necesario para manejar la subscripción y evitar duplicados

  channel: Channel;//Canal de este componente

  notificationsList: NotificationBigwave[] = []; //Lista de notificaciones

  notificationsCachedToDismiss: NotificationBigwave[] = [];

  iconography: Map<NotificationCategory, string> = getIconography(); //Obtener la iconografía correspondiente a cada categoría

  constructor(
    private cableService: ActionCableBigwaveService,
    private notificationService: NotificationService,
    private ui: UIComponentsService,
    private router: Router,
		private chatService: ChatService
  ) { }

  ngOnInit() {
    this.initialize();
  }

  ngOnDestroy() {
    this.instanceSuscription.unsubscribe();
  }

  /**
   * Inicializar la subscripciones importantes principales
   */
  initialize() {
    /*
    * Es importante suscribirnos a los valores de onInstance ya que nos ayuda a controlar
    * el estado del cable emitiendose desde el login component
    *
    */
    this.instanceSuscription = this.cableService.onInstance.subscribe(
      (status) => {
        this.inFetch = true;
        if (status === CableStatus.INSTANCED) {
          this.channel = this.cableService.getActualCable().channel('NotificationChannel');
          this.onRecieveSubscription();
          this.onConnectedSubscription();
        }
      }
    );
  }

  /**
   * Para solicitar todas las notificaciones unread
   */
  performUnreads() {
    this.channel.perform('unreads');
  }


  async dissmissAll() {
    let ids: string = '';
    const notificationsLength = this.notificationsList.length - 1;
    let count = 0;

    this.notificationsList.forEach(
      (val, index) => {
        ids = ids.concat(notificationsLength === index ? `${val.id}` : `${val.id}-`);
        count++;
      }
    );

    let respReads: HttpResponse<NotificationBigwave[]>;

    try {
      respReads = await this.notificationService.markAllAs(ids, 'read').toPromise();
    } catch (error) {
      this.ui.internalGenericError(error);
      return;
    }

    if (respReads.ok) {
      this.notificationsCachedToDismiss = respReads.body;
      this.ui.showSnackNotification(`Se han marcado ${count} notificaciones como leídas`, 'OK');
      this.performUnreads();
    }
  }

  markAsRead(notification: NotificationBigwave) {

    this.triggerActionNotification(notification);

    this.notificationService.markAs(notification.id.toString(), 'read').subscribe(
      (resp) => {
        if (!resp.ok) 
          this.ui.weirdGenericError(resp);

        this.performUnreads();
      }, (error) => {
        this.ui.internalGenericError(error);
      }
    )
  }

  triggerActionNotification(notification: NotificationBigwave) {
    switch (notification.category) {
			case NotificationCategory.COMMENT:
				break;
			case NotificationCategory.CONVERSATION:
				break;
			case NotificationCategory.FOLLOW:
				break;
			case NotificationCategory.ADMIN:
				break;
			case NotificationCategory.OFFER:
				break;
			case NotificationCategory.POST:
				break;
			case NotificationCategory.PRODUCT:
				break;
			case NotificationCategory.PROFILE:
				break;
			case NotificationCategory.WISH:
				break; //TODO
      case NotificationCategory.COTIZATION:
      	this.handleCotization(notification.body as Conversation);
      	break;
      default:
      	throw new Error(`No se reconoce la category ${notification.category}`);
    }
  }

  handleCotization(data: Conversation){
  	this.chatService.addOpenedChatForced(data);
	}

  /**
   * Subscripción al momento de recibir datos desde el socket
   */
  onRecieveSubscription() {
    this.channel.received().pipe(
      takeUntil(this.cableService.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED))),//es fundamental tomar el takeUntil() mejor que unsuscribe
    ).subscribe(
      (data: any) =>  {
       	const notifications: NotificationBigwave[] | NotificationBigwave = data.data;
        
        if (Array.isArray(notifications)) {
          this.notificationsList = notifications.reverse();
          return;
        }

        this.notificationsList.unshift(notifications);
      }
    );
  }

  /**
   * Subscripción que escucha los eventos al conectarse con el cable y canal
   */
  onConnectedSubscription() {
    this.channel.connected().pipe(
      takeUntil(this.cableService.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
    ).subscribe(
      () => {
        this.willAttemptReconnect = false;
        this.inFetch = false;

        this.onDisconectSubscription();
        this.performUnreads();
      }
    );
  }

  /**
   * Subscripción que escucha las desconexiones, para tener un manejo mas apropiado
   */
  onDisconectSubscription() {
    this.channel.disconnected().pipe(
      takeUntil(this.cableService.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
    ).subscribe(
      (isDisconected) => {
        this.inFetch = true;
        if (isDisconected.willAttemptReconnect) {
          this.willAttemptReconnect = true;
        }
      }
    )
  }
}
