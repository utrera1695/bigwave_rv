import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UIComponentsService} from '../../../../../core/services/material-ui.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {WishService} from '../../../../../core/services/wish.service';
import {Wish} from '../../../../../core/abstract/interfaces/wish.interface';


@Component({
  selector: 'app-create-wish',
  templateUrl: './create-wish.component.html',
  styleUrls: ['./create-wish.component.scss']
})
export class CreateWishComponent implements OnInit {

	form: FormGroup;
	loading = false;

  constructor(
  	private fb: FormBuilder,
		private ui: UIComponentsService,
		@Inject(MAT_DIALOG_DATA) public data,
		private ws: WishService,
		private ref: MatDialogRef<CreateWishComponent>
  ) { }

  ngOnInit() {
  	this.form = this.fb.group({
			name: ['', Validators.required],
			description: ['', Validators.required],
			budget: [this.data['price'] || 0, [Validators.required, Validators.min(0)]],
			wisheable_id: [this.data['id']],
			wisheable_type: ['Product']
		});
  }

  create(){
  	const values = this.form.value as Wish;
  	this.loading = true;
  	this.ws.create(values).subscribe(
			() => {
				this.ui.showSnackNotification('Se ha creado el deseo', 'OK');
				this.loading = false;
				this.ref.close();
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loading = false;
			}
		)

	}
}
