import {Component, OnInit, ViewChild} from '@angular/core';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {MatButtonToggleChange, MatExpansionPanel, MatPaginator, PageEvent} from '@angular/material';
import {WishService} from '../../../../core/services/wish.service';
import {TreeNodeGeneric} from '../../../../core/abstract/interfaces/tree-node-generic.interface';
import {GenericClass} from '../../../../core/abstract/interfaces/response.interface';
import {CategoriesService} from '../../../../core/services/categories.service';
import {AdminService} from '../../../../core/services/admin.service';
import {Router} from '@angular/router';
import {ProfileType} from '../../../../core/abstract/enums/profile-type.enum';
import {BigwaveUtils} from '../../../../core/abstract/class/BigwaveUtils.class';
import {UserWish} from '../../../../core/abstract/interfaces/user-wish.interface';

@Component({
  selector: 'app-user-wishes',
  templateUrl: './user-wishes.component.html',
  styleUrls: ['./user-wishes.component.scss']
})

export class UserWishesComponent implements OnInit {


	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild('filtersPanel') filtersPanel: MatExpansionPanel;

	sidenavOpen = false;

	categoriesToTree: TreeNodeGeneric[] = [];

	categoriesSelected: TreeNodeGeneric[] = [];
	loading = false;

	wishes: GenericClass<UserWish>[] = [];

	pageSize = 4;
	pageIndex = 0;

	sort: 'ASC' | 'DESC';

	constructor(
		private categoriesService: CategoriesService,
		private ui: UIComponentsService,
		private wishesService: WishService,
		private as: AdminService,
		private router: Router,
	) { }

	ngOnInit() {
		this.setCategoriesToTree();
		this.getWishedProducts();
	}

	loadingCategories = false;
	setCategoriesToTree(){
		this.loadingCategories = true;
		this.categoriesService.allCategories().subscribe(
			(data) => {
				const categories = data.data;
				this.categoriesToTree = categories.map<TreeNodeGeneric>(
					(parent) => {
						return {
							id: Number(parent.id),
							name: parent.attributes.name
						};
					}
				);
				this.loadingCategories = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loadingCategories = false;
			}
		);
	}

	onSortChanges(event: MatButtonToggleChange){
		this.sort = event.value;
	}


	getWishedProducts(query?: string){
		this.loading = true;
		this.wishesService.userWishes(query).subscribe(
			(data) => {
				this.wishes = data.data;
				this.loading = false;
			}, (error) => {
				this.loading = false;
				this.ui.internalGenericError(error);
			}
		);
	}

	goDetail(type_profile: ProfileType, owner_id: any, product_id: any) {
		this.router.navigate(["/product", type_profile, owner_id, product_id]);
	}

	onCategorySelect(event: TreeNodeGeneric){
		if(this.loading){
			return;
		}

		const has = this.categoriesSelected.findIndex(c => c.id === event.id) !== -1;
		if(has) return;
		this.categoriesSelected.push(event);
		this.getWishedProducts(BigwaveUtils.idsToParam(this.categoriesSelected.map(c => c.id)));
		this.sidenavOpen = false;
	}

	onCategoryRemove(category: TreeNodeGeneric){
		if(this.loading){
			return;
		}

		const index = this.categoriesSelected.findIndex(c => c.id === category.id);
		this.categoriesSelected.splice(index, 1);
		this.getWishedProducts(BigwaveUtils.idsToParam(this.categoriesSelected.map(c => c.id)));
		this.sidenavOpen = false;
	}

	onPageEvent(page: PageEvent){
		this.pageIndex = page.pageIndex;
		this.pageSize = page.pageSize;
	}

	unwishing;

	async unwish(id){
		const ok = await this.ui.showDialogNotification(
			'¿Deseas Romper el Deseo?',
			`Esta acción no podrá ser revertida, el deseo pasará a estar eliminado`,
			'Sí'
		).afterClosed().toPromise();

		if(!ok){
			return;
		}

		this.unwishing = id;
		this.wishesService.destroyWish(id).subscribe(
			() => {
				this.unwishing = null;
				this.ui.showSnackNotification('Se ha eliminado el deseo', 'OK');
				this.getWishedProducts();
			}, (error) => {
				this.unwishing = null;
				this.ui.internalGenericError(error);
			}
		);
	}
}
