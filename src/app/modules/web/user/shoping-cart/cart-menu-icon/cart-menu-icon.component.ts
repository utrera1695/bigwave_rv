import { Component, OnInit } from '@angular/core';
import { CartService } from '../../../../../core/services/cart.service';
import { ActionCableBigwaveService } from '../../../../../core/services/action-cable-bigwave.service';
import { CableStatus } from '../../../../../core/abstract/enums/cable-status.enum';
import { Channel } from 'angular2-actioncable';
import { takeUntil, filter, debounceTime } from 'rxjs/operators';
import { CartItem } from '../../../../../core/abstract/interfaces/cart-item.interface';

@Component({
  selector: 'app-cart-menu-icon',
  templateUrl: './cart-menu-icon.component.html',
  styleUrls: ['./cart-menu-icon.component.scss']
})
export class CartMenuIconComponent implements OnInit {

  items: number = 0;
  isConnected = false;

  private channel: Channel;

  private cartItems: CartItem[] = [];

  constructor(
    private cartService: CartService,
    private cable: ActionCableBigwaveService
  ) { }

  ngOnInit() {
    this.initSuscription();
    this.initCableSuscription();
  }

  initSuscription(){
    /**
     * Este es el suscriptor del flujo para el datasource crudo del carrito de compras
     * el parámetro devuelto tiene la lista del carrito de compras
     * 
     * Se debe ejecutar 
     * @function syncDatasourceAndLocalAndRemote()
     * Para mantener actualizado el datasource
     */
    this.cartService.cartData.subscribe(
      (data) => {
        this.items = 0;
        data.forEach(
          (v) => {
            this.items += v.items.length || 0;
          }
        );
        this.cartItems = data;
      }
    );

    /**
     * Suscriptor para enviar la data al servidor cuando escuche de que ha sido actualizada la data
     * del carrito de compras para cuando se agrega/elimina/modifica un producto
     * 
     * Se debe hacer trigger en 
     * @function triggerUpdateSource()
     */
    this.cartService.onUpdate.subscribe(
      (hasUpdated) => {
        if(hasUpdated && this.isConnected){
          this.channel.send({action: "push_items", data: this.cartItems });
        }
      }
    );

    /**
     * El suscriptor para cuando se actualiza la cantidad de productos
     * posee un pipe de debounceTime por temas de performance
     * 
     * El trigger se hace automáticamente cuando se actualiza la cantidad de los productos
     */
    this.cartService.onQuantityUpdate.pipe(debounceTime(1000)).subscribe(
      (hasUpdated) => {
        if(hasUpdated && this.isConnected){
          this.channel.send({action: "push_items", data: this.cartItems });
        }
      }
    )
  }

  /**
   * Se inicializa la instancia con el cable, el suscriptor escucha a los eventos del cable
   * cuando está conectado inicia el suscriptor del canal
   */
  initCableSuscription(){
    this.cable.onInstance.subscribe(
      (estatus) => {
        if(estatus === CableStatus.INSTANCED){
          this.initChannelSuscription();
        }
      }
    )
  }

  /**
   * Inicializa la suscripción del canal
   * Posterior inicia la suscripción al recibir datos del canal
   * Y la suscripción que debe tener al conectarse correctamente con el cable
   */
  initChannelSuscription(){
    const cable = this.cable.getActualCable();
    this.channel = cable.channel('ShoppingCartChannel');
    this.onChannelRecieveSubscription();
    this.onChannelConnectedSuscription();
  }

  /**
   * Subscripción al momento de recibir datos desde el socket
   * Vive mientras que la instancia del cable exista
   * Si el tipo de datos es objeto se toma en cuenta y se actualiza el datasource
   */
  onChannelRecieveSubscription() {
    this.channel.received().pipe(
      takeUntil(this.cable.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
    ).subscribe(
      (data: any) =>  {
        if(typeof data.data === 'object')
          this.cartService.syncDatasourceAndLocalAndRemote(data.data);
      }
    );
  }

   /**
   * Subscripción que escucha los eventos al conectarse con el canal
   * Vive mientras que la instancia del cable exista
   * Al obtener la conexión crea otra suscripción que funciona para tener los eventos al desconectarse del canal
   * Cada vez que se conecta se trae los items del remote
   */
  onChannelConnectedSuscription() {
    this.channel.connected().pipe(
      takeUntil(this.cable.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
    ).subscribe(
      () => {
        this.isConnected = true;
        
        this.onHasToRetrieveDataSuscription();
        this.onChannelDisconectSubscription();
      }
    );
  }
  /**
   * Suscriptor para saber si la aplicación debe traer los productos del carrito remoto
   * Esto es importante para saber cuando se va a traer los productos
   * desde el cable
   * 
   * Si se logueó marca true, de lo contrario será siempre false
   * Ayuda para las recargas estando iniciado
   */
  onHasToRetrieveDataSuscription(){
    this.cartService.hasToRetrieveRemoteData.subscribe(
      (bool) => {
        if(bool){
          this.channel.perform('pull_items');
        }
      }
    )
  }

   /**
   * Subscripción que escucha las desconexiones, para tener un manejo mas apropiado
   */
  onChannelDisconectSubscription() {
    this.channel.disconnected().pipe(
      takeUntil(this.cable.onInstance.pipe(filter(v => v !== CableStatus.INSTANCED)))
    ).subscribe(
      (isDisconected) => {
        this.isConnected = false;
      }
    )
  }

}
