import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ProductService } from '../../../../../core/services/product.service';
import { Product, CustomField, Option } from '../../../../../core/abstract/interfaces/product.interface';
import { UIComponentsService } from '../../../../../core/services/material-ui.service';
import { CartService } from '../../../../../core/services/cart.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Item, ItemOptsValues } from '../../../../../core/abstract/interfaces/cart-item.interface';

@Component({
  selector: 'app-add-to-cart-dialog',
  templateUrl: './add-to-cart-dialog.component.html',
  styleUrls: ['./add-to-cart-dialog.component.scss']
})
export class AddToCartDialogComponent implements OnInit {

  product: Product;
  customFields: CustomField[] = [];
  options: Option[] = [];
  stockAviable = 0;

  form: FormGroup;

  inPromise = true;

  constructor(
    private productService: ProductService,
    private carritoService: CartService,
    private ui: UIComponentsService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddToCartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private cart_url_get: any
  ) {
    if (this.cart_url_get.cart != undefined) {
      this.selectOption = this.cart_url_get.cart.selectOption;
      this.quantity = this.cart_url_get.cart.quantity;
    }

  }

  ngOnInit() {
    this.getData();

  }

  getData() {

    this.productService.getByEndpoint(this.cart_url_get.url_get).subscribe(
      (resp) => {
        if (!resp.ok) {
          this.ui.weirdGenericError(resp);
          this.inPromise = false;
          return;
        }

        this.product = resp.body.data;
        this.customFields = this.product.attributes.custom_fields;
        this.stockAviable = this.product.attributes.stock || 0;

        console.warn('Producto consultado ->', this.product);

        if (!this.stockAviable) {
          this.inPromise = false;
          return;
        }

        const options = this.product.attributes.options;
        this.generateForm(options);
        this.inPromise = false;

      }, (error) => {
        this.onClose();
        this.ui.internalGenericError(error);
        this.inPromise = false;
      }
    );
  }

  selectOption: Array <any> = [];
  quantity = 1;

  /**
   * Para generar un formulario de manera dinámica
   * @param options son provenientes del servicio
   */
  generateForm(options: Option[]) {
    // Creamos un objeto donde vamos a tener los formControlName
    // Inicializando en quantity que siempre será estático
    const formFields = { quantity: [1, [Validators.required, Validators.min(1), Validators.max(this.stockAviable)]] };
    // Iteramos sobre las opciones del producto
    options.forEach(
      (val) => {
        // Asignamos al formFields un objeto con el attr del nombre de la opcion y que es requerido
        Object.assign(formFields, { [val.name]: ['', Validators.required] });
      }
    );
    // De esta manera se llama a form builder y listo seteamos el objeto
    this.form = this.fb.group(formFields);
    // Finalizamos con asignar al valor global las opciones para sincronizar con el template
    // A la hora de que form tenga una instancia
    this.options = options;
  }

  onClose() {
    this.dialogRef.close();
  }

  /**
   * Para agregar el producto al carrito de compras
   */
  onAdd() {
    const product = this.product;
    // Obtenemos las opciones del producto
    const productOptions = this.product.attributes.options;
    // Creamos un nuevo array de 'ItemOptsValues' que será llenado posteriormente
    const optionValues: ItemOptsValues[] = [];
    const formValues = this.form.value; // data del formulario

    productOptions.forEach(// Iteramos tantas opciones exista
      (el) => {
        const value = formValues[el.name]; // Llamamos a el valor dentro del form
        optionValues.push({
          id: el.id.toString(), // Seteamos un id de la opción
          name: el.name,
          value: value// Seteamos el valor
        });
      }
    );
    // El item a agregar al carrito de compras
    // Seteando uno a uno cada valor
    const item: Item = {
      product_id: this.product.id,
      product_name: this.product.attributes.name,
      description: this.product.attributes.description,
      url_get: this.product.attributes.url_get.url,
      price: this.product.attributes.price,
      custom_field_ids: this.customFields.map((el) => el.id),
      option_ids: this.options.map((el) => el.id),
      option_values: optionValues,
      quantity: formValues.quantity,
      stock: this.product.attributes.stock
    };
    // pasamos los datos del profile y luego el item del carrito de compras
    this.carritoService.addItemLocal({
      profile_id: product.attributes.productable.id,
      profile_name: product.attributes.productable.name || product.attributes.productable.title,
      profile_url: product.attributes.productable.url
    }, item);

    this.ui.showSnackNotification(`Se ha agregado al carrito de compras '${this.product.attributes.name}'`, 'OK');

    //Importante para verificar si está logueado y actualizar remotamente
    this.carritoService.triggerUpdateSource();

    this.onClose();
  }

}
