import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material';
import { AddToCartDialogComponent } from '../add-to-cart-dialog/add-to-cart-dialog.component';

@Component({
  selector: 'app-cart-button',
  templateUrl: './cart-button.component.html',
  styleUrls: ['./cart-button.component.scss']
})
export class CartButtonComponent implements OnInit {

  @Input('product') product: any;
  @Input('cart') cart: any;

  private _mobileQueryListener: () => void; 
  mobileQuery: MediaQueryList;

  disabled: boolean = false;

  constructor(
    changeDetectorRef: ChangeDetectorRef, 
    media: MediaMatcher,
    private dialog: MatDialog
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 800px)');//Query para un breakpoint
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();//Mobile Query devolverá cuando sea ejecutado el detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener);//Listener del rezise event
   }

  ngOnInit() {
    this.mobileQuery.removeListener(this._mobileQueryListener);

    const product: Object = this.product;
    if(!this.product || !product.hasOwnProperty('url_get') ){
      this.disabled = true;
      console.error('No existe un producto definido o el valor url no se encuentra');
    }
  }

  onClick(){

    this.dialog.open(AddToCartDialogComponent, {
      //data: this.product.url_get.url,
      data:{ url_get: this.product.url_get.url, cart: this.cart },
      width: '800px'
    });
  }

}
