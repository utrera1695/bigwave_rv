import { Component, OnInit, OnDestroy } from '@angular/core';
import { CartService } from '../../../../../core/services/cart.service';
import { UIComponentsService } from '../../../../../core/services/material-ui.service';
import { CartItem, Item } from '../../../../../core/abstract/interfaces/cart-item.interface';
import { takeWhile } from 'rxjs/operators';
import { CotizationRequestToCreate, ItemCotizationRequest } from '../../../../../core/abstract/interfaces/cotization/create-cotization.interface';
import { MatDialog } from '@angular/material';
import { AddCotizationDialogComponent } from './add-cotization-dialog/add-cotization-dialog.component';
import { AngularTokenService } from 'angular-token';
import { ModalsService } from '../../../../../core/services/modals.service';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss']
})
export class CartListComponent implements OnInit, OnDestroy {

  alive = true;
  inPromise = true;

  cartList: CartItem[] = [];

  totalQuantity = 0;
  totalGlobal = 0;

  constructor(
    private cartService: CartService,
    private ui: UIComponentsService,
    private dialog: MatDialog,
    private angularTokenService: AngularTokenService,
		private modalService: ModalsService
  ) { }

  ngOnInit() {
    this.getAllData();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getAllData() {
    this.cartService.cartData.pipe(takeWhile(() => this.alive))
      .subscribe(
        (data) => {
          this.cartList = data;
          this.setTotalCuantity();
          this.setTotalGlobal();
        }
      );
  }

  /**
   * Obtiene la cantidad de productos existentes dentro del carrito
   */
  setTotalCuantity() {
    this.totalQuantity = 0;
    this.cartList.forEach(
      (cartItem) => {
        this.totalQuantity += cartItem.items.length;
      }
    );
  }

  /**
   * Para obtener el total global de los productos a cotizar
   */
  setTotalGlobal() {
    this.totalGlobal = 0; // Tenemos un valor nuevo en 0
    this.cartList.forEach(
      (cartItem) => {
        cartItem.items.forEach(//iteramos por los productos de los profiles
          (item) => {
            // Total global será igual a total global + (la cantidad * el precio)
            this.totalGlobal += (item.quantity * item.price);
          }
        );
      }
    );
  }

  getTotalByProfile(profile_id) {
    let total = 0;
    this.cartList.forEach(
      (cartItem) => {
        if (cartItem.profile_id === profile_id) {
          cartItem.items.forEach(
            (item) => {
              total += (item.quantity * item.price);
            }
          );
          return;
        }
      }
    );

    return total;
  }

  //TODO
  goToProfile(url: string) {
    console.log(url);
  }

  /**
   * Para elíminar el item del carrito de compras
   * @param profile_id
   * @param item el item a comparar con el localStorage
   */
  deleteItem(profile_id: number, item: Item) {
    this.ui.showDialogNotification(
      'Eliminar producto del carrito',
      `Se eliminará <strong>${item.product_name}</strong> del carrito <br> ¿Desea continuar?`,
      'Aceptar'
      ).beforeClosed().subscribe(
        (b: boolean) => {
          if (b) {
            this.cartService.deleteItem(profile_id, item);
            this.ui.showSnackNotification('Se ha eliminado el producto', 'OK');
            this.cartService.triggerUpdateSource();
          }
        }
      );
  }

  /**
   * Para aumtentar o disminuir la cantidad de productos del carrito
   * @param profile_id
   * @param item el item a comparar con el localStorage
   * @param increase es un valor true para aumentar o falsey para que disminuya, no deja agregar negativos
   */
  increaseOrDecrease(profile_id: number, item: Item, increase?: boolean) {
    this.cartService.updateQuantity(profile_id, item, increase);
  }

  /**
   * Elimina los productos del carrito de compras relacionados con un profile
   * @param profile_id
   */
  deleteByProfile(profile_id: number) {
    this.ui.showDialogNotification(
      'Eliminar productos del perfíl',
      `Se eliminarán <strong>${this.cartList.filter(v => v.profile_id === profile_id)[0].items.length} productos</strong>  <br> ¿Desea continuar?`,
      'Aceptar'
      ).beforeClosed().subscribe(
        (b: boolean) => {
          if (b) {
            this.cartService.deleteByProfile(profile_id);
            this.ui.showSnackNotification('Se han eliminado los productos del perfil', 'OK');
            this.cartService.triggerUpdateSource();
          }
        }
      );

  }

  async quoteByProfile(profile_id: number) {

    if(!this.angularTokenService.userSignedIn()){
      this.ui.showSnackNotification('Debes iniciar sesión para hacer esta operación', 'OK');
      this.modalService.openLoginDialog();
      return;
    }

    const data = this.cartService.getDataByIdProfile(profile_id);

    const confirm = await this.dialog.open(AddCotizationDialogComponent, {
      data: [data],
      width: '500px'
    }).beforeClosed().toPromise();

    if (!confirm) {
      return;
    }

    const itemsCotization: ItemCotizationRequest[] = [];
		let price = 0;
    data.items.forEach(
      (i) => {
				price += (i.price * i.quantity);
        itemsCotization.push({
          product_id: Number(i.product_id),
          custom_field_ids: i.custom_field_ids,
          option_ids: i.option_ids,
          option_values: i.option_values.map(v => Object.assign({[v.id.toString()]: v.value})),
          quantity: i.quantity
        });
      }
    );

    const toSend: CotizationRequestToCreate[] = [{ profile_id: data.profile_id, price, items: itemsCotization }];

    this.cartService.quoteItems(toSend).subscribe(
      (resp) => {
        if (resp.ok) {
          this.ui.showSnackNotification('Se ha enviado la cotización', 'OK');
          this.cartService.deleteByProfile(data.profile_id);
          this.cartService.triggerUpdateSource();
        } else {
          this.ui.weirdGenericError(resp, 'Al cotizar un profile');
        }
      }, (error) => {
        this.ui.internalGenericError(error, 'Al cotizar un profile');
      }
    );
  }

  async quoteAll() {

    if(!this.angularTokenService.userSignedIn()){
      this.ui.showSnackNotification('Debes iniciar sesión para hacer esta operación', 'OK');
      this.modalService.openLoginDialog();
      return;
    }

    const data = this.cartService.getAllDataLocal();

    const confirm = await this.dialog.open(AddCotizationDialogComponent, {
      data: data,
      width: '500px'
    }).beforeClosed().toPromise();

    if (!confirm) {
      return;
    }

    const toSend: CotizationRequestToCreate[] = [];

    // Por cada profile se debe iterar para crear el objeto
    data.forEach(
      (cartItem) => {
        const itemsCotization: ItemCotizationRequest[] = [];
        let price = 0;

        cartItem.items.forEach(
          (i) => {
          	price += (i.price * i.quantity); // Acumulamos el precio por profile
            itemsCotization.push({
              product_id: Number(i.product_id),
              custom_field_ids: i.custom_field_ids,
              option_ids: i.option_ids,
              option_values: i.option_values.map(v => Object.assign({[v.id.toString()]: v.value})),
              quantity: i.quantity
            });
          }
        );

        toSend.push(
          {
            profile_id: cartItem.profile_id,
						price : price,
            items: itemsCotization
          }
        );
      }
    );

    this.cartService.quoteItems(toSend).subscribe(
      (resp) => {
        if (resp.ok) {
          console.log(resp);
          this.ui.showSnackNotification('Se han enviado todas las cotizaciones', 'OK');
          this.cartService.deleteAll();
        } else {
          this.ui.weirdGenericError(resp, 'Al cotizar varios profile');
        }
      }, (error) => {
        this.ui.internalGenericError(error, 'Al cotizar varios profile');
      }
    );

  }

  deleteAll() {
    this.ui.showDialogNotification(
      'Vaciar mi carrito',
      `Se eliminarán <strong>${this.totalQuantity} productos</strong> del carrito <br> ¿Desea continuar?`,
      'Aceptar'
      ).beforeClosed().subscribe(
        (b: boolean) => {
          if (b) {
            this.cartService.deleteAll();
            this.ui.showSnackNotification('Se han eliminado todos los productos del carrito', 'OK');
          }
        }
      );
  }
}
