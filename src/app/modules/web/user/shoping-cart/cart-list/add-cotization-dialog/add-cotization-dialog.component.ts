import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-cotization-dialog',
  templateUrl: './add-cotization-dialog.component.html',
  styleUrls: ['./add-cotization-dialog.component.scss']
})
export class AddCotizationDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<AddCotizationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onClose(onSave?: boolean){
    this.dialogRef.close(onSave);
  }

  getTotalByProfile(profile_id){
    let total = 0;
    this.data.forEach(
      (cartItem) => {
        if(cartItem.profile_id === profile_id){
          cartItem.items.forEach(
            (item) => {
              total += (item.quantity * item.price);
            }
          )
          return;
        }
      }
    );

    return total;
  }

  /**
   * Para obtener el total global de los productos a cotizar
   */
  getTotalGlobal(){
    let totalGlobal = 0;// Tenemos un valor nuevo en 0
    this.data.forEach(
      (cartItem) => {
        cartItem.items.forEach(//iteramos por los productos de los profiles
          (item) => {
            // Total global será igual a total global + (la cantidad * el precio)
            totalGlobal += (item.quantity * item.price);
          }
        )
      }
    );

    return totalGlobal;
  }

}
