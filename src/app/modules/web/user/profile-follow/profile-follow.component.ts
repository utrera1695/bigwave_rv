import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { API_ROUTES } from '../../../../app.constants';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { PageEvent } from '@angular/material';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ProfileType } from '../../../../core/abstract/enums/profile-type.enum';
import { getIconographyProfile } from '../../../../core/abstract/functions/iconography.function';
import { Router } from '@angular/router';
declare var $;

interface FoodNode {
  name: string;
  id: string;
  children?: any[];
}

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: string
}

const TREE_DATA3: FoodNode[] = [
  {
    name: 'Orden alfabetico',
    id: '3',
    children: [
      { name: 'A-Z' },
      { name: 'Z-A' }
    ]
  }
];

@Component({
  selector: 'app-profile-follow',
  templateUrl: './profile-follow.component.html',
  styleUrls: ['./profile-follow.component.scss']
})

export class ProfileFollowComponent implements OnInit {

  filtro1: string = ''
  filtro2: string = ''
  filtro3: string = ''
  OrderName: string = undefined;
  categoryName: string = undefined;

  Disable_profil = [
    false,
    false,
    false
  ]

  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  categories: any[];

  pymes: any = [];
  loadingPyme: boolean = true;

  independents: any = [];
  loadingIndependent: boolean = true;

  sellers: any = [];
  loadingSeller: boolean = true;

  page_size_pymes: number = 4;
  page_number_pymes: number = 1;
  pageSizeOptions_pymes: number[] = [];

  page_size_independents: number = 4;
  page_number_independents: number = 1;
  pageSizeOptions_independents: number[] = [];

  page_size_sellers: number = 4;
  page_number_sellers: number = 1;
  pageSizeOptions_sellers: number[] = [];

  private transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id
    };
  }

  profileList = [
    { name: 'Tiendas', id: 'pyme', state: true, id_may: 'Pyme' },
    { name: 'Independientes', id: 'independent', state: true, id_may: 'Independent' },
    { name: 'Personas', id: 'seller', state: true, id_may: 'Seller' }
  ];

  iconography: Map<ProfileType, string> = getIconographyProfile()

  TREE_DATA2?: FoodNode[] = [];
  treeControl2 = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);
  treeFlattener2 = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);
  dataSource2 = new MatTreeFlatDataSource(this.treeControl2, this.treeFlattener2);

  treeControl3 = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);
  treeFlattener3 = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);
  dataSource3 = new MatTreeFlatDataSource(this.treeControl3, this.treeFlattener3);

  id_category;

  constructor(
    private http: HttpClient,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.dataSource2.data = this.TREE_DATA2;
    this.dataSource3.data = TREE_DATA3;
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.getProfilesFollowed("pyme");
    this.getProfilesFollowed("independent");
    this.getProfilesFollowed("seller");
    this.getCategories();
  }

  handlePagepymes(e: PageEvent) {
    this.page_size_pymes = e.pageSize;
    this.page_number_pymes = e.pageIndex + 1;
  }

  handlePageindependents(e: PageEvent) {
    this.page_size_independents = e.pageSize;
    this.page_number_independents = e.pageIndex + 1;
  }

  handlePagesellers(e: PageEvent) {
    this.page_size_sellers = e.pageSize;
    this.page_number_sellers = e.pageIndex + 1;
  }

  getProfilesFollowed(type_profile?, category?) {

    let url = '';

    if (category) {
      url = API_ROUTES.getFollowing().replace(":type_profile", type_profile) + `?categories=${category}`;
    } else {
      url = API_ROUTES.getFollowing().replace(":type_profile", type_profile);
    }

    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {

      this.page_number_pymes = 1
      this.page_number_independents = 1
      this.page_number_sellers = 1

      if (type_profile == 'pyme') {
        this.loadingPyme = true
        this.renderCarousel('1')
        this.pymes = data['data'];
        this.loadingPyme = false;
        if (data.data.length < 4) {
          this.pageSizeOptions_pymes = [data.data.length]
          this.page_size_pymes = data.data.length
        } else {
          this.page_size_pymes = 4
          this.pageSizeOptions_pymes = [4]
        }
      }

      if (type_profile == 'independent') {
        this.loadingIndependent = true;
        this.renderCarousel('2')
        this.independents = data['data'];
        this.loadingIndependent = false;
        if (data.data.length < 4) {
          this.pageSizeOptions_independents = [data.data.length]
          this.page_size_independents = data.data.length
        } else {
          this.page_size_independents = 4
          this.pageSizeOptions_pymes = [4]
        }
      }

      if (type_profile == 'seller') {
        this.loadingSeller = true;
        this.renderCarousel('3')
        this.sellers = data['data'];
        this.loadingSeller = false;
        if (data.data.length <= 4) {
          this.pageSizeOptions_sellers = [data.data.length]
          this.page_size_sellers = data.data.length
        } else {
          this.page_size_sellers = 4
          this.pageSizeOptions_sellers = [4]
        }
      }

    }, error => {
    });
  }

  getCategories() {

    let url = API_ROUTES.getCategories();
    this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {

      if (data['data'].length) {
        this.categories = data['data'];
      }

      this.categories = this.categories.map(function (op) {
        return { "id": op.id, "name": op.attributes.name, "subcategories": op.attributes.subcategories }
      })

      this.TREE_DATA2.push({
        name: 'Filtrar por categoría',
        children: this.categories,
        id: '0'
      });

      this.dataSource2.data = this.TREE_DATA2;

    },

      error => {
        if ("_body" in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
          }
        }
      }
    );
  }

  renderCarousel(id) {
    $(document).ready(function () {
      $('.owl-carousel' + id).owlCarousel({
        loop: false,
        margin: 5,
        dots: false,
        responsive: {
          0: {
            items: 1,
          },
          600: {
            items: 1
          },
          1000: {
            items: 1
          }
        }
      });
    });
  }

  Categoria(node) {
    this.categoryName = node.name;
    this.id_category = node.id
    this.getProfilesFollowed("pyme", node.id);
    this.getProfilesFollowed("independent", node.id);
    this.getProfilesFollowed("seller", node.id);
  }

  remove_category() {
    this.categoryName = undefined;
    this.id_category = undefined;
    this.getProfilesFollowed("pyme");
    this.getProfilesFollowed("independent");
    this.getProfilesFollowed("seller");
  }

  Orden(node) {
    this.OrderName = node.name
  }

  remove_order() {
    this.OrderName = undefined;
  }

  profileType(i) {

    let minTypeProfiles = 0;
    let indice;

    for (let index = 0; index < this.profileList.length; index++) {
      if (this.profileList[index].state) {
        minTypeProfiles++;
        indice = index;
      }
    }

    if (minTypeProfiles >= 1) {
      this.renderCarousel('' + (i + 1))
    }

    if (minTypeProfiles <= 1) {
      this.Disable_profil[indice] = true;
    }

    if (minTypeProfiles >= 2) {
      for (let index = 0; index < this.Disable_profil.length; index++) {
        this.Disable_profil[index] = false;
      }
    }
  }

  profilCategory() {
    this.router.navigate(["products", "category", this.id_category, this.categoryName]);
  }

  openProfile(type_profile, slug) {
    this.router.navigate(['/profiles/', type_profile, slug]);
  }

  removeFollow() {
    setTimeout(() => {
      this.getProfilesFollowed("pyme", this.id_category);
      this.getProfilesFollowed("independent", this.id_category);
      this.getProfilesFollowed("seller", this.id_category);
    }, 8000);
  }

}
