import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../../../core/abstract/const/route-animations';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularTokenService} from 'angular-token';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {ModalsService} from '../../../../core/services/modals.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {API_ROUTES} from '../../../../app.constants';
import {ProfileTypes} from '../../../../core/abstract/enums/profile-types.enum';

@Component({
	selector: 'app-getting-started',
	templateUrl: './getting-started.component.html',
	styleUrls: ['./getting-started.component.scss'],
	animations: [routerTransition()]
})
export class GettingStartedComponent implements OnInit {
	loading: boolean = true;
	seller: boolean = false;
	pyme: boolean = false;
	independent: boolean = false;
	public errors: any = [];
	public mySellers: any = [];

	ProfileTypes = ProfileTypes;

	public profile: any = {
		"title": "",
		"name": "",
		"email": "",
		"banner": "",
		"photo": null,
		"launched": null,
		"phone": null,
		"url": null,
		"address": null,
		"vision": null,
		"mission": null,
		"description": '',
		"web": null,
		"profile": null,
		"experience": null,
		"category_ids": []
	};

	constructor(
		private activatedRoute: ActivatedRoute,
		private router: Router,
		private _tokenService: AngularTokenService,
		private ui: UIComponentsService,
		private http: HttpClient,
		private modalService: ModalsService
	) {	}

	ngOnInit() {
		this.loading = false;
		this.getMySellers();
	}

	getMySellers() {
		this.loading = true;
		let url = API_ROUTES.getMySellers();
		this.http.get(`${environment.api_url}${url}`).subscribe((data: any) => {
			this.loading = false;
			if (!!data) {
				this.mySellers = (data.data != null) ? [data.data] : [];
			}

		}, (error) => {
			this.loading = false;
			this.ui.internalGenericError(error);
		}
		);
	}

	openCommerceDialog(tab: ProfileTypes) {
		const ref = this.modalService.openCreateProfileDialog(tab);
		ref.afterClosed().subscribe(
			(reload) => {
				if(reload){
					this.getMySellers();
				}
			}
		)
	}
}
