import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../../core/services/user.service';
import {AngularTokenService} from 'angular-token';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {LocationService} from '../../../../core/services/location.service';
import {Country} from '../../../../core/abstract/interfaces/country-state.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../../core/abstract/interfaces/user.interface';
import {HttpErrorResponse, HttpEventType} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {AreYouSureConfirmPasswordComponent} from '../../../shared-components/are-you-sure-confirm-password/are-you-sure-confirm-password.component';
import {BigwaveUtils} from '../../../../core/abstract/class/BigwaveUtils.class';
import {PATTERNS} from '../../../../core/abstract/const/patterns';
import {debounceTime} from 'rxjs/operators';


@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {

  user: User;
  loading = false;
  loadingUpdatePassword: boolean;
  loadingUsernameValidating = false;
  loadingUpdatingUsername = false;
  loadingCountries = false;
  uploadingBarProgress = 0;
  hide: boolean;
  editingNickname = false;
  nickNameValid = true;

  countries: Country[] = [];

  clientDataForm: FormGroup;
  updatePasswordForm: FormGroup;
  updateUsernameForm: FormGroup;
  hideNP: boolean;
  hideNPR: boolean;

  currentDate = new Date();

  isTmpPass = () => Boolean(localStorage.getItem('tmp_pass'));
  constructor(
    private userService: UserService,
    private _tokenService: AngularTokenService,
    private ui: UIComponentsService,
    private locationService: LocationService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.user = this.userService.getCurrentUserDataInStorage();
    this.getCountries();
    this.initForms();
  }

  initForms() {
    this.clientDataForm = this.fb.group({
      id: [this.user.id],
      email: [this.user.email, [Validators.required, Validators.email]],
      name: [this.user.name, [Validators.required]],
      lastname: [this.user.lastname, [Validators.required]],
      avatar: [''],
      banner: [''],
      dni: [this.user.dni],
      gender: [this.user.gender],
      country: [this.user.country],
      birth_date: [this.user.birth_date]
    });

    this.updatePasswordForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordConfirmation: ['', [Validators.required, Validators.minLength(8)]],
      passwordCurrent: ['', [Validators.minLength(8)]]
    });

    this.updateUsernameForm = this.fb.group({
      nickname: [this.user.nickname, [Validators.required, Validators.minLength(2), Validators.pattern(PATTERNS.USERNAME)]]
    });

    this.onNicknameChanges();
  }

  onNicknameChanges(){
    this.updateUsernameForm.get('nickname').valueChanges.subscribe(
      (value) => {
        this.loadingUsernameValidating = true;
        this.userService.validateUsername(value).pipe(debounceTime(500)).subscribe(
          () => {
            this.nickNameValid = true;
            this.loadingUsernameValidating = false;
          }, (error: HttpErrorResponse) => {
            if(error.status === 409){
              this.nickNameValid = false;
              this.loadingUsernameValidating = false;
              return;
            }
            this.ui.internalGenericError(error);
          }
        )
      }
    )
  }

  onChangeImage(event, control: string){
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let target: any = event.target;
        let content: string = target.result;
        this.clientDataForm.get(control).patchValue(content);
      }
    }
  }

  getCountries(){
    this.loadingCountries = true;
    this.locationService.getAllCountries().subscribe(
      (data) => {
        this.countries = data.countries;
        this.loadingCountries = false;
      }, (error) => {
        this.loadingCountries = false;
        this.ui.internalGenericError(error);
      }

    );
  }


  updateProfileInfo(){
    this.loading = true;
    const values = this.clientDataForm.value;
    this.userService.updateCurrentUserData(values).subscribe(
      (data) => {
        if(data.type === HttpEventType.UploadProgress){
          this.uploadingBarProgress = BigwaveUtils.getProgressFromEvent(data);
        }else if(data.type === HttpEventType.Response){
          this.loading = false;
          this.userService.updateStorageData(data.body['data']);
          this.ui.showSnackNotification('Se ha actualizado la información', 'OK');
          this.uploadingBarProgress = 0;
        }
      }, (error) => {
        this.loading = false;
        this.ui.internalGenericError(error);
      }
    )
  }

  doUpdatePassword(){
    const values = this.updatePasswordForm.value;
    if(values['password'] !== values['passwordConfirmation']){
      this.ui.showSnackNotification('La nueva contraseña no coincide con la confirmación', 'OK');
      this.updatePasswordForm.get('passwordConfirmation').setErrors({invalid: true});
      return;
    }

    if(this.isTmpPass()){
      this.regeneratePassword(values);
    }else{
      this.updatePassword(values);
    }
  }

  updatePassword(values){
    if(values['passwordCurrent'] === values['password']){
      this.ui.showSnackNotification('La nueva contraseña debe ser distinta a la actual', 'OK');
      return;
    }

    this.loadingUpdatePassword = true;
    this._tokenService.updatePassword(values).subscribe(
      () => {
        this.updatePasswordForm.reset();
        this.ui.showSnackNotification('Se ha actualizado la contraseña con éxito', 'OK');
        this.loadingUpdatePassword = false;
      }, (error) => {
        this.ui.internalGenericError(error);
        this.loadingUpdatePassword = false;
      }
    )
  }

  regeneratePassword(values){
    this.loadingUpdatePassword = true;
    const payload = {password: values['password'], password_confirmation: values['passwordConfirmation']};
    this.userService.regeneratePassword(payload).subscribe(
      () => {
        localStorage.removeItem('tmp_pass');
        this.updatePasswordForm.reset();
        this.ui.showSnackNotification('Se ha actualizado la contraseña con éxito', 'OK');
        this.loadingUpdatePassword = false;
      }, (error) => {
        this.ui.internalGenericError(error);
        this.loadingUpdatePassword = false;
      }
    )
  }

  onCancelUpdateNickname(){
    this.editingNickname = false;
    this.updateUsernameForm.get('nickname').setValue(this.user.nickname);
  }

  updateNickname(){
    const values = this.updateUsernameForm.value;
    this.editingNickname = false;
    this.loadingUpdatingUsername = true;
    this.userService.updateNickname(values).subscribe(
      () => {
        this.loadingUpdatingUsername = false;
        this.ui.showSnackNotification('Se ha cambiado el nombre de usuario', 'OK');
        this.user.nickname = values['nickname'];
        this.userService.updateNickNameDataStorage(values['nickname']);

      }, (error: HttpErrorResponse) => {
        this.loadingUpdatingUsername = false;
        if(error.status === 409){
          this.ui.showSnackNotification('Ha ocurrido un error al actualizar el nombre de usuario, vuelve a intentarlo', 'OK');
          return;
        }
        this.ui.internalGenericError(error);
      }
    )
  }
}
