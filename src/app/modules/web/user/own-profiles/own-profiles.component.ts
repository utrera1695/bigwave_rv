import {Component, OnInit} from '@angular/core';
import {AngularTokenService} from 'angular-token';
import {MatDialog} from '@angular/material';
import {AreYouSureConfirmPasswordComponent} from '../../../shared-components/are-you-sure-confirm-password/are-you-sure-confirm-password.component';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {ProfileTypes} from '../../../../core/abstract/enums/profile-types.enum';
import {ProfileService} from '../../../../core/services/profile.service';
import {ModalsService} from '../../../../core/services/modals.service';
import {ProfileType} from '../../../../core/abstract/enums/profile-type.enum';
import {Admin} from '../../../../core/abstract/interfaces/admin';


@Component({
  selector: 'app-pymes',
  templateUrl: './own-profiles.component.html',
  styleUrls: ['./own-profiles.component.scss']
})
export class OwnProfilesComponent implements OnInit {
  public generalLoading: boolean = false;
  public ownProfiles: Admin[] = [];
  currentProfileType: ProfileTypes;
  ProfileTypes = ProfileTypes;
  defaultImg = 'https://cdn2.iconfinder.com/data/icons/online-shopping-flat-round/550/store-512.png';
  constructor(
    public dialog: MatDialog,
    private _tokenService: AngularTokenService,
    private ui: UIComponentsService,
    private http: HttpClient,
		private router: Router,
		private profileService: ProfileService,
		private modalsService: ModalsService
  ) {}

  ngOnInit() {
		this.setProfileType();
		this.getOwn();
	}

  setProfileType(){
  	const path = this.router.url;
  	if(path.includes(ProfileTypes.PYMES)){
  		this.currentProfileType = ProfileTypes.PYMES;
		}else if(path.includes(ProfileTypes.INDEPENDENTS)){
  		this.currentProfileType = ProfileTypes.INDEPENDENTS;
		}else{
  		this.currentProfileType = ProfileTypes.SELLERS
		}
	}

	add(){
  	const ref = this.modalsService.openCreateProfileDialog(this.currentProfileType);
  	ref.afterClosed().subscribe(reload => {
  		if(reload) {
  			this.getOwn()
			}
		})
	}

	loadingDelete;
  async delete(profile: Admin){
    const result = await this.dialog.open(AreYouSureConfirmPasswordComponent, { width: '300px' }).afterClosed().toPromise();

    if(!result){
      return;
    }

    this.loadingDelete = profile.id;
    this.profileService.delete(profile.type, profile.id, result).subscribe(
      (resp) => {
        this.loadingDelete = null;
        this.ui.showSnackNotification('Se ha elíminado exitosamente', 'OK');
        this.getOwn();
      }, (error: HttpErrorResponse) => {
        this.loadingDelete = null;
        if(error.status === 401){
          this.ui.showSnackNotification('La contraseña es incorrecta', 'Ok');
          return
        }
        this.ui.internalGenericError(error);
      }
    );
  }

  getOwn(){
    this.generalLoading = true;
    this.profileService.own(this.currentProfileType).subscribe(
      (data: any) => {
      	const profiles = data ? data['data'] : [];
        this.ownProfiles = Array.isArray(profiles) ? profiles: [profiles];
        this.generalLoading=false;
      },
      error =>  {
        this.generalLoading=false;
        this.ui.internalGenericError(error);
      }
    );
  }

  get showIfNotSellerAdded(): boolean{
  	return this.currentProfileType === ProfileTypes.SELLERS ? !this.ownProfiles.length && !this.generalLoading: true;
	}

  goTo(id){
  	this.router.navigate(['/admin', this.currentProfileType, id]);
	}

}
