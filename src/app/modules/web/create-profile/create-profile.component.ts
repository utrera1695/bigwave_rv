import {Component, ElementRef, Inject, NgZone, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {HttpErrorResponse} from '@angular/common/http';
import {MapsAPILoader} from '@agm/core';
import {AdminService} from '../../../core/services/admin.service';
import {ProfileService} from '../../../core/services/profile.service';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';
import {CategoriesService} from '../../../core/services/categories.service';
import {Categories} from '../../../core/abstract/interfaces/categories.interface';
import {GenericClass} from '../../../core/abstract/interfaces/response.interface';

@Component({
  selector: 'app-create-commerce',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {

  @ViewChild('search') public searchElement: ElementRef;

  public location = {
    lat: -33.4718999,
    lng: -70.9100259
  };

  categories: GenericClass<Categories>[] = [];
  form: FormGroup;

  loading = false;
  loadingCategories = false;
  autocomplete: google.maps.places.Autocomplete;

  constructor(
    public dialogRef: MatDialogRef<CreateProfileComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) private currentProfileTypes: ProfileTypes,
    private ui: UIComponentsService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private adminService: AdminService,
		private profileService: ProfileService,
    private categoriesService: CategoriesService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    if (this.adminService.currentProfileCreateName.getValue()) {
      this.form.get('title').setValue(this.adminService.currentProfileCreateName.getValue());
    }

    this.buildForm();
    this.getCategories();
    this.setCurrentPosition();
    this.setApiMapsLoader();

  }

  buildForm(){
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      category_ids: [[], Validators.required],
      address: ['', Validators.required],
    })
  }

  getCategories() {
    this.loadingCategories = true;
    this.categoriesService.allCategories().subscribe(
      (data) => {
        this.categories = data.data;
        this.loadingCategories = false;
      },
      error => {
        this.ui.internalGenericError(error);
      }
    );
  }


	setApiMapsLoader(){
		this.mapsAPILoader.load().then(() => {

			this.autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement);
      this.autocomplete.addListener('place_changed', this.onPlaceChangeListener.bind(this))
    });
	}

	onPlaceChangeListener(){
    this.ngZone.run(() => {
      let place = this.autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }

      this.location = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      };

      let address = `${place.name}, `;
      if (place.address_components) {
        address += [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }
      this.form.get('address').patchValue(address);
    });
  }

  private setCurrentPosition() {
    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition((position) => {
        this.location = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        this.doGeocodeAndSetAddress();
      });
    }
  }

  onNoClick(cerrado?:boolean): void {
		this.adminService.currentProfileCreateName.next(null);
    this.dialogRef.close(cerrado);
  }

  loadAndSetAddress(event) {
    this.location = {lat: event.coords.lat, lng: event.coords.lng};
    this.doGeocodeAndSetAddress();
  }

  doGeocodeAndSetAddress(){
    const geocoder = new google.maps.Geocoder;
    geocoder.geocode({location: this.location}, (results, status) => {
      if (status ===  google.maps.GeocoderStatus.OK) {
        this.form.get('address').setValue(results[0] ? results[0].formatted_address: '');
      }
    })
  }

  create(){
  	this.loading = true;
  	const payload = this.form.value;
  	Object.assign(payload, {locations: {latitude: this.location.lat, longitude: this.location.lng}});
  	this.profileService.create(this.currentProfileTypes, { profile: payload }).subscribe(
			(resp) => {
				this.loading = false;
        this.router.navigate([`/admin`,this.currentProfileTypes, resp['data']['id']]);
				this.ui.showSnackNotification("Creado exitosamente", "OK");
				this.onNoClick(true);
			},(error: HttpErrorResponse) => {
				this.loading = false;
				this.ui.internalGenericError(error);
			}
		)
	}
}
