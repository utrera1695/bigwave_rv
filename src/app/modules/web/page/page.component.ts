import { Component, OnInit, Inject } from '@angular/core';
import { AngularTokenService } from 'angular-token';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  store: boolean = false;
  seller: boolean = false;
  independent: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public _tokenService: AngularTokenService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA)
    private data: any) { }

  ngOnInit() {
    this.routeInit(this.route.snapshot.params['type_profile']);
  }

  routeInit(type: string) {
    if (type == "store") {
      this.store = true;
    } else if (type == "seller") {
      this.seller = true;
    } else
      this.independent = true;
  }


}
