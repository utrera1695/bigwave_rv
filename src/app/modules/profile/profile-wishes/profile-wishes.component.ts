import {Component, OnInit, ViewChild} from '@angular/core';
import {MatExpansionPanel, MatPaginator, PageEvent} from '@angular/material';
import {CategoriesService} from '../../../core/services/categories.service';
import {TreeNodeGeneric} from '../../../core/abstract/interfaces/tree-node-generic.interface';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {WishService} from '../../../core/services/wish.service';
import {AdminService} from '../../../core/services/admin.service';
import {ProfileWish, QueryProfileWish} from '../../../core/abstract/interfaces/profile-wishes.interface';
import {Router} from '@angular/router';
import {ProfileType} from '../../../core/abstract/enums/profile-type.enum';
import {Meta} from '../../../core/abstract/interfaces/response.interface';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BigwaveUtils} from '../../../core/abstract/class/BigwaveUtils.class';

@Component({
  selector: 'app-profile-wishes',
  templateUrl: './profile-wishes.component.html',
  styleUrls: ['./profile-wishes.component.scss']
})
export class ProfileWishesComponent implements OnInit {

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild('filtersPanel') filtersPanel: MatExpansionPanel;

	sidenavOpen = false;

	categoriesToTree: TreeNodeGeneric[] = [];

	categoriesSelected: TreeNodeGeneric[] = [];
	subCategoriesSelected: TreeNodeGeneric[] = [];

	loading = false;

	wishes: ProfileWish[];
	controls: Meta;

	pageSize: number = 4;

	formFilter: FormGroup;

	currentDate = new Date();

	lastFilterData: QueryProfileWish;

	constructor(
  	private categoriesService: CategoriesService,
		private ui: UIComponentsService,
		private wishesService: WishService,
		private as: AdminService,
		private router: Router,
		private fb: FormBuilder
	) { }

  ngOnInit() {
		this.formFilter = this.fb.group({
			q: [''],
			start_date: [''],
			end_date: ['']
		});

  	this.setCategoriesToTree();
  	this.getWishedProducts();
  }

  loadingCategories = false;
  setCategoriesToTree(){
  	this.loadingCategories = true;
		this.categoriesService.allCategories().subscribe(
			(data) => {
				const categories = data.data;
				this.categoriesToTree = categories.map<TreeNodeGeneric>(
					(parent) => {
						const children = parent.attributes.subcategories.map<TreeNodeGeneric>(
							(child) => {
								return {
									id: child.id,
									name: child.name,
									parent_id: child.category_id
								};
							}
						);

						return {
							id: Number(parent.id),
							name: parent.attributes.name,
							children
						};
					}
				);
				this.loadingCategories = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loadingCategories = false;
			}
		);
	}


	getWishedProducts(query?: QueryProfileWish){
		this.loading = true;
		const q: QueryProfileWish = {per_page: String(this.pageSize), page: '1'};
  	this.wishesService.profileWishedProducts(this.as.getCurrentAdminType(), this.as.getCurrentAdminId(), query || q).subscribe(
			(data) => {
				this.wishes = data.data;
				this.controls = data.meta;
				this.loading = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loading = false;
			}
		);
	}

	goDetail(type_profile: ProfileType, owner_id: any, product_id: any) {
		this.router.navigate(["/product", type_profile, owner_id, product_id]);
	}

	onCategorySelect(event: TreeNodeGeneric){
  	if(!event.parent_id){
			const has = this.categoriesSelected.findIndex(c => c.id === event.id) !== -1;
			if(has) return;
			this.categoriesSelected.push(event);
		}else{
			const has = this.subCategoriesSelected.findIndex(c => c.id === event.id) !== -1;
			if(has) return;
			this.subCategoriesSelected.push(event);
		}
	}

	onCategoryRemove(category: TreeNodeGeneric){
  	if(!category.parent_id){
			const index = this.categoriesSelected.findIndex(c => c.id === category.id);
			this.categoriesSelected.splice(index, 1);
		}else{
			const index = this.subCategoriesSelected.findIndex(c => c.id === category.id);
			this.subCategoriesSelected.splice(index, 1);
		}
	}

	isFormEmpty(){
  	return BigwaveUtils.isFormEmpty(this.formFilter.value);
	}

	onPageEvent($event: PageEvent) {
  	let page = $event.pageIndex + 1;
		if(this.pageSize !== $event.pageSize){
  		page = 1;
  		this.paginator.pageIndex = 0;
		}
		this.pageSize = $event.pageSize;

		let filters: QueryProfileWish;
		if(this.lastFilterData){
			filters = this.lastFilterData;
			filters.page = String(page);
			filters.per_page = String($event.pageSize);
		}else{
			filters = {page: String(page), per_page: String($event.pageSize)}
		}
		this.getWishedProducts(filters);

	}

	get filterFormData(){
		const filters = this.formFilter.value as QueryProfileWish;

		filters.q = BigwaveUtils.formatSearch(filters.q);
		filters.start_date = BigwaveUtils.parseDateToParam(filters.start_date);
		filters.end_date = BigwaveUtils.parseDateToParam(filters.end_date);
		filters.page = '1';
		filters.per_page = String(this.pageSize);

		return filters;
	}

	applyFilters(){
  	const filters = this.filterFormData;
		if(this.lastFilterData){
			filters.categories = this.lastFilterData.categories;
			filters.subcategories = this.lastFilterData.subcategories;
		}

  	this.lastFilterData = filters;
  	this.filtersPanel.close();
  	this.paginator.pageIndex = 0;
		this.getWishedProducts(filters);
	}

	viewAll(){
		this.getWishedProducts();
		this.formFilter.reset();
		this.filtersPanel.close();
		this.removeAllCategories();
		this.lastFilterData = null;
	}

	removeAllCategories(){
  	this.categoriesSelected = [];
  	this.subCategoriesSelected = [];
	}

	clearCategories(){
		this.removeAllCategories();

		let filter: QueryProfileWish;
		if(this.lastFilterData){
			filter = this.lastFilterData;
			filter.page = '1';
		}else{
			filter = {page: '1', per_page: String(this.pageSize)};
		}

		if(!filter.categories && !filter.subcategories){
			return;
		}

		filter.categories = '';
		filter.subcategories = '';
		this.lastFilterData = filter;
		this.sidenavOpen = false;
		this.getWishedProducts(filter);
	}

	applyCategories(){
  	let filter: QueryProfileWish;
  	if(this.lastFilterData){
  		filter = this.lastFilterData;
			filter.page = '1';
		}else{
  		filter = {page: '1', per_page: String(this.pageSize)};
		}
		filter.categories = BigwaveUtils.idsToParam(this.categoriesSelected.map(c => c.id));
		filter.subcategories = BigwaveUtils.idsToParam(this.subCategoriesSelected.map(c => c.id));
		this.lastFilterData = filter;
		this.sidenavOpen = false;
		this.getWishedProducts(filter);
	}
}
