import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UIComponentsService} from '../../../../../core/services/material-ui.service';
import {WishService} from '../../../../../core/services/wish.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AdminService} from '../../../../../core/services/admin.service';
import {WishDetail} from '../../../../../core/abstract/interfaces/wish.interface';

@Component({
  selector: 'app-answer-wish',
  templateUrl: './answer-wish.component.html',
  styleUrls: ['./answer-wish.component.scss']
})
export class AnswerWishComponent implements OnInit {

	form: FormGroup;

	loading = false;
	currentDate = new Date();

  constructor(
  	private ui: UIComponentsService,
		private fb: FormBuilder,
		private ws: WishService,
		private ref: MatDialogRef<AnswerWishComponent>,
		private as: AdminService,
		@Inject(MAT_DIALOG_DATA) public wish: WishDetail
	) { }

  ngOnInit() {
  	this.form = this.fb.group({
			type_profile: [this.as.getCurrentAdminType()],
			profile_id: [Number(this.as.getCurrentAdminId())],
			message: [`Felicidades, hemos concedido tu deseo "${this.wish.user.name}"`, Validators.required],
			special_price: [this.wish.budget || 0, [Validators.required ,Validators.min(0), Validators.pattern(/^\d+$/)]],
			limit_date: [new Date()],
			claim_code: [''],
			special_conditions: ['']
		});
  	//this.answer();
  }

  answer(){
  	this.loading = true;
  	const values = this.form.value;
  	this.ws.answerWish(values, this.wish.sended_wish.id).subscribe(
			(data) => {
				this.ui.showSnackNotification('El deseo fue concedido, el usuario recibirá en breves segundos un mensaje', 'OK');
				this.onClose(true);
				this.loading = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.onClose();
				this.loading = false;
			}
		)
	}


  onClose(b?: boolean){
  	this.ref.close(b);
	}

}
