import {Component, OnInit, ViewChild} from '@angular/core';
import {WishService} from '../../../../core/services/wish.service';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatExpansionPanel, MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {QueryWishDetail, WishDetail} from '../../../../core/abstract/interfaces/wish.interface';
import {FormBuilder, FormGroup} from '@angular/forms';
import {BigwaveUtils} from '../../../../core/abstract/class/BigwaveUtils.class';
import {Meta} from '../../../../core/abstract/interfaces/response.interface';
import {AdminService} from '../../../../core/services/admin.service';
import {AnswerWishComponent} from './answer-wish/answer-wish.component';
import {ViewWishDetailAdminComponent} from './view-wish-detail-admin/view-wish-detail-admin.component';

@Component({
  selector: 'app-wishes-items',
  templateUrl: './wishes-list.component.html',
  styleUrls: ['./wishes-list.component.scss']
})
export class WishesListComponent implements OnInit {

	@ViewChild(MatSort) set sort(sort: MatSort){
		this.dataSource.sort = sort;
	}

	paginator: MatPaginator;
	@ViewChild(MatPaginator) set pag(paginator: MatPaginator){
		//this.dataSource.paginator = paginator;
		this.paginator = paginator;
	}

	@ViewChild('filtersPanel') filtersPanel: MatExpansionPanel;

	dataSource: MatTableDataSource<WishDetail> = new MatTableDataSource();
	displayedColumns = ['name', 'created_at', 'response', 'user', 'budget', 'opts'];

	pageSize = 10;

	currentId;
	productName;

	formFilter: FormGroup;

	currentDate = new Date();
	lastFilterData: QueryWishDetail;
	loading: boolean;

	controls: Meta;
  constructor(
		private wishesService: WishService,
		private ui: UIComponentsService,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private as: AdminService,
		private router: Router,
		private dialog: MatDialog
	) { }

  ngOnInit() {
  	this.formFilter = this.fb.group({
			response: [''],
			start_date: [''],
			end_date: ['']
		});

  	this.route.params.subscribe(
			(params) => {
				this.currentId = params['id'];
				this.getWishes();
			}
		);

  	this.route.queryParams.subscribe(
			(params) => {
				this.productName = params['product_name'] || 'N/A';
			}
		)
  }

  getWishes(query?: QueryWishDetail){
  	this.loading = true;
		const q: QueryWishDetail = {per_page: String(this.pageSize), page: '1'};
  	this.wishesService.wishesByProductProfile(this.currentId, query || q).subscribe(
			(data) => {
				this.dataSource.data = data.data;
				this.controls = data.meta;
				this.loading = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loading = false;
			}
		);
	}

	onPageEvent($event: PageEvent) {
		let page = $event.pageIndex + 1;
		if(this.pageSize !== $event.pageSize){
			page = 1;
			this.paginator.pageIndex = 0;
		}
		this.pageSize = $event.pageSize;

		let filters: QueryWishDetail;
		if(this.lastFilterData){
			filters = this.lastFilterData;
			filters.page = String(page);
			filters.per_page = String($event.pageSize);
		}else{
			filters = {page: String(page), per_page: String($event.pageSize)}
		}
		this.getWishes(filters);
	}

	viewAll() {
		this.getWishes();
		this.formFilter.reset();
		this.filtersPanel.close();
		this.lastFilterData = null;
	}

	applyFilters() {
		const filters = this.filterFormData;

		this.lastFilterData = filters;
		this.filtersPanel.close();
		this.getWishes(filters);
	}

	get filterFormData(){
		const filters = this.formFilter.value as QueryWishDetail;

		filters.response = String(filters.response);
		filters.start_date = BigwaveUtils.parseDateToParam(filters.start_date);
		filters.end_date = BigwaveUtils.parseDateToParam(filters.end_date);
		filters.page = '1';
		filters.per_page = String(this.pageSize);

		return filters;
	}

	isFormEmpty() {
		return BigwaveUtils.isFormEmpty(this.formFilter.value);
	}

	goBack(){
  	this.router.navigate([this.as.getRoute('/deseos')]);
	}

	async answerWish(wish: WishDetail){
  	const answered = await this.dialog.open(AnswerWishComponent, {width:'400px', data: wish}).afterClosed().toPromise();
  	if(answered){
  		this.getWishes();
		}
	}

	unwishing;

	async discard(wish: WishDetail){
  	const discard = await this.ui.showDialogNotification(
  		'¿Deseas Romper el Deseo?',
			`Esta acción no podrá ser revertida, el deseo pasará a estar eliminado`,
			'Sí'
		).afterClosed().toPromise();

  	if(!discard){
  		return;
		}
  	this.unwishing = wish.id;
  	this.wishesService.destroyWish(wish.id).subscribe(
			() => {
				this.unwishing = null;
				this.ui.showSnackNotification('Se ha eliminado el deseo', 'OK');
				if(this.lastFilterData){
					this.getWishes(this.lastFilterData);
				}else{
					this.getWishes();
				}
			},
			(error) => {
				this.ui.internalGenericError(error);
				this.unwishing = null;
			}
		)
	}

	viewWish(wish: WishDetail){
  	this.dialog.open(ViewWishDetailAdminComponent, {width: '500px', data: wish});
	}
}
