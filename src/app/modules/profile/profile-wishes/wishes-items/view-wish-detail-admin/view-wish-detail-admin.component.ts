import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {WishDetail} from '../../../../../core/abstract/interfaces/wish.interface';

@Component({
  selector: 'app-view-wish-detail-admin',
  templateUrl: './view-wish-detail-admin.component.html',
  styles: []
})
export class ViewWishDetailAdminComponent implements OnInit {

  constructor(
  	@Inject(MAT_DIALOG_DATA) public wish: WishDetail
	) { }

  ngOnInit() {
  }

}
