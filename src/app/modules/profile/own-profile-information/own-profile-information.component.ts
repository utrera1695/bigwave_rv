import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../core/services/admin.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Admin} from '../../../core/abstract/interfaces/admin';
import {ProfileTypes} from '../../../core/abstract/enums/profile-types.enum';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {HttpEventType} from '@angular/common/http';
import {BigwaveUtils} from '../../../core/abstract/class/BigwaveUtils.class';
import {GenericClass} from '../../../core/abstract/interfaces/response.interface';
import {Categories} from '../../../core/abstract/interfaces/categories.interface';
import {CategoriesService} from '../../../core/services/categories.service';

@Component({
  selector: 'app-information-admin',
  templateUrl: './own-profile-information.component.html',
  styleUrls: ['./own-profile-information.component.scss']
})
export class OwnProfileInformationComponent implements OnInit {
	loading = false;
  currentAdmin: Admin;
  form: FormGroup;
  ProfileTypes = ProfileTypes;
  uploadingBarProgress: number = 0;
  categories: GenericClass<Categories>[] = [];
  loadingCategories: boolean;
  constructor(
    private as: AdminService,
    private fb: FormBuilder,
		private ui: UIComponentsService,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit() {
    this.getCategories();
    this.setFormData();
  }

  setFormData(){
		// Obtenemos la información del profile que está siendo editado y la seteamos en el formulario
		this.currentAdmin = this.as.getAdminData();

		this.form = this.fb.group({
			title: [this.currentAdmin.attributes.title, Validators.required],
			email: [this.currentAdmin.attributes.email, [Validators.required, Validators.email]],
			description: [this.currentAdmin.attributes.description],
			mission: [this.currentAdmin.attributes.mission],
			vision: [this.currentAdmin.attributes.vision],
      banner: [''],
      photo: [''],
      category_ids: [this.currentAdmin.attributes.categories.map(c => String(c.id)), Validators.required],
			slug: [this.currentAdmin.attributes.slug, [Validators.required, Validators.pattern(/^[a-z0-9]+(?:-[a-z0-9]+)*$/)]]
		});
		Object.keys(this.form.controls).forEach((key) => this.form.controls[key].markAsTouched());
	}

  getCategories() {
    this.loadingCategories = true;
    this.categoriesService.allCategories().subscribe(
      (data) => {
        this.categories = data.data;
        this.loadingCategories = false;
      },
      error => {
        this.ui.internalGenericError(error);
      }
    );
  }


  handlePhotoUpload(event, control: string){
		const image = event.target.files[0] as File;
		const fr = new FileReader();

		fr.readAsDataURL(image);

		fr.onload = ((ev: any) =>{
			this.form.get(control).patchValue(ev.target.result);
		});
	}

	generateAutoSlug(){
  	let name: string = this.form.controls['title'].value;

		name = name.toString().toLowerCase()
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			.replace(/--+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');            // Trim - from end of text

		this.form.get('slug').setValue(name);
	}

	update(){
  	this.loading = true;
  	this.as.updateAdmin(this.currentAdmin.type, this.currentAdmin.id, this.form.value).subscribe(
			(resp) => {
			  console.log(resp);
				if(resp.type === HttpEventType.UploadProgress){
          this.uploadingBarProgress = BigwaveUtils.getProgressFromEvent(resp);
          console.log(this.uploadingBarProgress);
        }else if(resp.type === HttpEventType.Response){
          this.ui.showSnackNotification('Información actualizada exitosamente', 'OK');
          this.loading = false;
        }
			},(error) => {
				this.loading = false;
				this.ui.internalGenericError(error,'Error: Actualizar profile');
			}
		)
	}

}
