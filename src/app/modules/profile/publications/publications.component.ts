import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../../core/services/admin.service';
import {UIComponentsService} from '../../../core/services/material-ui.service';
import {MatDialog, PageEvent} from '@angular/material';
import {AddPublicationDialogComponent} from './add-publication-dialog/add-publication-dialog.component';
import {PublicationsService} from '../../../core/services/publications.service';
import {Post} from '../../../core/abstract/interfaces/post';
import {GenericClass} from '../../../core/abstract/interfaces/response.interface';
import {ViewPublicationDialogComponent} from './view-publication-dialog/view-publication-dialog.component';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.scss']
})
export class PublicationsComponent implements OnInit {

	posts: GenericClass<Post>[];
	loading;

  constructor(
  	private adminService: AdminService,
		private ui: UIComponentsService,
		private dialog: MatDialog,
		private publicationsService: PublicationsService
	) { }

  ngOnInit() {
  	this.getAll();
  	//sthis.publicationsService.getOne(2).subscribe(console.log);

  }

  getPostContentMin(content: string){
  	if(content.length > 140){
			const sliced = content.slice(0, 140);
			return sliced.concat('...');
		}

  	return content;
	}

  getAll(){
  	this.loading = true;
  	const profileId = this.adminService.getCurrentAdminId();
  	this.publicationsService.getHistoryByProfile(profileId).subscribe(
			(data) => {
				this.posts = data.data;
				this.loading = false;
			}, (error) => {
				this.ui.internalGenericError(error);
				this.loading = false;
			}
		)
	}

  add(){
  	const ref = this.dialog.open(AddPublicationDialogComponent, {
  		width: '400px'
		});

  	ref.afterClosed().subscribe(
			(resetList) => {
				if(resetList){
					this.getAll();
				}
			}
		);
	}

	update(post){
  	const ref = this.dialog.open(AddPublicationDialogComponent, {
			width: '400px',
			data: post
		});

  	ref.afterClosed().subscribe(
			(resetList) => {
				if(resetList){
					this.getAll();
				}
			}
		)
	}

	async delete(id){
  	const confirm = await this.ui.showDialogNotification(
  		'¿Desea elíminar la publicación?',
			`Toda la información ya no estará disponible y se perderá, <b>¿Esta seguro?</b>`,
			'Sí'
		).afterClosed().toPromise();

  	if(confirm){
  		this.publicationsService.delete(id).subscribe(
				(data) => {
					this.ui.showSnackNotification('Se ha elíminado la información', 'OK');
					this.getAll();
				}, (error) => {
					this.ui.internalGenericError(error, 'Elíminar posts');
				}
			);
		}
	}

	view(post: GenericClass<Post>) {
		this.dialog.open(ViewPublicationDialogComponent, {
			width: '500px',
			data: post
		});
	}

	pageIndex = 0;
  pageSize = 3;
	onPageEvent(page: PageEvent){
  	this.pageIndex = page.pageIndex;
  	this.pageSize = page.pageSize;
	}
}
