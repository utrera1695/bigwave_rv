import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {GenericClass} from '../../../../core/abstract/interfaces/response.interface';
import {Post} from '../../../../core/abstract/interfaces/post';

@Component({
  selector: 'app-view-publication-dialog',
  templateUrl: './view-publication-dialog.component.html',
  styleUrls: ['./view-publication-dialog.component.scss']
})
export class ViewPublicationDialogComponent implements OnInit {

  constructor(
  	@Inject(MAT_DIALOG_DATA) public data: GenericClass<Post>
	) { }

  ngOnInit() {
  }

}
