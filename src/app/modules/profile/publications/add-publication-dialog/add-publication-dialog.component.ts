import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PublicationsService} from '../../../../core/services/publications.service';
import {AdminService} from '../../../../core/services/admin.service';
import {GenericClass} from '../../../../core/abstract/interfaces/response.interface';
import {Post} from '../../../../core/abstract/interfaces/post';

@Component({
  selector: 'app-add-publication-dialog',
  templateUrl: './add-publication-dialog.component.html',
  styleUrls: ['./add-publication-dialog.component.scss']
})
export class AddPublicationDialogComponent implements OnInit {

	form: FormGroup;
	loading = false;
	image: string;
	banner: string;

  constructor(
  	private fb: FormBuilder,
		private ui: UIComponentsService,
		private ref: MatDialogRef<AddPublicationDialogComponent>,
		private publicationsService: PublicationsService,
		private adminService: AdminService,
		@Inject(MAT_DIALOG_DATA) public data: GenericClass<Post>
	) { }

  ngOnInit() {
  	this.form = this.fb.group({
			title: ['', Validators.required],
			content: ['', Validators.required]
		});

  	if(this.data){
  		this.setData();
		}
  }

  setData(){
  	this.form.get('title').patchValue(this.data.attributes.title);
  	this.form.get('content').patchValue(this.data.attributes.content);
  	this.banner = this.data['banner'];
	}

  save(){
  	this.loading = true;
  	const values = this.form.value;
  	if(!this.data){
  		Object.assign(values, {postable_type:'Profile', postable_id: this.adminService.getCurrentAdminId(), banner: this.banner});
			this.publicationsService.add({post: values}).subscribe(this.handleResponse.bind(this), this.handleError.bind(this));
		}else{
  		Object.assign(values, {banner: this.banner});
			this.publicationsService.update(this.data['id'], {post: values}).subscribe(this.handleResponse.bind(this), this.handleError.bind(this));
		}
	}

	handleResponse = (data) => {
		this.ui.showSnackNotification('Se ha guardado la información correctamente');
		this.loading = false;
		this.close(true);
	};


	handleError = (error) => {
		this.ui.internalGenericError(error);
		this.loading = false;
		this.close();
	};

	close(resetList?: boolean){
  	this.ref.close(resetList);
	}

	onSelectFile($event) {
		if ($event.target.files && $event.target.files[0]) {
			let reader = new FileReader();

			reader.readAsDataURL($event.target.files[0]); // read file as data url

			reader.onload = (event) => { // called once readAsDataURL is completed
				let target: any = event.target;
				this.banner = target.result;
			};
		}


	}
}
