import { Component, OnInit, Input, NgZone, ViewChild, ElementRef } from '@angular/core';
import { API_ROUTES } from '../../../app.constants';
import { ActivatedRoute } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { FormControl } from '@angular/forms';
import { MatSidenav } from '@angular/material';
import { UIComponentsService } from '../../../core/services/material-ui.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
import { AdminService } from '../../../core/services/admin.service';
import { Admin } from '../../../core/abstract/interfaces/admin';
declare var google;
declare var $;



@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  adminData: Admin;

  public errors: any = [];

  public profileId;

  public loading = true;
  public loadingmap = true;

  horario: any = {};


  public botonHorario = 'monday';

  @Input() independentId: string;
  @Input() independent: any;

  @Input() pymeId: string;
  @Input() pyme: any;

  @Input() sellerId: string;
  @Input() seller: any;

  @ViewChild('search')
  public searchElement: ElementRef;

  agmMarker: any = [];
  title = 'My first AGM project';
  lat = 0;
  lng = 0;
  address;
  marker = false;
  titleLocation: string;
  public searchControl: FormControl;
  public zoom = 4;

  newMarker = false;
  selectMarker = false;
  locationNew = true;
  updateID;

  constructor(
    private http: HttpClient,
    private _route: ActivatedRoute,
    private MapsAIPloader: MapsAPILoader,
    private ngZone: NgZone,
    private ui: UIComponentsService,
    private adminService: AdminService
  ) { }

  ngOnInit() {

    this.adminData = this.adminService.getAdminData();
    this.getHorarios();
    this.getLocation();
    this.map();
  }

  //carrusel

  carrusel() {
    $(document).ready(function () {
      $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 5,
        //nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
          0: {
            items: 1,
          },
          600: {
            items: 1
          },
          1000: {
            items: 1
          }
        }
      });
    });
  }

  /////////////////////////////////////////////////////7
  //Metodos horarios
  //////////////////////////////////////////////////////////7

  desformatearHora(items) {
    const item = items.split(':');
    let hora = item[0];
    const minuto = item[1].substring(0, 2);
    let tiempo = 'am';
    if (hora.substring(0, 1) == '0' && hora.substring(1, 2) != '0') {
      hora = parseInt(hora.substring(1, 2));
    } else {
      if (hora.substring(0, 2) != '00') {
        hora = parseInt(hora);
        if (hora > 12) {
          hora = hora - 12;
          tiempo = 'pm';
        }
      } else {
        hora = 12;
      }
    }

    const retornar = hora + ':' + minuto + tiempo;
    return retornar;
  }

  formatearHora(items) {
    const item = items.split(':');
    let hora = item[0];
    const minuto = item[1].substring(0, 2);
    const tiempo = item[1].substring(2, 4);
    if (tiempo == 'pm') {
      hora = parseInt(hora) + 12;
    } else {
      if (hora == 12) {
        hora = '00';
      }
    }
    hora = hora + '';
    if (hora.length > 1) {
      return hora + ':' + minuto;
    } else {
      return '0' + hora + ':' + minuto;
    }
  }

  getHorarios() {
    this.loading = true;
    const object = this;
    const url = `${this.adminData.type}/${this.adminData.id}/schedules/show`;

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {
        this.horario = data;
        this.horario['monday'].open = this.formatearHora(this.horario['monday'].open);
        this.horario['monday'].close = this.formatearHora(this.horario['monday'].close);
        this.horario['tuesday'].open = this.formatearHora(this.horario['tuesday'].open);
        this.horario['tuesday'].close = this.formatearHora(this.horario['tuesday'].close);
        this.horario['wednesday'].open = this.formatearHora(this.horario['wednesday'].open);
        this.horario['wednesday'].close = this.formatearHora(this.horario['wednesday'].close);
        this.horario['thursday'].open = this.formatearHora(this.horario['thursday'].open);
        this.horario['thursday'].close = this.formatearHora(this.horario['thursday'].close);
        this.horario['friday'].open = this.formatearHora(this.horario['friday'].open);
        this.horario['friday'].close = this.formatearHora(this.horario['friday'].close);
        this.horario['saturday'].open = this.formatearHora(this.horario['saturday'].open);
        this.horario['saturday'].close = this.formatearHora(this.horario['saturday'].close);
        this.horario['sunday'].open = this.formatearHora(this.horario['sunday'].open);
        this.horario['sunday'].close = this.formatearHora(this.horario['sunday'].close);
        //console.log(this.horario);
        this.carrusel();
        this.loading = false;
      },
      error => {
        this.loading = false;
        if ('_body' in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              object.errors.push(element);
            });
          }
          this.ui.showSnackNotification('Revisa tu conexión a internet', 'OK');
        }
      }
    );
  }

  updateHorario() {
    const horario = this.horario;
    horario['monday'].open = this.desformatearHora(this.horario['monday'].open);
    horario['monday'].close = this.desformatearHora(this.horario['monday'].close);
    horario['tuesday'].open = this.desformatearHora(this.horario['tuesday'].open);
    horario['tuesday'].close = this.desformatearHora(this.horario['tuesday'].close);
    horario['wednesday'].open = this.desformatearHora(this.horario['wednesday'].open);
    horario['wednesday'].close = this.desformatearHora(this.horario['wednesday'].close);
    horario['thursday'].open = this.desformatearHora(this.horario['thursday'].open);
    horario['thursday'].close = this.desformatearHora(this.horario['thursday'].close);
    horario['friday'].open = this.desformatearHora(this.horario['friday'].open);
    horario['friday'].close = this.desformatearHora(this.horario['friday'].close);
    horario['saturday'].open = this.desformatearHora(this.horario['saturday'].open);
    horario['saturday'].close = this.desformatearHora(this.horario['saturday'].close);
    horario['sunday'].open = this.desformatearHora(this.horario['sunday'].open);
    horario['sunday'].close = this.desformatearHora(this.horario['sunday'].close);
    //console.log('horario actual desformateado',this.horario);

    const object = this;
    object.errors = [];
    this.loading = true;

    const url = `${this.adminData.type}/${this.adminData.id}/schedules/update`;

    this.http.put(`${environment.api_url}${url}`, horario).subscribe(
      (data: any) => {

        this.horario = data;
        this.horario['monday'].open = this.formatearHora(this.horario['monday'].open);
        this.horario['monday'].close = this.formatearHora(this.horario['monday'].close);
        this.horario['tuesday'].open = this.formatearHora(this.horario['tuesday'].open);
        this.horario['tuesday'].close = this.formatearHora(this.horario['tuesday'].close);
        this.horario['wednesday'].open = this.formatearHora(this.horario['wednesday'].open);
        this.horario['wednesday'].close = this.formatearHora(this.horario['wednesday'].close);
        this.horario['thursday'].open = this.formatearHora(this.horario['thursday'].open);
        this.horario['thursday'].close = this.formatearHora(this.horario['thursday'].close);
        this.horario['friday'].open = this.formatearHora(this.horario['friday'].open);
        this.horario['friday'].close = this.formatearHora(this.horario['friday'].close);
        this.horario['saturday'].open = this.formatearHora(this.horario['saturday'].open);
        this.horario['saturday'].close = this.formatearHora(this.horario['saturday'].close);
        this.horario['sunday'].open = this.formatearHora(this.horario['sunday'].open);
        this.horario['sunday'].close = this.formatearHora(this.horario['sunday'].close);
        this.ui.showSnackNotification('Horario Actualizado Exitosamente', 'OK');
        this.loading = false;
      },
      error => {
        this.loading = false;
        if ('_body' in error) {
          error = JSON.parse(error._body);
          //console.log(error);
          //console.log(object);
          if (error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              this.ui.showSnackNotification(`${element}`, 'OK');
            });
          } else {
            error.errors.forEach(element => {
              this.ui.showSnackNotification(`${element}`, 'OK');
            });
          }
        }
      });
  }

  goDia(item) {
    this.botonHorario = item;
  }

  //////////////////////////////////////////////////
  //Ubicacion
  ////////////////////////////////////

  map() {
    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.MapsAIPloader.load().then(() => {

      const autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement);
      autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);


      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          var place = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined && place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();

          if (this.newMarker) {
            this.selectMarker = true;
            this.titleLocation = 'Puedes seleccionar una ubicación en el mapa, buscar una dirección o obtener la ubiación actual';
          }

          this.zoom = 12;

        });
      });
    });
  }

  public setCurrentPosition(item) {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
        this.marker = item;
        if (this.newMarker) {
          this.selectMarker = true;
          this.titleLocation = 'Puedes seleccionar una ubicación en el mapa, buscar una dirección o obtener la ubiación actual';
        }
      });
    }
  }

  locationUpdate() {
    this.locationNew = false;
    this.titleLocation = 'Haz click en el marcador de la ubicación a actulizar';
  }

  dragEnd(event) {

    console.log(event);
    if (this.newMarker) {
      this.selectMarker = true;
      this.titleLocation = 'Puedes seleccionar una ubicación en el mapa, buscar una dirección o obtener la ubiación actual';
    }
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;

  }

  oldLat;
  oldLng;

  updateMarker(i) {
   // console.log('div click', i)
    if (!this.locationNew && !this.newMarker) {
      this.lng = this.agmMarker[i].attributes.longitude;
      this.lat = this.agmMarker[i].attributes.latitude;
      this.titleLocation = 'Puedes seleccionar una ubicación en el mapa, buscar una dirección o obtener la ubiación actual';
      this.updateID = this.agmMarker[i].id;
      this.newMarker = true;
      this.selectMarker = true;
     // console.log(this.updateID);
    }
  }

  labelsOptions = {
    color: 'blue',
    fontFamily: '',
    fontSize: '17px',
    fontWeight: 'bold',
    text: 'Mover',
  }

  cancelLocation() {
    this.locationNew = true;
    this.newMarker = false;
    this.selectMarker = false;
    this.updateID = null;
    if (this.agmMarker.length > 0) {
      this.titleLocation = 'Actualmente tienes ' + this.agmMarker.length + ' ubicaciones';
    } else {
      this.titleLocation = 'Actualmente no tienes ninguna ubicación establecida';
      this.setCurrentPosition(false);
    }
  }

  newLocation() {
    this.locationNew = true;
    this.newMarker = true;
    this.titleLocation = 'Puedes seleccionar una ubicación en el mapa, buscar una dirección o obtener la ubiación actual';
  }

  getLocation() {

    const object = this;

    const url = `locations/${this.adminData.type}/${this.adminData.id}`;

    this.http.get(`${environment.api_url}${url}`).subscribe(
      (data: any) => {


        console.log('recibe get location', data['data']);
        this.agmMarker = data['data'];

        if (data['data'].length > 0) {
          this.lat = data['data'][0].attributes.latitude;
          this.lng = data['data'][0].attributes.longitude;
          this.address = data['data'][0].attributes.address;
          this.titleLocation = 'Actualmente tienes ' + data['data'].length + ' ubicaciones';
        } else {
          this.titleLocation = 'Actualmente no tienes ninguna ubicación establecida';
          this.setCurrentPosition(false);
        }

        //this.generalLoading = false;
      },
      error => {
        //this.generalLoading=false;

        if ('_body' in error) {
          error = error._body;
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              object.errors.push(element);
            });
          }
          this.ui.showSnackNotification('Revisa tu conexión a internet', 'OK');
        }
      }
    );
  }

  placeMarker($event) {
    //console.log($event.coords.lat);
    //console.log($event.coords.lng);

    if (this.newMarker) {
      this.selectMarker = true;
      this.titleLocation = 'Puedes seleccionar una ubicación en el mapa, buscar una dirección o obtener la ubiación actual';
    }

    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
    this.marker = true;
  }

  createLocation() {
    this.loading = true;
    const object = this;

    const url = `locations/${this.adminData.type}/${this.adminData.id}/create`;

    const params = {
      'location': {
        'lat': this.lat + '',
        'lng': this.lng + ''
      }
    };

    //console.log('location too save', params);

    this.http.post(`${environment.api_url}${url}`, params).subscribe((data: any) => {

      this.ui.showSnackNotification('Ubicación Guardada exitosamente', 'OK');
      this.getLocation();
      this.cancelLocation();
      this.updateID = null;
      this.loading = false;
      //this.router.navigate(['/profile'], {queryParams: {tab: "independents"}});
    },
      error => {
        this.loading = false;
        if ('_body' in error) {
          error = error._body;
          console.log('error: ', error);
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              object.errors.push(element);
            });
          }
          this.ui.showSnackNotification('Error al actualizar ubicación', 'OK');
        }
      }
    );
  }

  updateLocation() {

    //console.log('update');
    this.loading = true;
    const object = this;


    const url = `locations/${this.adminData.type}/${this.adminData.id}/${this.updateID}/update`;

    const params = {
      'location': {
        'lat': this.lat + '',
        'lng': this.lng + ''
      }
    };
    //JSON.stringify(

    console.log('location to update', params);

    this.http.put(`${environment.api_url}${url}`, params).subscribe((data: any) => {

      this.ui.showSnackNotification('Ubicación actualizada', 'OK');
      this.updateID = null;

      this.loading = false;
      this.getLocation();
      this.cancelLocation();
    },
      error => {
        this.loading = false;
        if ('_body' in error) {
          error = error._body;
          //console.log('error: ', error);
          if (error.errors && error.errors.full_messages) {
            error.errors.full_messages.forEach(element => {
              object.errors.push(element);
            });
          }
          this.ui.showSnackNotification('Error al actualizar ubicación', 'OK');
        }
      }
    );
  }

}


