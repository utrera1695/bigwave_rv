import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss']
})
export class PromotionsComponent implements OnInit {

  myOffers:any=[
		{
			attributes:{name:"Oferta uno"}
		},
		{
			attributes:{name:"Oferta dos"}
		},
		{
			attributes:{name:"Oferta tres"}
		}
	]

  constructor() { }

  ngOnInit() {
  }

  openCreateOffer(){

  }

  deleteOffer(publication, publicationId){

  }

}
