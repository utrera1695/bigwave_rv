import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {HttpClient} from '@angular/common/http';
import {ProductService} from '../../../../core/services/product.service';
import {AdminService} from '../../../../core/services/admin.service';
import {Admin} from '../../../../core/abstract/interfaces/admin';
import {CreateProductDialogComponent} from '../create-product-dialog/create-product-dialog.component';
import {DeleteProductDialogComponent} from '../delete-product-dialog/delete-product-dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {

	public loading: boolean = false;
	public generalLoading: boolean = false;
	public errors: any=[];
	public hasProducts:boolean = true;

	public search:string;
	public myProducts: any;
	public myProducts2: any;
	public products:any;

	currentAdmin: Admin;


	constructor(
		public dialog: MatDialog,
		private ui: UIComponentsService,
		private http: HttpClient,
		private ps: ProductService,
		private as: AdminService,
		private router: Router
	) { }

	ngOnInit() {
		this.myProducts=[];
		this.currentAdmin = this.as.getAdminData();
		this.getMyProducts();
	}

	openEditProduct(productId){
		this.router.navigate(['/admin',this.currentAdmin.type, this.currentAdmin.id, 'productos', productId]);
	}

	openCreateProduct(){
		const dialogRef2 = this.dialog.open(CreateProductDialogComponent, {
			width: '60%',
			disableClose: true
		});

		dialogRef2.afterClosed().subscribe(result => {
			if (result) {
				this.getMyProducts();
			}
		});
	}

	deleteProduct(product){
		let dialogRef = this.dialog.open(DeleteProductDialogComponent, {
			width: '300px',
			data: {
				product: product,
				passRequired: true,
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			this.getMyProducts();
		});

	}

	getMyProducts(){
		this.myProducts=[];
		this.generalLoading=true;
		this.ps.getAll(this.currentAdmin.type, this.currentAdmin.id).subscribe(
			(data) => {
				if (data['data'].length){
					this.myProducts = data['data'];
					this.products = data['data'];
					this.hasProducts = true;
					this.generalLoading=false;
				}else{
					this.generalLoading=false;
					this.hasProducts = false;
				}
			},
			(error) => {
				this.generalLoading=false;
				this.ui.internalGenericError(error, 'Obtener todos los productos');
			}
		);
	}

	changeStatus(id){
		this.ps.changeStatus(id).subscribe(
			data => {
				this.getMyProducts();
			},
			(error) => {
				this.ui.internalGenericError(error, 'Cambiar el estatus de product' + id);
			}
		);
	}

	oderByPrice(){
		this.myProducts.sort(function(a, b) {
			return a.attributes.price - b.attributes.price;
		});
	}

	orderByStatusActive(){
		let productsStatus:any=[];

		for(let i=0; i < this.products.length;i++){
			if(this.products[i].attributes.status==true){
				productsStatus.push(this.products[i]);
			}
		}
		this.myProducts=[];
		this.myProducts = productsStatus;
	}

	orderByStatusDesactive(){
		let productsStatus:any=[];

		for(let i=0; i < this.products.length;i++){
			if(this.products[i].attributes.status==false){
				productsStatus.push(this.products[i]);
			}
		}
		this.myProducts=[];
		this.myProducts = productsStatus;
	}

	searchProduct(){
    let productsFiltered = this.products.filter(e => e.attributes.name.indexOf(this.search) != -1);
		this.myProducts=[];
		this.myProducts = productsFiltered;
	}

}
