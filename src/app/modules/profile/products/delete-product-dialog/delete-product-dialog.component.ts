import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {Admin} from '../../../../core/abstract/interfaces/admin';
import {AdminService} from '../../../../core/services/admin.service';
import {ProductService} from '../../../../core/services/product.service';

@Component({
  selector: 'app-delete-product-dialog',
  templateUrl: './delete-product-dialog.component.html',
  styleUrls: ['./delete-product-dialog.component.scss']
})
export class DeleteProductDialogComponent implements OnInit {

	passRequired: boolean = false;
	password: string = "";
	public loading: boolean = false;
	currentAdmin: Admin;
	constructor(
		public dialogRef: MatDialogRef<DeleteProductDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private ui: UIComponentsService,
		private as: AdminService,
		private ps: ProductService
	) {
	}

	ngOnInit() {
		this.currentAdmin = this.as.getAdminData();
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onDelete(){
		this.loading=true;
		//let url = API_ROUTES.deleteIndependentsProducts().replace(":profile_id", this.data.independentId).replace(":product_id", this.data.product.id);
		this.ps.delete(this.currentAdmin.type, this.currentAdmin.id, this.data.product.id, this.password).subscribe(
			(data) => {
				this.loading=false;
				this.ui.showSnackNotification( "Producto Eliminado Exitosamente", 'OK');
				this.dialogRef.close();
			}, (error) => {
				this.loading=false;
				this.ui.showSnackNotification("Error al eliminar el Producto, verifique su contraseña", 'OK');
			}
		);
	}

}
