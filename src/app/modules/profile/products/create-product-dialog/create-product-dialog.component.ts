import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {MatDialogRef} from '@angular/material';
import {UIComponentsService} from '../../../../core/services/material-ui.service';
import {HttpClient} from '@angular/common/http';
import {map, startWith} from 'rxjs/operators';
import {API_ROUTES} from '../../../../app.constants';
import {environment} from '../../../../../environments/environment';
import {AdminService} from '../../../../core/services/admin.service';
import {Admin} from '../../../../core/abstract/interfaces/admin';

export const _filter = (opt: any[], value: string): any[] => {
	const filterValue = value.toLowerCase();

	return opt.filter(item => item.name.toLowerCase().indexOf(filterValue) === 0);
};

export interface Categories {
	category: string;
	subcategories: any[];
}

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product-dialog.component.html',
  styleUrls: ['./create-product-dialog.component.scss']
})
export class CreateProductDialogComponent implements OnInit {

	stateForm: FormGroup = this.fb.group({
		stateGroup: '',
	});
	stateGroupOptions: Observable<Categories[]>;
	public subcategory_name;
	public removable: boolean = true;
	public loading: boolean = false;
	public subcategories = [];
	public subcategories2: any = [];
	public product: any = {
		"name": "",
		"title": "",
		"description": "",
		"price": "",
		"tags": [],
		"subcategory_ids": [],
		"status": true,
		"currency": '',
		"stock":'',
		"photos":[""],
    cover: ''
	};

	imagen;
	public typeMoney : string = '';
	moneyType = [
		{ value: 'CLP', viewValue: 'Peso chileno' },
		{value: 'USD', viewValue: 'Dólares'},
		{value: 'BS', viewValue: 'Bolívares'}
	];
	myControl: FormControl = new FormControl();
	currentAdmin: Admin;
	constructor(
		private fb: FormBuilder,
		public dialogRef: MatDialogRef<CreateProductDialogComponent>,
		private ui: UIComponentsService,
		private http : HttpClient,
		private as: AdminService
	) { }

	ngOnInit() {
		this.currentAdmin = this.as.getAdminData();
		this.getCategories();
		this.stateGroupOptions = this.stateForm.get('stateGroup')!.valueChanges
			.pipe(
				startWith(''),
				map(value => this._filterGroup(value))
			);
		this.product.status = true;
	}

	private _filterGroup(value: string): Categories[] {

		if (value) {
			return this.subcategories
				.map(c => ({ category: c.category, subcategories: _filter(c.subcategories, value)}))
				.filter(c => c.subcategories.length > 0);
		}

		return this.subcategories;
	}

	addSubcategory(subcategory): void {
		let flag = true;
		this.subcategories2.forEach((s) => {
			if (s.name == subcategory.name) {
				this.ui.showSnackNotification("Ya se agregó esta Categoría", "OK");
				flag = false;
			}
		});
		this.subcategory_name = '';

		if (flag) {
			this.subcategories2.push({name: subcategory.name, id: subcategory.id.toString()})
			this.product.subcategory_ids.push(subcategory.id);
		}
		this.subcategory_name = '';
	}

	removeSubcategory(subcategory: any, subcategoryId): void {
		if (this.subcategories.length === 1) {
			this.ui.showSnackNotification('El producto debe tener al menos una categoría', 'OK');
		} else {
			let index = this.subcategories2.indexOf(subcategory);
			if (index >= 0) {
				this.subcategories2.splice(index, 1);
				this.product.subcategory_ids.splice(index, 1);
			}
		}

	}

	onSelectFile(event) {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();

			reader.readAsDataURL(event.target.files[0]); // read file as data url

			reader.onload = (event) => { // called once readAsDataURL is completed
				let target: any = event.target;
				let content: string = target.result;
				console.log(content);
				this.product.cover = content;
				this.imagen = content;
			}
		}
	}

	close(item): void {
		this.dialogRef.close(item);
	}

	messages(title){
		this.ui.showSnackNotification(title, 'OK');
	}

	createProduct() {

		if(this.product.name==""||this.product.name==null){
			this.messages("Nombre es requerido");
			return;
		}
		if(this.product.price==""||this.product.price==null){
			this.messages("Precio es requerido");
			return;
		}
		if(this.product.currency==""||this.product.currency==null||this.product.currency.length<1){
			this.messages("Moneda es requerido");
			return;
		}
		if(this.product.subcategory_ids.length<1){
			this.messages("Categorias es requerido");
			return;
		}

		if(this.product.status==""||this.product.status==null){
			this.messages("Estatus es requerido");
			return;
		}
		if(this.product.photos[0]==""||this.product.photos[0]==null){
			delete this.product.photos;
		}

		this.createProductsByProfile();
	}

	createProductsByProfile(){
		this.loading=true;

		let body = {"product": this.product};

		this.http.post(`${environment.api_url}${this.currentAdmin.type}/${this.currentAdmin.id}/products`, body).subscribe(
			(data: any) =>      {

				this.messages("Producto creado exitosamente");
				this.close(true);
				this.loading=false;
			},
			error =>   {
				this.loading=false;
				this.ui.internalGenericError(error);
			}
		);

	}

	getCategories(){
		this.loading=true;
		let url = API_ROUTES.getCategories();
		this.http.get(`${environment.api_url}${url}`).subscribe(
			(data: any) => {
				if (data['data'].length)
					this.subcategories = data['data'];
				this.loading=false;

				this.subcategories = this.subcategories.map(function(op) {
					return {category: op.attributes.name, subcategories: op.attributes.subcategories.map(function(s) {
							return {name: s.name, id: s.id}
						}) }
				})

			},
			error =>  {
				this.loading=false;
				this.ui.internalGenericError(error);
			}
		);
	}

}
