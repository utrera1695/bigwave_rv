import {Component, OnInit} from '@angular/core';
import {AdminService} from '../../../core/services/admin.service';
import {Admin} from '../../../core/abstract/interfaces/admin';

@Component({
  selector: 'app-blackboard',
  templateUrl: './blackboard.component.html',
  styleUrls: ['./blackboard.component.scss']
})
export class BlackboardComponent implements OnInit {

	adminData: Admin;
  constructor(
  	private adminService: AdminService
	) { }

  ngOnInit() {
  	this.adminData = this.adminService.getAdminData();
  }
}
