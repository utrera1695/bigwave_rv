import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MenuComponent} from './modules/shared-components/menu/menu.component';
import {NavbarSearchComponent} from './modules/web/search/navbar-search/navbar-search.component';
import {SearchPageComponent} from './modules/web/search/search-page.component';
import {SearchsProfileComponent} from './modules/web/search/searchs-profile/searchs-profile.component';
import {HomeComponent} from './modules/web/home/home.component';
import {UserService} from './core/services/user.service';
import {ForgotPasswordComponent} from './modules/web/forgot-password/forgot-password.component';
import {AngularTokenModule} from 'angular-token';
import {OwnProfilesComponent} from './modules/web/user/own-profiles/own-profiles.component';
import {LoginComponent} from './modules/shared-components/login/login.component';
import {RegisterUserComponent} from './modules/shared-components/register-user/register-user.component';
import {AreYouSureConfirmPasswordComponent} from './modules/shared-components/are-you-sure-confirm-password/are-you-sure-confirm-password.component';
import {FooterComponent} from './modules/shared-components/footer/footer.component';
import {CreateProfileComponent} from './modules/web/create-profile/create-profile.component';
import {ProductsByCategoryComponent} from './modules/web/products-by-category/products-by-category.component';
import {DetailComponent} from './modules/web/detail/detail.component';
import {DetailProductComponent} from './modules/web/detail-product/detail-product.component';
import {AlertComponent} from './modules/shared-components/alert/alert.component';
import {PageComponent} from './modules/web/page/page.component';
import {PoliticsPrivacyComponent} from './modules/shared-components/footer/politics-privacy/politics-privacy.component';
import {ContactFormComponent} from './modules/web/contact-form/contact-form.component';
import {MaterialModule} from './core/UI-modules/material.module';
import {MAT_DATE_LOCALE, MAT_DIALOG_DATA} from '@angular/material';
import 'hammerjs';
import {safeUrlPipe} from './core/pipes/safeurl.pipe';
import {FilterSearchs} from './core/pipes/filterSearchs.pipe';
import {Truncar} from './core/pipes/truncar.pipe';
import {FilterCategoryPyme} from './core/pipes/filterCategoryPyme.pipe';
import {HideItems} from './core/pipes/hidePaginate.pipe';
import {FilterComments} from './core/pipes/filterComments.pipe';
import {GettingStartedComponent} from './modules/web/user/getting-started/getting-started.component';
import {InformationComponent} from './modules/web/user/information/information.component';
import {MessagesProfileComponent} from './modules/shared-components/messages-profile/messages-profile.component';
import {AgmCoreModule} from '@agm/core';
import {CartService} from './core/services/cart.service';
import {CustomersComponent} from './modules/profile/customers/customers.component';
import {OrdersComponent} from './modules/profile/orders/orders.component';
import {StadisticsComponent} from './modules/profile/stadistics/stadistics.component';
import {PublicationsComponent} from './modules/profile/publications/publications.component';
import {PromotionsComponent} from './modules/profile/promotions/promotions.component';
import {BlackboardComponent} from './modules/profile/blackboard/blackboard.component';
import {HelpCenterComponent} from './modules/web/help-center/help-center.component';
import {ShareButtonModule} from '@ngx-share/button';
import {ShareButtonsModule} from '@ngx-share/buttons';
import {ShareModule} from '@ngx-share/core';
import {EditProductComponent} from './modules/profile/products/edit-product/edit-product.component';
import {SidenavService} from './core/services/sidenav.service';
import {ImagesService} from './core/services/images.service';
import {ChatService} from './core/services/chat.service';
import {ActionCableService} from 'angular2-actioncable';
import {ActionCableBigwaveService} from './core/services/action-cable-bigwave.service';
import {LocationService} from './core/services/location.service';
import {SearchService} from './core/services/search.service';
import {NotificationService} from './core/services/notification.service';
import {DateNotificationPipe} from './core/pipes/date-notification.pipe';
import {UIComponentsService} from './core/services/material-ui.service';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FilterStockPipe} from './core/pipes/filterstock.pipe';
import {IconographyFontAwesomeModule} from './core/UI-modules/iconography.font-awesome.module';
import {ANGULAR_TOKEN_CONFIG} from './core/abstract/const/angular-token.config';
import {AutofocusDirective} from './core/directives/focus';
import {PaginProfilesPipe} from './core/pipes/pagin-profiles.pipe';
import {ProductService} from './core/services/product.service';
import {AreYouSureDialogComponent} from './modules/shared-components/are-you-sure-dialog/are-you-sure-dialog.component';
import {NgxImageZoomModule} from 'ngx-image-zoom';
import {IsLoggedGuardService} from './core/services/guards/is-logged-guard.service';
import {ProfileOptionsMenuComponent} from './modules/shared-components/profile-options-menu/profile-options-menu.component';
import {AdminService} from './core/services/admin.service';
import {SidenavBigwaveComponent} from './modules/shared-components/sidenav-bigwave/sidenav-bigwave.component';
import {OwnProfileInformationComponent} from './modules/profile/own-profile-information/own-profile-information.component';
import {PaginDeseosPipe} from './core/pipes/pagin-deseos.pipe';
import {SureComponent} from './modules/shared-components/sure/sure.component';
import {BlockHelperComponent} from './modules/shared-components/block-helper/block-helper.component';
import {ChangeCotizacionStatusBottomshetComponent} from './modules/shared-components/messages-profile/change-cotizacion-status-bottomshet/change-cotizacion-status-bottomshet.component';
import {CartMenuIconComponent} from './modules/web/user/shoping-cart/cart-menu-icon/cart-menu-icon.component';
import {CartListComponent} from './modules/web/user/shoping-cart/cart-list/cart-list.component';
import {CartButtonComponent} from './modules/web/user/shoping-cart/cart-button/cart-button.component';
import {PagingProfilesPipe} from './core/pipes/paging-profiles.pipe';
import {ProfileFollowComponent} from './modules/web/user/profile-follow/profile-follow.component';
import {AddToCartDialogComponent} from './modules/web/user/shoping-cart/add-to-cart-dialog/add-to-cart-dialog.component';
import {CategoryIconoComponent} from './modules/shared-components/category/category-icono/category-icono.component';
import {LikeComponent} from './modules/shared-components/like/like.component';
import {AddCotizationDialogComponent} from './modules/web/user/shoping-cart/cart-list/add-cotization-dialog/add-cotization-dialog.component';
import {FollowComponent} from './modules/shared-components/follow/follow.component';
import {NotificationsComponent} from './modules/web/user/notifications/notifications.component';
import {SelectCountryComponent} from './modules/shared-components/select-country/select-country.component';
import {ImgsProductComponent} from './modules/web/detail-product/imgs-product/imgs-product.component';
import {PaginCategoryPipe} from './core/pipes/pagin-category.pipe';
import {ScheduleComponent} from './modules/profile/schedule/schedule.component';
import {ListProductComponent} from './modules/profile/products/list-product/list-product.component';
import {CreateProductDialogComponent} from './modules/profile/products/create-product-dialog/create-product-dialog.component';
import {DeleteProductDialogComponent} from './modules/profile/products/delete-product-dialog/delete-product-dialog.component';
import {GlobalLoadingComponent} from './modules/shared-components/global-loading/global-loading.component';
import {GlobalUiService} from './core/services/global-ui.service';
import {ChatToggleButtonFabComponent} from './modules/shared-components/chat-bigwave/chat-toggle-button-fab/chat-toggle-button-fab.component';
import {ChatContainerComponent} from './modules/shared-components/chat-bigwave/chat-container/chat-container.component';
import {ChatListComponent} from './modules/shared-components/chat-bigwave/chat-list/chat-list.component';
import {ProfilesCategoryPipe} from './core/pipes/profiles-category.pipe';
import {CotizationDetailComponent} from './modules/shared-components/chat-bigwave/cotization-detail/cotization-detail.component';
import {CotizationService} from './core/services/cotization.service';
import {FollowProfilesPipe} from './core/pipes/follow-profiles.pipe';
import {CotizationDetailItemComponent} from './modules/shared-components/chat-bigwave/cotization-detail/cotization-detail-item/cotization-detail-item.component';
import {LoadingSpinnerComponent} from './modules/shared-components/loading-spinner/loading-spinner.component';
import {AlertGenericMessageComponent} from './modules/shared-components/alert-generic-message/alert-generic-message.component';
import {CarouselModule} from 'ngx-owl-carousel-o';
import {ProfilesCarouselItemsComponent} from './modules/shared-components/profiles-carousel-items/profiles-carousel-items.component';
import {ProductsCarouselItemsComponent} from './modules/shared-components/products-carousel-items/products-carousel-items.component';
import {CategoriesCarouselItemsComponent} from './modules/shared-components/categories-carousel-items/categories-carousel-items.component';
import {NoDataAviableLocaleComponent} from './modules/shared-components/no-data-aviable-locale/no-data-aviable-locale.component';
import {ErrorPageComponent} from './modules/shared-components/error-page/error-page.component';
import {SharedModule} from './core/shared-modules/shared.module';
import {AddPublicationDialogComponent} from './modules/profile/publications/add-publication-dialog/add-publication-dialog.component';
import {ViewPublicationDialogComponent} from './modules/profile/publications/view-publication-dialog/view-publication-dialog.component';
import {NoDataAviableDisplayComponent} from './modules/shared-components/no-data-aviable-display/no-data-aviable-display.component';
import {PaginatorSlicerPipe} from './core/pipes/paginator-slicer.pipe';
import {FilterByPipe} from './core/pipes/filter-by.pipe';
import {ProfileWishesComponent} from './modules/profile/profile-wishes/profile-wishes.component';
import {WishesListComponent} from './modules/profile/profile-wishes/wishes-items/wishes-list.component';
import {AnswerWishComponent} from './modules/profile/profile-wishes/wishes-items/answer-wish/answer-wish.component';
import {TreeNodeGenericComponent} from './modules/shared-components/tree-node-generic/tree-node-generic.component';
import {ViewWishDetailAdminComponent} from './modules/profile/profile-wishes/wishes-items/view-wish-detail-admin/view-wish-detail-admin.component';
import {UserWishesComponent} from './modules/web/user/user-wishes/user-wishes.component';
import {CreateWishComponent} from './modules/web/user/user-wishes/create-wish/create-wish.component';
import {SortByPipe} from './core/pipes/sort-by.pipe';
import {AuthServiceConfig, SocialLoginModule} from 'angularx-social-login';
import {SOCIAL_LOGIN_CONFIG} from './core/abstract/const/social-login.config';
import {FlexLayoutModule} from '@angular/flex-layout';
import { LoadingBarProgressComponent } from './modules/shared-components/loading-bar-progress/loading-bar-progress.component';
import { GlobalAlertComponent } from './modules/shared-components/global-alert/global-alert.component';
import { GenericProductItemComponent } from './modules/shared-components/products/generic-product-item/generic-product-item.component';
import { GenericProfileItemComponent } from './modules/shared-components/generic-profile-item/generic-profile-item.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    NavbarSearchComponent,
    SearchPageComponent,
    SearchsProfileComponent,
    HomeComponent,
    ForgotPasswordComponent,
    OwnProfilesComponent,
    LoginComponent,
    RegisterUserComponent,
    AreYouSureConfirmPasswordComponent,
    DetailComponent,
    DetailProductComponent,
    PageComponent,
    AlertComponent,
    safeUrlPipe,
    UserWishesComponent,
    CreateWishComponent,
    GettingStartedComponent,
    InformationComponent,
    FilterSearchs,
    Truncar,
    FilterCategoryPyme,
    HideItems,
    FilterComments,
    MessagesProfileComponent,
    EditProductComponent,
    FooterComponent,
    PoliticsPrivacyComponent,
    ContactFormComponent,
    ProductsByCategoryComponent,
    CreateProfileComponent,
    BlackboardComponent,
    HelpCenterComponent,
    CustomersComponent,
    OrdersComponent,
    StadisticsComponent,
    PublicationsComponent,
    PromotionsComponent,
    SureComponent,
    BlockHelperComponent,
    LikeComponent,
    FollowComponent,
    ImgsProductComponent,
    ScheduleComponent,
    NotificationsComponent,
    DateNotificationPipe,
    ProfileFollowComponent,
    SelectCountryComponent,
    FilterStockPipe,
    AutofocusDirective,
    PaginProfilesPipe,
    CartButtonComponent,
    CartListComponent,
    AddToCartDialogComponent,
    CartMenuIconComponent,
    AreYouSureDialogComponent,
    PaginCategoryPipe,
    CategoryIconoComponent,
    PagingProfilesPipe,
    AddCotizationDialogComponent,
    ChangeCotizacionStatusBottomshetComponent,
    ProfileOptionsMenuComponent,
    SidenavBigwaveComponent,
    OwnProfileInformationComponent,
    PaginDeseosPipe,
    ListProductComponent,
    CreateProductDialogComponent,
    DeleteProductDialogComponent,
    GlobalLoadingComponent,
    ChatToggleButtonFabComponent,
    ChatContainerComponent,
    ProfilesCategoryPipe,
    FollowProfilesPipe,
    ChatListComponent,
    ProfilesCategoryPipe,
    CotizationDetailComponent,
    CotizationDetailItemComponent,
    LoadingSpinnerComponent,
    AlertGenericMessageComponent,
    ProfilesCarouselItemsComponent,
    ProductsCarouselItemsComponent,
    CategoriesCarouselItemsComponent,
    NoDataAviableLocaleComponent,
    ErrorPageComponent,
    AddPublicationDialogComponent,
    ViewPublicationDialogComponent,
    NoDataAviableDisplayComponent,
    PaginatorSlicerPipe,
    FilterByPipe,
		DateNotificationPipe,
		ProfileWishesComponent,
		WishesListComponent,
		AnswerWishComponent,
		TreeNodeGenericComponent,
		ViewWishDetailAdminComponent,
		SortByPipe,
		LoadingBarProgressComponent,
		GlobalAlertComponent,
		GenericProductItemComponent,
		GenericProfileItemComponent
  ],
  entryComponents: [
    AreYouSureConfirmPasswordComponent,
    AlertComponent,
    PoliticsPrivacyComponent,
    ContactFormComponent,
    CreateProfileComponent,
    SureComponent,
    SelectCountryComponent,
    AddToCartDialogComponent,
    AreYouSureDialogComponent,
    AddCotizationDialogComponent,
    ChangeCotizacionStatusBottomshetComponent,
    LoginComponent,
    RegisterUserComponent,
		CreateProductDialogComponent,
		DeleteProductDialogComponent,
		CotizationDetailComponent,
		AddPublicationDialogComponent,
		ViewPublicationDialogComponent,
		AnswerWishComponent,
		ViewWishDetailAdminComponent,
		CreateWishComponent,
	],
	imports: [
		AppRoutingModule,
		BrowserModule,
		FormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MaterialModule,
		ReactiveFormsModule,
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyBg7tZl34ZEcA3qtqmUUQHpy1bX_0Tj39A',
			libraries: ['places']
		}),
		ShareModule,
		ShareButtonsModule,
		ShareButtonModule,
		AngularTokenModule.forRoot(ANGULAR_TOKEN_CONFIG),
		IconographyFontAwesomeModule,
		NgxImageZoomModule.forRoot(),
		CarouselModule,
		SharedModule,
    SocialLoginModule,
    FlexLayoutModule
  ],
  providers: [
    UserService,
    CartService,
    SidenavService,
    ImagesService,
    ChatService,
    ActionCableService,
    ActionCableBigwaveService,
    LocationService,
    SearchService,
    NotificationService,
    UIComponentsService,
		{ provide: MAT_DIALOG_DATA, useValue: [] },
		{provide: MAT_DATE_LOCALE, useValue: 'es-US'},
    AngularTokenModule,
    ProductService,
    IsLoggedGuardService,
    AdminService,
		GlobalUiService,
		CotizationService,
    { provide: AuthServiceConfig, useFactory: () => SOCIAL_LOGIN_CONFIG }
	],
  bootstrap: [AppComponent]
})
export class AppModule { }
