import {environment} from '../environments/environment';

export const CONSTANTS = {
  BACK_URL: environment.api_url
};

export const Search = {
  query: ''
};

/**
 * No se debería utilizar mas esta constante
 *
 * En lo posible ir elíminando métodos que no estén siendo usados para así eliminar la constante por completo
 * Esto es una mala práctica trabajarlo
 *
 * Recomiendo que se trabaje con un service provider y la url que provenga desde el environment
 * @Deprecated
 * */
export const API_ROUTES = {
  getCategories() {
    return `profiles/categories`;
  },
  getAPyme() {
    return `pymes/:profile_id`;
  },
  getMySellers() {
    return `sellers/own`;
  },
  getASeller() {
    return `sellers/:profile_id`;
  },
  getAIndependent() {
    return `independents/:profile_id`;
  },
  searchs(q) {
    return `/searchs?q=${q}`;
  },
  searchsLookForProfiles(q,page?) {
    return !page ? `searchs/profiles?${q}` : `${q}`;
  },
  createRange() {
    return `:type_profile/:profile_id/products/:product_id/price_ranges`;
  },
  getProduct() {
    return `:type_profile/:profile_id/products/:product_id`;
  },
  getProductByCategory(id) {
    return `searchs/services/filters_by_cat?quantity=20&type_search=products&category=${id}`;
    //return `searchs/services/products?quantity=10&categories=${id}`;;
  },
  getProductByCategoryByPais(id,countries_code){
    return `searchs/services/filters_by_cat?quantity=20&type_search=products&category=${id}&countries_codes=${countries_code}`;
  },
  getProfilesByCategory(id) {
    return `searchs/services/filters_by_cat?quantity=20&type_search=profiles&category=${id}`;
  },

  getProfilesByCategoryByPais(id,countries_code) {
    return `searchs/services/filters_by_cat?quantity=20&type_search=profiles&category=${id}&countries_codes=${countries_code}`;
  },

  getProductsbySubcategory(id) {
    return `subcategories/${id}/products`;
  },
  getProductsbySubcategorybyPais(id,countries_code) {
    return `subcategories/${id}/products?countries_codes=${countries_code}`;
  },
  deletePriceRange() {
    return `:type_profile/:profile_id/products/:product_id/price_ranges/:price_range_id/destroy`;
  },
  productCreateOptions() {
    return `:type_profile/:profile_id/products/:product_id/options`;
  },
  productCreateCustomFields() {
    return `:type_profile/:profile_id/products/:product_id/custom_fields`;
  },
  productDeleteCustomFields() {
    return `:type_profile/:profile_id/products/:product_id/custom_fields/:custom_field_id/destroy`;
  },
  productDeleteOptions() {
    return `:type_profile/:profile_id/products/:product_id/options/:option_id/destroy`;
  },
  followProfile() {
    return `current_user/follow/:type_profile/:profile_id`;
  },
  unfollowProfile() {
    return `current_user/unfollow/:type_profile/:profile_id`;
  },
  getFollowing() {
    return `current_user/following/:type_profile`;
  },
  createWishes() {
    return 'wishes/create';
  },
  myWishes() {
    return 'wishes/my_wishes';
  },
  deleteWishes() {
    return `wishes/:wish_id/destroy`;
  },
  deleteFile() {
    return `products/:product_id/upload/:file_type/:file_id/delete`;
  },
  getCountrys() {
    return `countries`;
  },
  addCart(product) {
    return `shopping_carts/add_product/${product}`;
  },
  getProducts(type, id) {
    return `${type}/${id}/products`;
  },
  getA(type, id) {
    return `${type}/${id}`;
  },
  getHome(countryCode?) {
  	if(countryCode){
			return `services/home?quantity=10&countries_codes=${countryCode}`;
		}
    return `services/home?quantity=10`;
  },
  getPolitics() {
    return `policy_terms/show`;
  },
  getTypeContact() {
    return 'contact_types';
  },
  postContactForm() {
    return 'contacts/create';
  },
  createComment() {
    return `comments/new`;
  },
  getcommmets(type, id) {
    return `comments/${type}/${id}`;
  },
  updateComment(id) {
    return `comments/${id}/update`;
  },
  deleteComment(id) {
    return `comments/${id}/destroy`;
  },
  createRate(type, profile) {
    return `rates/current_user/${type}/${profile}`;
  },
  getRates(type, id) {
    return `rates/current_user/${type}/${id}`;
  }
};
