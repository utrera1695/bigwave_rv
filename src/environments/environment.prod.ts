export const environment = {
  production: true,
  api_url: 'https://api.ibigwave.com/v1/',
  api_url_at: 'https://api.ibigwave.com',
  api_cable: 'https://api.ibigwave.com/cable/'
};
